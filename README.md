# Unoffical Procountor .NET SDK
This is an unofficial .NET SDK for Procountor [API](https://dev.procountor.com/api-reference/).

Maintained by [Wellfish](https://wellfish.se).

## State
This is a **work in progress** and not all endpoints are implemented. If you need a specific endpoint implemented please open an issue.

All models are complete and are generated from the [OpenAPI](https://pts-procountor.pubdev.azure.procountor.com/api/openapi.json) spec.

### Latest version
[NuGet](https://gitlab.com/wellfish/bl-sdk/-/packages)

## Installation
```bash
dotnet nuget add source "https://gitlab.com/api/v4/projects/44016273/packages/nuget/index.json" --name gitlab
```


## Usage (M2M flow)
### Auth
```csharp
[HttpGet("procountor/authurl")]
public async Task<string> GetAuthUrl() // Send to client
{
    return ProcountorAuthService.AuthUrl(_procountorClientId, _procountorRedirectUri, "state");
}

[HttpGet("procountor/callback")]
public async Task<IActionResult> ProcountorCallBack([FromQuery] string? code, [FromQuery] string? state, [FromQuery] string? error, [FromQuery(Name = "error_description")] string? errorDescription)
var apiKey = await ProcountorAuthService.GetApiKey(code, "client_id", "secret", "callback_url");
var accessToken = await ProcountorAuthService.GetAccessToken(apiKey.ApiKey, "client_id", "secret", "callback_url");
```

### Fetch company information
```csharp
public async Task<Details> GetCompanyInformation(string userKey)
{
    var auth = await Auth();
    var connector = new BlConnector(auth.AccessToken, userKey);
    
    var companyInformation = await BlDetailsService.GetDetails(connector);
    return companyInformation;
}
```

### For more examples see the service directory




## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### Releases
To make a new release just push a tag to the repo and a new package will be published to the gitlab nuget feed.

## License
[MIT](https://choosealicense.com/licenses/mit/)

