using Procountor.Client;
using Procountor.Models;
using Procountor.Models.Paginated;
using RestSharp;

namespace Procountor.Services;

public sealed class ProcountorInvoiceService
{
    private readonly ProcountorApiClient _apiClient;

    public ProcountorInvoiceService(ProcountorApiClient apiClient)
    {
        _apiClient = apiClient;
    }

    public ProcountorInvoiceService(string apiUrl = "https://api.procountor.com/api/")
    {
        _apiClient = new ProcountorApiClient(apiUrl);
    }

    public async Task<List<InvoiceBasicInfo>> GetInvoices(DateTime startDate, DateTime endDate, string accessToken)
    {
        var allAccounts = new List<InvoiceBasicInfo>();

        var response = await _apiClient.ExecuteRequest<PaginatedResponse<InvoiceBasicInfo>>(Method.Get, $"invoices?startDate={startDate:yyyy-MM-dd}&endDate={endDate:yyyy-MM-dd}", accessToken);

        allAccounts.AddRange(response.Results);

        return allAccounts;
    }
    public async Task<Invoice> GetInvoice(int id, string accessToken)
    {
        return await _apiClient.ExecuteRequest<Invoice>(Method.Get, $"invoices/{id}", accessToken);
    }
}