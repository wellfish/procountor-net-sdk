using System.Text;
using Procountor.Models;
using RestSharp;

namespace Procountor.Services.Auth;

public static class ProcountorAuthService
{
    /// <summary>
    /// Generates the AuthUrl used to initiate the oauth2 flow
    /// </summary>
    /// <param name="procountorClientId"></param>
    /// <param name="procountorRedirectUri"></param>
    /// <param name="state"></param>
    /// <param name="authorizationUrl">You may want to change if working against the test environment</param>
    public static string AuthUrl(string procountorClientId, string procountorRedirectUri, string? state, string authorizationUrl = "https://api.procountor.com/keylogin")
    {
        var url = new StringBuilder(authorizationUrl);
        url.Append($"?client_id={procountorClientId}");
        url.Append($"&redirect_uri={procountorRedirectUri}");
        url.Append("&response_type=code");
        if (!string.IsNullOrWhiteSpace(state))
        {
            url.Append($"&state={state}");
        }
        return url.ToString();
    }

    /// <summary>
    /// Generates a API KEY
    /// https://dev.procountor.com/m2m-authentication/#creating%20an%20api%20key%20using%20the%20api
    /// </summary>
    /// <param name="code"></param>
    /// <param name="procountorClientId"></param>
    /// <param name="procountorClientSecret"></param>
    /// <param name="procountorRedirectUri"></param>
    /// <param name="authorizationUrl">You may want to change if working against the test environment</param>
    /// <exception cref="Exception"></exception>
    public static async Task<ApiKeyResponse> GetApiKey(string code, string procountorClientId, string procountorClientSecret, string procountorRedirectUri, string authorizationUrl = "https://api.procountor.com/api/")
    {
        var restClient = new RestClient(authorizationUrl);
        const string resource = "oauth/key";
        var request = new RestRequest(resource, Method.Post);
        request.AddHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        request.AddParameter("grant_type", "authorization_code");
        request.AddParameter("client_id", procountorClientId);
        request.AddParameter("client_secret", procountorClientSecret);
        request.AddParameter("code", code);
        request.AddParameter("redirect_uri", procountorRedirectUri);

        var response = await restClient.ExecuteAsync<ApiKeyResponse>(request);
        if (!response.IsSuccessful)
        {
            throw response.ErrorException ?? new Exception(response.ErrorMessage);
        }

        return response.Data ?? throw new Exception("Could not get data from Procountor");
    }

    public static async Task<AccessTokenResponse> GetAccessToken(string apiKey, string procountorClientId, string procountorClientSecret, string procountorRedirectUri, string authorizationUrl = "https://api.procountor.com/api/")
    {
        var restClient = new RestClient(authorizationUrl);
        const string resource = "oauth/token";
        var request = new RestRequest(resource, Method.Post);
        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        request.AddParameter("grant_type", "client_credentials");
        request.AddParameter("client_id", procountorClientId);
        request.AddParameter("client_secret", procountorClientSecret);
        request.AddParameter("redirect_uri", procountorRedirectUri);
        request.AddParameter("api_key", apiKey);

        var response = await restClient.ExecuteAsync<AccessTokenResponse>(request);
        if (!response.IsSuccessful)
        {
            throw response.ErrorException ?? new Exception(response.ErrorMessage);
        }

        return response.Data ?? throw new Exception("Could not get data from Procountor");
    }
}