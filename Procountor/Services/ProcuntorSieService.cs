// namespace Procountor.Services;
//
// public class procountorSieService
// {
//     private readonly procountorFiscalyearService _fiscalyearService;
//     private readonly procountorAccountsBalancesService _accountsBalancesService;
//     private readonly procountorVoucherService _voucherService;
//     public procountorSieService(procountorFiscalyearService fiscalyearService, procountorVoucherService voucherService, procountorAccountsBalancesService accountsBalancesService)
//     {
//         _fiscalyearService = fiscalyearService;
//         _voucherService = voucherService;
//         _accountsBalancesService = accountsBalancesService;
//     }
//
//     public procountorSieService(string apiUrl = "https://eaccountingapi.procountoronline.com/v2/")
//     {
//         _fiscalyearService = new procountorFiscalyearService(apiUrl);
//         _accountsBalancesService = new procountorAccountsBalancesService(apiUrl);
//         _voucherService = new procountorVoucherService(apiUrl);
//     }
//
//     public async Task<SieDocument> GetSieData(string accessToken, Guid fiscalYear)
//     {
//         var fiscalYearById = await _fiscalyearService.GetFiscalyearById(fiscalYear, accessToken);
//         var balances = await _accountsBalancesService.GetAccountsBalances(fiscalYearById.StartDate, accessToken);
//         var vouchers = await _voucherService.GetVouchersByFiscalYear(fiscalYearById.Id, accessToken);
//         
//         var sieDocument = GenerateSieDocument(balances, vouchers, fiscalYearById);
//         return sieDocument;
//     }
//
//     public SieDocument GenerateSieDocument(List<AccountBalanceApi> balances, List<VoucherApi> vouchers, FiscalYearApi fiscalYearById)
//     {
//         var sieDocument = new SieDocument
//         {
//             IB = new List<SiePeriodValue>(),
//             KONTO = new Dictionary<string, SieAccount>(),
//             VER = new List<SieVoucher>(),
//             rar = new SieBookingYear
//             {
//                 Start = fiscalYearById.StartDate,
//                 End = fiscalYearById.EndDate
//             }
//         };
//
//         foreach (var balance in balances)
//         {
//             sieDocument.IB.Add(new SiePeriodValue
//             {
//                 Account = new SieAccount
//                 {
//                     Name = balance.AccountName,
//                     Number = balance.AccountNumber.ToString(),
//                 },
//                 YearNr = 0,
//                 Amount = (decimal) balance.Balance
//             });
//             sieDocument.KONTO.Add(balance.AccountNumber.ToString(), new SieAccount
//             {
//                 Name = balance.AccountName,
//                 Number = balance.AccountNumber.ToString(),
//             });
//         }
//
//         foreach (var voucher in vouchers)
//         {
//             var sieVoucher = new SieVoucher
//             {
//                 Number = voucher.NumberAndNumberSeries,
//                 VoucherDate = voucher.VoucherDate,
//                 CreatedDate = voucher.CreatedUtc,
//                 Text = voucher.VoucherText,
//                 Series = voucher.NumberSeries,
//                 Rows = new List<SieVoucherRow>()
//             };
//             foreach (var row in voucher.Rows)
//             {
//                 sieVoucher.Rows.Add(new SieVoucherRow
//                 {
//                     Account = new SieAccount
//                     {
//                         Number = row.AccountNumber.ToString(),
//                     },
//                     Amount = row.CreditAmount,
//                     Text = row.TransactionText
//                 });
//             }
//
//             sieDocument.VER.Add(sieVoucher);
//         }
//         return sieDocument;
//     }
// }