using Procountor.Client;
using Procountor.Models;
using Procountor.Models.Paginated;
using RestSharp;

namespace Procountor.Services;

public sealed class ProcountorAttachmentService
{
    private readonly ProcountorApiClient _apiClient;

    public ProcountorAttachmentService(ProcountorApiClient apiClient)
    {
        _apiClient = apiClient;
    }

    public ProcountorAttachmentService(string apiUrl = "https://api.procountor.com/api/")
    {
        _apiClient = new ProcountorApiClient(apiUrl);
    }

    public async Task<List<Document>> GetAttachments(DateTime startDate, DateTime endDate, string accessToken, IEnumerable<ReferenceType> filter)
    {
        var attachments = new List<Document>();
        var page = 0;
        var types = string.Join(",", filter);
        const int pageSize = 200;

        while (true)
        {
            var response = await _apiClient.ExecuteRequest<PaginatedResponse<Document>>(Method.Get, $"attachments?size={pageSize}&page={page}&type={types}&createdStartDate={startDate:yyyy-MM-dd}&createdEndDate={endDate:yyyy-MM-dd}&orderByCreatedDate=ASC", accessToken);

            if (response.Results.Count != 0)
            {
                attachments.AddRange(response.Results);
            }

            if (response.Results.Count < pageSize)
            {
                break;
            }

            page++;
        }

        return attachments;
    }
    public async Task<(AttachmentMetadata metadata, byte[] fileData)> GetAttachment(int id, string accessToken)
    {
        return await _apiClient.DownloadDocumentWithMetadata(id, accessToken);
    }
}