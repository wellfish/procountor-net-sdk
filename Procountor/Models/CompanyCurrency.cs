using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CompanyCurrency {
  /// <summary>
  /// Currency of company.
  /// </summary>
  /// <value>Currency of company.</value>
  [DataMember(Name="companyCurrency", EmitDefaultValue=false)]
  [JsonPropertyName("companyCurrency")]
  public string _CompanyCurrency { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CompanyCurrency {\n");
    sb.Append("  _CompanyCurrency: ").Append(_CompanyCurrency).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}