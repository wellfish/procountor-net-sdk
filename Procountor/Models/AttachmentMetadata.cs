using System.Text.Json.Serialization;

namespace Procountor.Models;

public class AttachmentMetadata
{
    [JsonPropertyName("id")]
    public int Id { get; set; }

    [JsonPropertyName("name")]
    public string Name { get; set; }

    [JsonPropertyName("referenceType")]
    public string ReferenceType { get; set; }

    [JsonPropertyName("referenceId")]
    public int ReferenceId { get; set; }

    [JsonPropertyName("sendWithInvoice")]
    public bool SendWithInvoice { get; set; }

    [JsonPropertyName("mimeType")]
    public string MimeType { get; set; }
}