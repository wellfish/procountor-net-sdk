using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Procountor.Models;

public sealed class AccessTokenResponse
{
    [JsonPropertyName("access_token")]
    public required string AccessToken { get; set; }
    
    [JsonPropertyName("expires_in")]
    public required int ExpiresIn { get; set; }

    [NotMapped]
    public DateTimeOffset ExpiresAt { get; set; }

    [OnDeserialized]
    internal void OnDeserializedMethod(StreamingContext context)
    {
        ExpiresAt = DateTimeOffset.Now + TimeSpan.FromSeconds(ExpiresIn);
    }
}