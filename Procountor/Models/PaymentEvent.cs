using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class PaymentEvent {
  /// <summary>
  /// Unique identifier of the payment event
  /// </summary>
  /// <value>Unique identifier of the payment event</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Unique identifier of the invoice.
  /// </summary>
  /// <value>Unique identifier of the invoice.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }

  /// <summary>
  /// Type of payment.
  /// </summary>
  /// <value>Type of payment.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Date when the payment transaction has to be performed.
  /// </summary>
  /// <value>Date when the payment transaction has to be performed.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public DateTime? PaymentDate { get; set; }

  /// <summary>
  /// Status of payment.
  /// </summary>
  /// <value>Status of payment.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Payment amount in accounting currency.
  /// </summary>
  /// <value>Payment amount in accounting currency.</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Currency of the payment in accounting currency in ISO 4217 format.
  /// </summary>
  /// <value>Currency of the payment in accounting currency in ISO 4217 format.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Payment description. Used only in sales invoices.
  /// </summary>
  /// <value>Payment description. Used only in sales invoices.</value>
  [DataMember(Name="description", EmitDefaultValue=false)]
  [JsonPropertyName("description")]
  public string Description { get; set; }

  /// <summary>
  /// Payment amount in paid currency.
  /// </summary>
  /// <value>Payment amount in paid currency.</value>
  [DataMember(Name="paidAmount", EmitDefaultValue=false)]
  [JsonPropertyName("paidAmount")]
  public decimal? PaidAmount { get; set; }

  /// <summary>
  /// Currency of the payment in paid currency in ISO 4217 format.
  /// </summary>
  /// <value>Currency of the payment in paid currency in ISO 4217 format.</value>
  [DataMember(Name="paidCurrency", EmitDefaultValue=false)]
  [JsonPropertyName("paidCurrency")]
  public string PaidCurrency { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentEvent {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
    sb.Append("  Type: ").Append(Type).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  Amount: ").Append(Amount).Append('\n');
    sb.Append("  Currency: ").Append(Currency).Append('\n');
    sb.Append("  Description: ").Append(Description).Append('\n');
    sb.Append("  PaidAmount: ").Append(PaidAmount).Append('\n');
    sb.Append("  PaidCurrency: ").Append(PaidCurrency).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}