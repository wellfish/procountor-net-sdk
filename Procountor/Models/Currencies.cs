using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class Currencies {
  /// <summary>
  /// List of available currencies.
  /// </summary>
  /// <value>List of available currencies.</value>
  [DataMember(Name="currencies", EmitDefaultValue=false)]
  [JsonPropertyName("currencies")]
  public List<string> _Currencies { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Currencies {\n");
      sb.Append("  _Currencies: ").Append(_Currencies).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}