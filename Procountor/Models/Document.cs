using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class Document {
  /// <summary>
  /// ID of the attachment.
  /// </summary>
  /// <value>ID of the attachment.</value>
  [DataMember(Name="attachmentId", EmitDefaultValue=false)]
  [JsonPropertyName("attachmentId")]
  public int? AttachmentId { get; set; }

  /// <summary>
  /// Name of the attached file.
  /// </summary>
  /// <value>Name of the attached file.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Size of the attached file in bytes.
  /// </summary>
  /// <value>Size of the attached file in bytes.</value>
  [DataMember(Name="size", EmitDefaultValue=false)]
  [JsonPropertyName("size")]
  public int? Size { get; set; }

  /// <summary>
  /// Attachment user.
  /// </summary>
  /// <value>Attachment user.</value>
  [DataMember(Name="user", EmitDefaultValue=false)]
  [JsonPropertyName("user")]
  public string User { get; set; }

  /// <summary>
  /// Attachment reference Id
  /// </summary>
  /// <value>Attachment reference Id</value>
  [DataMember(Name="referenceId", EmitDefaultValue=false)]
  [JsonPropertyName("referenceId")]
  public int? ReferenceId { get; set; }

  /// <summary>
  /// Attachment reference type
  /// </summary>
  /// <value>Attachment reference type</value>
  [DataMember(Name="referenceType", EmitDefaultValue=false)]
  [JsonPropertyName("referenceType")]
  public string ReferenceType { get; set; }

  /// <summary>
  /// Attaching time.
  /// </summary>
  /// <value>Attaching time.</value>
  [DataMember(Name="attached", EmitDefaultValue=false)]
  [JsonPropertyName("attached")]
  public DateTime? Attached { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class Document {\n");
    sb.Append("  AttachmentId: ").Append(AttachmentId).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Size: ").Append(Size).Append('\n');
    sb.Append("  User: ").Append(User).Append('\n');
    sb.Append("  ReferenceId: ").Append(ReferenceId).Append('\n');
    sb.Append("  ReferenceType: ").Append(ReferenceType).Append('\n');
    sb.Append("  Attached: ").Append(Attached).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}