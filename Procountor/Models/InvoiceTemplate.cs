using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class InvoiceTemplate {
  /// <summary>
  /// Invoice template ID.
  /// </summary>
  /// <value>Invoice template ID.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Invoice template name.
  /// </summary>
  /// <value>Invoice template name.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceTemplate {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}