using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Penal expense product.
/// </summary>
[DataContract]
public class ProductBasicInfo {
  /// <summary>
  /// Identifier of the product.
  /// </summary>
  /// <value>Identifier of the product.</value>
  [DataMember(Name="productId", EmitDefaultValue=false)]
  [JsonPropertyName("productId")]
  public long? ProductId { get; set; }

  /// <summary>
  /// Name of the product.
  /// </summary>
  /// <value>Name of the product.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Code of the product.
  /// </summary>
  /// <value>Code of the product.</value>
  [DataMember(Name="code", EmitDefaultValue=false)]
  [JsonPropertyName("code")]
  public string Code { get; set; }

  /// <summary>
  /// Default account for the product.
  /// </summary>
  /// <value>Default account for the product.</value>
  [DataMember(Name="defaultAccount", EmitDefaultValue=false)]
  [JsonPropertyName("defaultAccount")]
  public string DefaultAccount { get; set; }

  /// <summary>
  /// Price of the product.
  /// </summary>
  /// <value>Price of the product.</value>
  [DataMember(Name="price", EmitDefaultValue=false)]
  [JsonPropertyName("price")]
  public decimal? Price { get; set; }

  /// <summary>
  /// Currency of the price.
  /// </summary>
  /// <value>Currency of the price.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Unit type of product.
  /// </summary>
  /// <value>Unit type of product.</value>
  [DataMember(Name="unit", EmitDefaultValue=false)]
  [JsonPropertyName("unit")]
  public string Unit { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ProductBasicInfo {\n");
    sb.Append("  ProductId: ").Append(ProductId).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Code: ").Append(Code).Append('\n');
    sb.Append("  DefaultAccount: ").Append(DefaultAccount).Append('\n');
    sb.Append("  Price: ").Append(Price).Append('\n');
    sb.Append("  Currency: ").Append(Currency).Append('\n');
    sb.Append("  Unit: ").Append(Unit).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}