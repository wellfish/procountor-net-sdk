using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class BusinessPartnerGroup {
  /// <summary>
  /// Id of the partner group.
  /// </summary>
  /// <value>Id of the partner group.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Name of the partner group.
  /// </summary>
  /// <value>Name of the partner group.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Type of the partner group.
  /// </summary>
  /// <value>Type of the partner group.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Is partner group active? NOTE: If the business partner group is in use in the debt collection settings for the Maksuvahti service, then it can't be set to inactive
  /// </summary>
  /// <value>Is partner group active? NOTE: If the business partner group is in use in the debt collection settings for the Maksuvahti service, then it can't be set to inactive</value>
  [DataMember(Name="active", EmitDefaultValue=false)]
  [JsonPropertyName("active")]
  public bool? Active { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerGroup {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  Type: ").Append(Type).Append('\n');
      sb.Append("  Active: ").Append(Active).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}