using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoice extra info.
/// </summary>
[DataContract]
public class ExtraInfo {
  /// <summary>
  /// Indicates if accounting by row is used (true) or not (false). Accounting by row means that a separate ledger transaction is created for each invoice row.
  /// </summary>
  /// <value>Indicates if accounting by row is used (true) or not (false). Accounting by row means that a separate ledger transaction is created for each invoice row.</value>
  [DataMember(Name="accountingByRow", EmitDefaultValue=false)]
  [JsonPropertyName("accountingByRow")]
  public bool? AccountingByRow { get; set; }

  /// <summary>
  /// Indicates if the unit prices on invoice rows include VAT (true) or not (false).
  /// </summary>
  /// <value>Indicates if the unit prices on invoice rows include VAT (true) or not (false).</value>
  [DataMember(Name="unitPricesIncludeVat", EmitDefaultValue=false)]
  [JsonPropertyName("unitPricesIncludeVat")]
  public bool? UnitPricesIncludeVat { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ExtraInfo {\n");
    sb.Append("  AccountingByRow: ").Append(AccountingByRow).Append('\n');
    sb.Append("  UnitPricesIncludeVat: ").Append(UnitPricesIncludeVat).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}