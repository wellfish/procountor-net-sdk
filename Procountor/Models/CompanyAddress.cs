using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Address of current company.
/// </summary>
[DataContract]
public class CompanyAddress {
  /// <summary>
  /// Specifier, such as c/o address.
  /// </summary>
  /// <value>Specifier, such as c/o address.</value>
  [DataMember(Name="specifier", EmitDefaultValue=false)]
  [JsonPropertyName("specifier")]
  public string Specifier { get; set; }

  /// <summary>
  /// Post office box for company.
  /// </summary>
  /// <value>Post office box for company.</value>
  [DataMember(Name="poBox", EmitDefaultValue=false)]
  [JsonPropertyName("poBox")]
  public string PoBox { get; set; }

  /// <summary>
  /// Street.
  /// </summary>
  /// <value>Street.</value>
  [DataMember(Name="street", EmitDefaultValue=false)]
  [JsonPropertyName("street")]
  public string Street { get; set; }

  /// <summary>
  /// Zip code
  /// </summary>
  /// <value>Zip code</value>
  [DataMember(Name="zip", EmitDefaultValue=false)]
  [JsonPropertyName("zip")]
  public string Zip { get; set; }

  /// <summary>
  /// City
  /// </summary>
  /// <value>City</value>
  [DataMember(Name="city", EmitDefaultValue=false)]
  [JsonPropertyName("city")]
  public string City { get; set; }

  /// <summary>
  /// Country
  /// </summary>
  /// <value>Country</value>
  [DataMember(Name="country", EmitDefaultValue=false)]
  [JsonPropertyName("country")]
  public string Country { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CompanyAddress {\n");
    sb.Append("  Specifier: ").Append(Specifier).Append('\n');
    sb.Append("  PoBox: ").Append(PoBox).Append('\n');
    sb.Append("  Street: ").Append(Street).Append('\n');
    sb.Append("  Zip: ").Append(Zip).Append('\n');
    sb.Append("  City: ").Append(City).Append('\n');
    sb.Append("  Country: ").Append(Country).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}