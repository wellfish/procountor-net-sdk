using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class Dimension {
  /// <summary>
  /// Dimension ID. Required in PUT
  /// </summary>
  /// <value>Dimension ID. Required in PUT</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Dimension name.
  /// </summary>
  /// <value>Dimension name.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Dimension items.
  /// </summary>
  /// <value>Dimension items.</value>
  [DataMember(Name="items", EmitDefaultValue=false)]
  [JsonPropertyName("items")]
  public List<DimensionItem> Items { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class Dimension {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Items: ").Append(Items).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}