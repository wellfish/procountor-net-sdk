using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class VerifierList {
  /// <summary>
  /// Id of the verifier list.
  /// </summary>
  /// <value>Id of the verifier list.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Name the verifier list.
  /// </summary>
  /// <value>Name the verifier list.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// List of verifiers.
  /// </summary>
  /// <value>List of verifiers.</value>
  [DataMember(Name="verifiers", EmitDefaultValue=false)]
  [JsonPropertyName("verifiers")]
  public List<Verifier> Verifiers { get; set; }

  /// <summary>
  /// List of acceptors.
  /// </summary>
  /// <value>List of acceptors.</value>
  [DataMember(Name="acceptors", EmitDefaultValue=false)]
  [JsonPropertyName("acceptors")]
  public List<Verifier> Acceptors { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class VerifierList {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Verifiers: ").Append(Verifiers).Append('\n');
    sb.Append("  Acceptors: ").Append(Acceptors).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}