using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class InvoicePaymentList {
  /// <summary>
  /// List containing InvoicePayment objects.
  /// </summary>
  /// <value>List containing InvoicePayment objects.</value>
  [DataMember(Name="payments", EmitDefaultValue=false)]
  [JsonPropertyName("payments")]
  public List<InvoicePayment> Payments { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoicePaymentList {\n");
    sb.Append("  Payments: ").Append(Payments).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}