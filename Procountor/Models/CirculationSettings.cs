using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CirculationSettings {
  /// <summary>
  /// Indicates if approval circulation is enabled for purchase invoices.
  /// </summary>
  /// <value>Indicates if approval circulation is enabled for purchase invoices.</value>
  [DataMember(Name="useForPurchaseInvoices", EmitDefaultValue=false)]
  [JsonPropertyName("useForPurchaseInvoices")]
  public bool? UseForPurchaseInvoices { get; set; }

  /// <summary>
  /// Indicates if approval circulation is enabled for travel and expense invoices.
  /// </summary>
  /// <value>Indicates if approval circulation is enabled for travel and expense invoices.</value>
  [DataMember(Name="useForTravelAndExpenseInvoices", EmitDefaultValue=false)]
  [JsonPropertyName("useForTravelAndExpenseInvoices")]
  public bool? UseForTravelAndExpenseInvoices { get; set; }

  /// <summary>
  /// Represents the amount of time in which circulation notification will be sent.
  /// </summary>
  /// <value>Represents the amount of time in which circulation notification will be sent.</value>
  [DataMember(Name="reminderInterval", EmitDefaultValue=false)]
  [JsonPropertyName("reminderInterval")]
  public string ReminderInterval { get; set; }

  /// <summary>
  /// Indicates whether forced notifications are enabled.
  /// </summary>
  /// <value>Indicates whether forced notifications are enabled.</value>
  [DataMember(Name="forceNotifications", EmitDefaultValue=false)]
  [JsonPropertyName("forceNotifications")]
  public bool? ForceNotifications { get; set; }

  /// <summary>
  /// Indicates if determined order is enabled for verification and approval.
  /// </summary>
  /// <value>Indicates if determined order is enabled for verification and approval.</value>
  [DataMember(Name="useDeterminedOrder", EmitDefaultValue=false)]
  [JsonPropertyName("useDeterminedOrder")]
  public bool? UseDeterminedOrder { get; set; }

  /// <summary>
  /// Indicates whether payment is allowed only after approval.
  /// </summary>
  /// <value>Indicates whether payment is allowed only after approval.</value>
  [DataMember(Name="allowPaymentOnlyWhenApproved", EmitDefaultValue=false)]
  [JsonPropertyName("allowPaymentOnlyWhenApproved")]
  public bool? AllowPaymentOnlyWhenApproved { get; set; }

  /// <summary>
  /// Indicates whether payee information change is not allowed.
  /// </summary>
  /// <value>Indicates whether payee information change is not allowed.</value>
  [DataMember(Name="preventPayeeInformationChange", EmitDefaultValue=false)]
  [JsonPropertyName("preventPayeeInformationChange")]
  public bool? PreventPayeeInformationChange { get; set; }

  /// <summary>
  /// Indicates whether specific discussion for invoice is allowed.
  /// </summary>
  /// <value>Indicates whether specific discussion for invoice is allowed.</value>
  [DataMember(Name="allowInvoiceSpecificDiscussion", EmitDefaultValue=false)]
  [JsonPropertyName("allowInvoiceSpecificDiscussion")]
  public bool? AllowInvoiceSpecificDiscussion { get; set; }

  /// <summary>
  /// Indicates whether extended tagging in discussion is allowed.
  /// </summary>
  /// <value>Indicates whether extended tagging in discussion is allowed.</value>
  [DataMember(Name="allowExtendedTaggingInDiscussion", EmitDefaultValue=false)]
  [JsonPropertyName("allowExtendedTaggingInDiscussion")]
  public bool? AllowExtendedTaggingInDiscussion { get; set; }

  /// <summary>
  /// Indicates whether purchase orders and invoices in invoice checking are allocated.
  /// </summary>
  /// <value>Indicates whether purchase orders and invoices in invoice checking are allocated.</value>
  [DataMember(Name="allocatePurchaseOrdersAndInvoicesInChecking", EmitDefaultValue=false)]
  [JsonPropertyName("allocatePurchaseOrdersAndInvoicesInChecking")]
  public bool? AllocatePurchaseOrdersAndInvoicesInChecking { get; set; }

  /// <summary>
  /// Maximum number of verifiers.
  /// </summary>
  /// <value>Maximum number of verifiers.</value>
  [DataMember(Name="maxVerifiers", EmitDefaultValue=false)]
  [JsonPropertyName("maxVerifiers")]
  public int? MaxVerifiers { get; set; }

  /// <summary>
  /// Maximum number of acceptors.
  /// </summary>
  /// <value>Maximum number of acceptors.</value>
  [DataMember(Name="maxAcceptors", EmitDefaultValue=false)]
  [JsonPropertyName("maxAcceptors")]
  public int? MaxAcceptors { get; set; }

  /// <summary>
  /// List of verifiers.
  /// </summary>
  /// <value>List of verifiers.</value>
  [DataMember(Name="verifiers", EmitDefaultValue=false)]
  [JsonPropertyName("verifiers")]
  public List<Verifier> Verifiers { get; set; }

  /// <summary>
  /// List of acceptors.
  /// </summary>
  /// <value>List of acceptors.</value>
  [DataMember(Name="acceptors", EmitDefaultValue=false)]
  [JsonPropertyName("acceptors")]
  public List<Verifier> Acceptors { get; set; }

  /// <summary>
  /// Indicate whether setting personal substitutions is allowed.
  /// </summary>
  /// <value>Indicate whether setting personal substitutions is allowed.</value>
  [DataMember(Name="allowPersonalSubstitution", EmitDefaultValue=false)]
  [JsonPropertyName("allowPersonalSubstitution")]
  public bool? AllowPersonalSubstitution { get; set; }

  /// <summary>
  /// List of substitutions.
  /// </summary>
  /// <value>List of substitutions.</value>
  [DataMember(Name="substitutions", EmitDefaultValue=false)]
  [JsonPropertyName("substitutions")]
  public List<Substitution> Substitutions { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CirculationSettings {\n");
    sb.Append("  UseForPurchaseInvoices: ").Append(UseForPurchaseInvoices).Append('\n');
    sb.Append("  UseForTravelAndExpenseInvoices: ").Append(UseForTravelAndExpenseInvoices).Append('\n');
    sb.Append("  ReminderInterval: ").Append(ReminderInterval).Append('\n');
    sb.Append("  ForceNotifications: ").Append(ForceNotifications).Append('\n');
    sb.Append("  UseDeterminedOrder: ").Append(UseDeterminedOrder).Append('\n');
    sb.Append("  AllowPaymentOnlyWhenApproved: ").Append(AllowPaymentOnlyWhenApproved).Append('\n');
    sb.Append("  PreventPayeeInformationChange: ").Append(PreventPayeeInformationChange).Append('\n');
    sb.Append("  AllowInvoiceSpecificDiscussion: ").Append(AllowInvoiceSpecificDiscussion).Append('\n');
    sb.Append("  AllowExtendedTaggingInDiscussion: ").Append(AllowExtendedTaggingInDiscussion).Append('\n');
    sb.Append("  AllocatePurchaseOrdersAndInvoicesInChecking: ").Append(AllocatePurchaseOrdersAndInvoicesInChecking).Append('\n');
    sb.Append("  MaxVerifiers: ").Append(MaxVerifiers).Append('\n');
    sb.Append("  MaxAcceptors: ").Append(MaxAcceptors).Append('\n');
    sb.Append("  Verifiers: ").Append(Verifiers).Append('\n');
    sb.Append("  Acceptors: ").Append(Acceptors).Append('\n');
    sb.Append("  AllowPersonalSubstitution: ").Append(AllowPersonalSubstitution).Append('\n');
    sb.Append("  Substitutions: ").Append(Substitutions).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}