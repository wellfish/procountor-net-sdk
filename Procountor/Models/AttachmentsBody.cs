using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class AttachmentsBody {
  /// <summary>
  /// Gets or Sets Meta
  /// </summary>
  [DataMember(Name="meta", EmitDefaultValue=false)]
  [JsonPropertyName("meta")]
  public Attachment Meta { get; set; }

  /// <summary>
  /// Gets or Sets File
  /// </summary>
  [DataMember(Name="file", EmitDefaultValue=false)]
  [JsonPropertyName("file")]
  public Object File { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AttachmentsBody {\n");
      sb.Append("  Meta: ").Append(Meta).Append('\n');
      sb.Append("  File: ").Append(File).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}