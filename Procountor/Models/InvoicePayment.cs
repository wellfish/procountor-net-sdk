using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List containing InvoicePayment objects.
/// </summary>
[DataContract]
public class InvoicePayment {
  /// <summary>
  /// The amount of the payment in the given currency. Currency is defined within the invoice, which identifier is set in invoiceId field.
  /// </summary>
  /// <value>The amount of the payment in the given currency. Currency is defined within the invoice, which identifier is set in invoiceId field.</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Bank reference code.
  /// </summary>
  /// <value>Bank reference code.</value>
  [DataMember(Name="bankReferenceCode", EmitDefaultValue=false)]
  [JsonPropertyName("bankReferenceCode")]
  public string BankReferenceCode { get; set; }

  /// <summary>
  /// Reference code generation type. If type not allowed for a user country will be given, then 400 is returned
  /// </summary>
  /// <value>Reference code generation type. If type not allowed for a user country will be given, then 400 is returned</value>
  [DataMember(Name="bankReferenceCodeType", EmitDefaultValue=false)]
  [JsonPropertyName("bankReferenceCodeType")]
  public string BankReferenceCodeType { get; set; }

  /// <summary>
  /// Message for the payment, if reference payment is not used.
  /// </summary>
  /// <value>Message for the payment, if reference payment is not used.</value>
  [DataMember(Name="message", EmitDefaultValue=false)]
  [JsonPropertyName("message")]
  public string Message { get; set; }

  /// <summary>
  /// Bank account number of the recipient.
  /// </summary>
  /// <value>Bank account number of the recipient.</value>
  [DataMember(Name="recipientBankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("recipientBankAccount")]
  public string RecipientBankAccount { get; set; }

  /// <summary>
  /// BIC code of recipient bank account number.
  /// </summary>
  /// <value>BIC code of recipient bank account number.</value>
  [DataMember(Name="recipientBicCode", EmitDefaultValue=false)]
  [JsonPropertyName("recipientBicCode")]
  public string RecipientBicCode { get; set; }

  /// <summary>
  /// Payment method type defining what kind of payment has to be made.
  /// </summary>
  /// <value>Payment method type defining what kind of payment has to be made.</value>
  [DataMember(Name="paymentMethod", EmitDefaultValue=false)]
  [JsonPropertyName("paymentMethod")]
  public string PaymentMethod { get; set; }

  /// <summary>
  /// Bank account number of the person making the payment. The payer bank account has to be predefined in the environment to be able to use the payment.
  /// </summary>
  /// <value>Bank account number of the person making the payment. The payer bank account has to be predefined in the environment to be able to use the payment.</value>
  [DataMember(Name="payerBankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("payerBankAccount")]
  public string PayerBankAccount { get; set; }

  /// <summary>
  /// Date specifying when the payment transaction has to be performed. Payment date should be grater then current date.
  /// </summary>
  /// <value>Date specifying when the payment transaction has to be performed. Payment date should be grater then current date.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public string PaymentDate { get; set; }

  /// <summary>
  /// Gets or Sets RecipientNameAndAddress
  /// </summary>
  [DataMember(Name="recipientNameAndAddress", EmitDefaultValue=false)]
  [JsonPropertyName("recipientNameAndAddress")]
  public Address RecipientNameAndAddress { get; set; }

  /// <summary>
  /// Receiver bank clearing code.
  /// </summary>
  /// <value>Receiver bank clearing code.</value>
  [DataMember(Name="receiverBankClearingCode", EmitDefaultValue=false)]
  [JsonPropertyName("receiverBankClearingCode")]
  public string ReceiverBankClearingCode { get; set; }

  /// <summary>
  /// Gets or Sets ReceiverBankNameAndAddress
  /// </summary>
  [DataMember(Name="receiverBankNameAndAddress", EmitDefaultValue=false)]
  [JsonPropertyName("receiverBankNameAndAddress")]
  public Address ReceiverBankNameAndAddress { get; set; }

  /// <summary>
  /// Gets or Sets IntermediaryBankNameAndAddress
  /// </summary>
  [DataMember(Name="intermediaryBankNameAndAddress", EmitDefaultValue=false)]
  [JsonPropertyName("intermediaryBankNameAndAddress")]
  public Address IntermediaryBankNameAndAddress { get; set; }

  /// <summary>
  /// Intermediary bank BIC.
  /// </summary>
  /// <value>Intermediary bank BIC.</value>
  [DataMember(Name="intermediaryBankBic", EmitDefaultValue=false)]
  [JsonPropertyName("intermediaryBankBic")]
  public string IntermediaryBankBic { get; set; }

  /// <summary>
  /// If not provided, for Finnish foreign payment it will be automatically set to BOTH_PAY_OWN_FEES.
  /// </summary>
  /// <value>If not provided, for Finnish foreign payment it will be automatically set to BOTH_PAY_OWN_FEES.</value>
  [DataMember(Name="serviceCharge", EmitDefaultValue=false)]
  [JsonPropertyName("serviceCharge")]
  public string ServiceCharge { get; set; }

  /// <summary>
  /// Unique identifier of the invoice.
  /// </summary>
  /// <value>Unique identifier of the invoice.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }

  /// <summary>
  /// Invoice version.
  /// </summary>
  /// <value>Invoice version.</value>
  [DataMember(Name="invoiceVersion", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceVersion")]
  public DateTime? InvoiceVersion { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoicePayment {\n");
    sb.Append("  Amount: ").Append(Amount).Append('\n');
    sb.Append("  BankReferenceCode: ").Append(BankReferenceCode).Append('\n');
    sb.Append("  BankReferenceCodeType: ").Append(BankReferenceCodeType).Append('\n');
    sb.Append("  Message: ").Append(Message).Append('\n');
    sb.Append("  RecipientBankAccount: ").Append(RecipientBankAccount).Append('\n');
    sb.Append("  RecipientBicCode: ").Append(RecipientBicCode).Append('\n');
    sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append('\n');
    sb.Append("  PayerBankAccount: ").Append(PayerBankAccount).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  RecipientNameAndAddress: ").Append(RecipientNameAndAddress).Append('\n');
    sb.Append("  ReceiverBankClearingCode: ").Append(ReceiverBankClearingCode).Append('\n');
    sb.Append("  ReceiverBankNameAndAddress: ").Append(ReceiverBankNameAndAddress).Append('\n');
    sb.Append("  IntermediaryBankNameAndAddress: ").Append(IntermediaryBankNameAndAddress).Append('\n');
    sb.Append("  IntermediaryBankBic: ").Append(IntermediaryBankBic).Append('\n');
    sb.Append("  ServiceCharge: ").Append(ServiceCharge).Append('\n');
    sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
    sb.Append("  InvoiceVersion: ").Append(InvoiceVersion).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}