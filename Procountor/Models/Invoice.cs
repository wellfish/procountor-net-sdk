using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class Invoice {
  /// <summary>
  /// Unique identifier of the invoice. Generated by Procountor and present in the object returned.
  /// </summary>
  /// <value>Unique identifier of the invoice. Generated by Procountor and present in the object returned.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Technical ID for the business partner. Used to link the invoice to a customer or supplier in the business partner register. If supplied, the company must have this partner ID in the corresponding register. This field is not editable in PUT /invoices endpoint.
  /// </summary>
  /// <value>Technical ID for the business partner. Used to link the invoice to a customer or supplier in the business partner register. If supplied, the company must have this partner ID in the corresponding register. This field is not editable in PUT /invoices endpoint.</value>
  [DataMember(Name="partnerId", EmitDefaultValue=false)]
  [JsonPropertyName("partnerId")]
  public int? PartnerId { get; set; }

  /// <summary>
  /// Invoice type. Note that this affects validation requirements. PERIODIC_TAX_RETURN is supported only by GET /invoices endpoint. It's not supported by PUT and POST /invoices endpoints.
  /// </summary>
  /// <value>Invoice type. Note that this affects validation requirements. PERIODIC_TAX_RETURN is supported only by GET /invoices endpoint. It's not supported by PUT and POST /invoices endpoints.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Invoice status. A new invoice created through the API will have its status set as UNFINISHED. Updating invoice is possible when its status is UNFINISHED. Updating expense claim, purchase, purchase order and travel invoice is possible when the status is UNFINISHED or RECEIVED. PUT /invoices endpoint is not able to change invoice status.
  /// </summary>
  /// <value>Invoice status. A new invoice created through the API will have its status set as UNFINISHED. Updating invoice is possible when its status is UNFINISHED. Updating expense claim, purchase, purchase order and travel invoice is possible when the status is UNFINISHED or RECEIVED. PUT /invoices endpoint is not able to change invoice status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Invoice date. This is synonymous to billing date.
  /// </summary>
  /// <value>Invoice date. This is synonymous to billing date.</value>
  [DataMember(Name="date", EmitDefaultValue=false)]
  [JsonPropertyName("date")]
  public DateTime? Date { get; set; }

  /// <summary>
  /// Date of the latest payment transaction related to the invoice. Set automatically when payments are made and can't be modified directly.
  /// </summary>
  /// <value>Date of the latest payment transaction related to the invoice. Set automatically when payments are made and can't be modified directly.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public DateTime? PaymentDate { get; set; }

  /// <summary>
  /// Gets or Sets CounterParty
  /// </summary>
  [DataMember(Name="counterParty", EmitDefaultValue=false)]
  [JsonPropertyName("counterParty")]
  public CounterParty CounterParty { get; set; }

  /// <summary>
  /// Gets or Sets BillingAddress
  /// </summary>
  [DataMember(Name="billingAddress", EmitDefaultValue=false)]
  [JsonPropertyName("billingAddress")]
  public Address BillingAddress { get; set; }

  /// <summary>
  /// Gets or Sets DeliveryAddress
  /// </summary>
  [DataMember(Name="deliveryAddress", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryAddress")]
  public Address DeliveryAddress { get; set; }

  /// <summary>
  /// Gets or Sets PaymentInfo
  /// </summary>
  [DataMember(Name="paymentInfo", EmitDefaultValue=false)]
  [JsonPropertyName("paymentInfo")]
  public PaymentInfo PaymentInfo { get; set; }

  /// <summary>
  /// Gets or Sets DeliveryTermsInfo
  /// </summary>
  [DataMember(Name="deliveryTermsInfo", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryTermsInfo")]
  public DeliveryTermsInfoDTO DeliveryTermsInfo { get; set; }

  /// <summary>
  /// Gets or Sets ExtraInfo
  /// </summary>
  [DataMember(Name="extraInfo", EmitDefaultValue=false)]
  [JsonPropertyName("extraInfo")]
  public ExtraInfo ExtraInfo { get; set; }

  /// <summary>
  /// Invoice discount percentage. Scale: 4.
  /// </summary>
  /// <value>Invoice discount percentage. Scale: 4.</value>
  [DataMember(Name="discountPercent", EmitDefaultValue=false)]
  [JsonPropertyName("discountPercent")]
  public decimal? DiscountPercent { get; set; }

  /// <summary>
  /// Order reference of the invoice. This will be copied to the payment as message if no reference code is specified.
  /// </summary>
  /// <value>Order reference of the invoice. This will be copied to the payment as message if no reference code is specified.</value>
  [DataMember(Name="orderReference", EmitDefaultValue=false)]
  [JsonPropertyName("orderReference")]
  public string OrderReference { get; set; }

  /// <summary>
  /// Invoice rows. An invoice should always have at least one row. The only exception are PERIODIC_TAX_RETURN invoices which do not have any invoice rows.
  /// </summary>
  /// <value>Invoice rows. An invoice should always have at least one row. The only exception are PERIODIC_TAX_RETURN invoices which do not have any invoice rows.</value>
  [DataMember(Name="invoiceRows", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceRows")]
  public List<InvoiceRow> InvoiceRows { get; set; }

  /// <summary>
  /// Invoice number generated by Procountor. For custom invoice numbers, see the originalInvoiceNumber property.
  /// </summary>
  /// <value>Invoice number generated by Procountor. For custom invoice numbers, see the originalInvoiceNumber property.</value>
  [DataMember(Name="invoiceNumber", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceNumber")]
  public int? InvoiceNumber { get; set; }

  /// <summary>
  /// Invoice VAT status. Required for all invoices except travel invoices and expense claims.
  /// </summary>
  /// <value>Invoice VAT status. Required for all invoices except travel invoices and expense claims.</value>
  [DataMember(Name="vatStatus", EmitDefaultValue=false)]
  [JsonPropertyName("vatStatus")]
  public int? VatStatus { get; set; }

  /// <summary>
  /// Invoice number from the biller in an external system.
  /// </summary>
  /// <value>Invoice number from the biller in an external system.</value>
  [DataMember(Name="originalInvoiceNumber", EmitDefaultValue=false)]
  [JsonPropertyName("originalInvoiceNumber")]
  public string OriginalInvoiceNumber { get; set; }

  /// <summary>
  /// First day of the delivery period.
  /// </summary>
  /// <value>First day of the delivery period.</value>
  [DataMember(Name="deliveryStartDate", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryStartDate")]
  public DateTime? DeliveryStartDate { get; set; }

  /// <summary>
  /// Last day of the delivery period.
  /// </summary>
  /// <value>Last day of the delivery period.</value>
  [DataMember(Name="deliveryEndDate", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryEndDate")]
  public DateTime? DeliveryEndDate { get; set; }

  /// <summary>
  /// Delivery method for the goods. SALES_INVOICE and SALES_ORDER do not support type OTHER.
  /// </summary>
  /// <value>Delivery method for the goods. SALES_INVOICE and SALES_ORDER do not support type OTHER.</value>
  [DataMember(Name="deliveryMethod", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryMethod")]
  public string DeliveryMethod { get; set; }

  /// <summary>
  /// Delivery instructions.
  /// </summary>
  /// <value>Delivery instructions.</value>
  [DataMember(Name="deliveryInstructions", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryInstructions")]
  public string DeliveryInstructions { get; set; }

  /// <summary>
  /// Channel of distribution for the invoice. Values EDIFACT and PAPER_INVOICE are not allowed for POST /invoices and PUT /invoices endpoints.
  /// </summary>
  /// <value>Channel of distribution for the invoice. Values EDIFACT and PAPER_INVOICE are not allowed for POST /invoices and PUT /invoices endpoints.</value>
  [DataMember(Name="invoiceChannel", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceChannel")]
  public string InvoiceChannel { get; set; }

  /// <summary>
  /// Gets or Sets InvoiceOperatorInfo
  /// </summary>
  [DataMember(Name="invoiceOperatorInfo", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceOperatorInfo")]
  public InvoiceOperatorInfo InvoiceOperatorInfo { get; set; }

  /// <summary>
  /// Penal interest rate. Scale: 2.
  /// </summary>
  /// <value>Penal interest rate. Scale: 2.</value>
  [DataMember(Name="penaltyPercent", EmitDefaultValue=false)]
  [JsonPropertyName("penaltyPercent")]
  public decimal? PenaltyPercent { get; set; }

  /// <summary>
  /// Language of the invoice. Required for sales invoices and sales orders, otherwise ignored.
  /// </summary>
  /// <value>Language of the invoice. Required for sales invoices and sales orders, otherwise ignored.</value>
  [DataMember(Name="language", EmitDefaultValue=false)]
  [JsonPropertyName("language")]
  public string Language { get; set; }

  /// <summary>
  /// SALES_INVOICE and SALES_ORDER only. ID of invoice template.
  /// </summary>
  /// <value>SALES_INVOICE and SALES_ORDER only. ID of invoice template.</value>
  [DataMember(Name="invoiceTemplateId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceTemplateId")]
  public int? InvoiceTemplateId { get; set; }

  /// <summary>
  /// Invoice notes containing additional information. Visible on the invoice. Use \\n as line break.
  /// </summary>
  /// <value>Invoice notes containing additional information. Visible on the invoice. Use \\n as line break.</value>
  [DataMember(Name="additionalInformation", EmitDefaultValue=false)]
  [JsonPropertyName("additionalInformation")]
  public string AdditionalInformation { get; set; }

  /// <summary>
  /// Country code describing which country's VAT standards are being used. Usage of foreign VAT settings must be agreed on separately with Procountor. Required if the company uses foreign VATs. Example value: SWEDEN.See Address.country in POST/PUT /invoices for a list of allowable values
  /// </summary>
  /// <value>Country code describing which country's VAT standards are being used. Usage of foreign VAT settings must be agreed on separately with Procountor. Required if the company uses foreign VATs. Example value: SWEDEN.See Address.country in POST/PUT /invoices for a list of allowable values</value>
  [DataMember(Name="vatCountry", EmitDefaultValue=false)]
  [JsonPropertyName("vatCountry")]
  public string VatCountry { get; set; }

  /// <summary>
  /// ID of the ledger receipt linked to this invoice. A ledger receipt is linked to every invoice for holding its accounting information. For defining ledger accounts, dimensions or VAT status regarding the invoice, modify the ledger receipt. See PUT /ledgerreceipts for more info.
  /// </summary>
  /// <value>ID of the ledger receipt linked to this invoice. A ledger receipt is linked to every invoice for holding its accounting information. For defining ledger accounts, dimensions or VAT status regarding the invoice, modify the ledger receipt. See PUT /ledgerreceipts for more info.</value>
  [DataMember(Name="ledgerReceiptId", EmitDefaultValue=false)]
  [JsonPropertyName("ledgerReceiptId")]
  public int? LedgerReceiptId { get; set; }

  /// <summary>
  /// Invoice notes (seller's/buyer's notes). Not visible on the invoice. Use \\n as line break.
  /// </summary>
  /// <value>Invoice notes (seller's/buyer's notes). Not visible on the invoice. Use \\n as line break.</value>
  [DataMember(Name="notes", EmitDefaultValue=false)]
  [JsonPropertyName("notes")]
  public string Notes { get; set; }

  /// <summary>
  /// SALES_INVOICE and SALES_ORDER only. ID for external financing agreement. The bankAccount.accountNumber specified must match the one used by the specified financing agreement. Financing agreements cannot be used with cash payments.
  /// </summary>
  /// <value>SALES_INVOICE and SALES_ORDER only. ID for external financing agreement. The bankAccount.accountNumber specified must match the one used by the specified financing agreement. Financing agreements cannot be used with cash payments.</value>
  [DataMember(Name="factoringContractId", EmitDefaultValue=false)]
  [JsonPropertyName("factoringContractId")]
  public int? FactoringContractId { get; set; }

  /// <summary>
  /// SALES_INVOICE and SALES_ORDER only. Additional notes about external financing agreement.
  /// </summary>
  /// <value>SALES_INVOICE and SALES_ORDER only. Additional notes about external financing agreement.</value>
  [DataMember(Name="factoringText", EmitDefaultValue=false)]
  [JsonPropertyName("factoringText")]
  public string FactoringText { get; set; }

  /// <summary>
  /// Only for invoice type PERIODIC_TAX_RETURN. Ignored for other types. Contains the total sum of invoice. POST and PUT operation cannot assign/modify value of this property.
  /// </summary>
  /// <value>Only for invoice type PERIODIC_TAX_RETURN. Ignored for other types. Contains the total sum of invoice. POST and PUT operation cannot assign/modify value of this property.</value>
  [DataMember(Name="sum", EmitDefaultValue=false)]
  [JsonPropertyName("sum")]
  public decimal? Sum { get; set; }

  /// <summary>
  /// Travel information items. A travel invoice may have one or more travel information items containing departure date, return date, destinations and travel purpose.
  /// </summary>
  /// <value>Travel information items. A travel invoice may have one or more travel information items containing departure date, return date, destinations and travel purpose.</value>
  [DataMember(Name="travelInformationItems", EmitDefaultValue=false)]
  [JsonPropertyName("travelInformationItems")]
  public List<TravelInformationItem> TravelInformationItems { get; set; }

  /// <summary>
  /// Gets or Sets InvoiceApprovalInformation
  /// </summary>
  [DataMember(Name="invoiceApprovalInformation", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceApprovalInformation")]
  public InvoiceApprovalInformation InvoiceApprovalInformation { get; set; }

  /// <summary>
  /// List of attachments added to this invoice. Use POST and DELETE /attachments to add and delete attachments.
  /// </summary>
  /// <value>List of attachments added to this invoice. Use POST and DELETE /attachments to add and delete attachments.</value>
  [DataMember(Name="attachments", EmitDefaultValue=false)]
  [JsonPropertyName("attachments")]
  public List<Attachment> Attachments { get; set; }

  /// <summary>
  /// Order number. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.
  /// </summary>
  /// <value>Order number. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.</value>
  [DataMember(Name="orderNumber", EmitDefaultValue=false)]
  [JsonPropertyName("orderNumber")]
  public string OrderNumber { get; set; }

  /// <summary>
  /// Agreement number. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.
  /// </summary>
  /// <value>Agreement number. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.</value>
  [DataMember(Name="agreementNumber", EmitDefaultValue=false)]
  [JsonPropertyName("agreementNumber")]
  public string AgreementNumber { get; set; }

  /// <summary>
  /// Accounting code. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.
  /// </summary>
  /// <value>Accounting code. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.</value>
  [DataMember(Name="accountingCode", EmitDefaultValue=false)]
  [JsonPropertyName("accountingCode")]
  public string AccountingCode { get; set; }

  /// <summary>
  /// Delivery site. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.
  /// </summary>
  /// <value>Delivery site. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.</value>
  [DataMember(Name="deliverySite", EmitDefaultValue=false)]
  [JsonPropertyName("deliverySite")]
  public string DeliverySite { get; set; }

  /// <summary>
  /// Tender reference. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.
  /// </summary>
  /// <value>Tender reference. Valid only in Finnish environments for SALES_INVOICE and PURCHASE_INVOICE.</value>
  [DataMember(Name="tenderReference", EmitDefaultValue=false)]
  [JsonPropertyName("tenderReference")]
  public string TenderReference { get; set; }

  /// <summary>
  /// Invoice version.
  /// </summary>
  /// <value>Invoice version.</value>
  [DataMember(Name="version", EmitDefaultValue=false)]
  [JsonPropertyName("version")]
  public DateTime? Version { get; set; }

  /// <summary>
  /// It marks invoice as an offer. It is applicable only to SALES_ORDER type of invoices.
  /// </summary>
  /// <value>It marks invoice as an offer. It is applicable only to SALES_ORDER type of invoices.</value>
  [DataMember(Name="isOffer", EmitDefaultValue=false)]
  [JsonPropertyName("isOffer")]
  public bool? IsOffer { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class Invoice {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  PartnerId: ").Append(PartnerId).Append('\n');
    sb.Append("  Type: ").Append(Type).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  Date: ").Append(Date).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  CounterParty: ").Append(CounterParty).Append('\n');
    sb.Append("  BillingAddress: ").Append(BillingAddress).Append('\n');
    sb.Append("  DeliveryAddress: ").Append(DeliveryAddress).Append('\n');
    sb.Append("  PaymentInfo: ").Append(PaymentInfo).Append('\n');
    sb.Append("  DeliveryTermsInfo: ").Append(DeliveryTermsInfo).Append('\n');
    sb.Append("  ExtraInfo: ").Append(ExtraInfo).Append('\n');
    sb.Append("  DiscountPercent: ").Append(DiscountPercent).Append('\n');
    sb.Append("  OrderReference: ").Append(OrderReference).Append('\n');
    sb.Append("  InvoiceRows: ").Append(InvoiceRows).Append('\n');
    sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append('\n');
    sb.Append("  VatStatus: ").Append(VatStatus).Append('\n');
    sb.Append("  OriginalInvoiceNumber: ").Append(OriginalInvoiceNumber).Append('\n');
    sb.Append("  DeliveryStartDate: ").Append(DeliveryStartDate).Append('\n');
    sb.Append("  DeliveryEndDate: ").Append(DeliveryEndDate).Append('\n');
    sb.Append("  DeliveryMethod: ").Append(DeliveryMethod).Append('\n');
    sb.Append("  DeliveryInstructions: ").Append(DeliveryInstructions).Append('\n');
    sb.Append("  InvoiceChannel: ").Append(InvoiceChannel).Append('\n');
    sb.Append("  InvoiceOperatorInfo: ").Append(InvoiceOperatorInfo).Append('\n');
    sb.Append("  PenaltyPercent: ").Append(PenaltyPercent).Append('\n');
    sb.Append("  Language: ").Append(Language).Append('\n');
    sb.Append("  InvoiceTemplateId: ").Append(InvoiceTemplateId).Append('\n');
    sb.Append("  AdditionalInformation: ").Append(AdditionalInformation).Append('\n');
    sb.Append("  VatCountry: ").Append(VatCountry).Append('\n');
    sb.Append("  LedgerReceiptId: ").Append(LedgerReceiptId).Append('\n');
    sb.Append("  Notes: ").Append(Notes).Append('\n');
    sb.Append("  FactoringContractId: ").Append(FactoringContractId).Append('\n');
    sb.Append("  FactoringText: ").Append(FactoringText).Append('\n');
    sb.Append("  Sum: ").Append(Sum).Append('\n');
    sb.Append("  TravelInformationItems: ").Append(TravelInformationItems).Append('\n');
    sb.Append("  InvoiceApprovalInformation: ").Append(InvoiceApprovalInformation).Append('\n');
    sb.Append("  Attachments: ").Append(Attachments).Append('\n');
    sb.Append("  OrderNumber: ").Append(OrderNumber).Append('\n');
    sb.Append("  AgreementNumber: ").Append(AgreementNumber).Append('\n');
    sb.Append("  AccountingCode: ").Append(AccountingCode).Append('\n');
    sb.Append("  DeliverySite: ").Append(DeliverySite).Append('\n');
    sb.Append("  TenderReference: ").Append(TenderReference).Append('\n');
    sb.Append("  Version: ").Append(Version).Append('\n');
    sb.Append("  IsOffer: ").Append(IsOffer).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}