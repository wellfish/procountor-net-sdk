using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of error messages.
/// </summary>
[DataContract]
public class ErrorMessage {
  /// <summary>
  /// HTTP response status.
  /// </summary>
  /// <value>HTTP response status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public int? Status { get; set; }

  /// <summary>
  /// Field path of violation.
  /// </summary>
  /// <value>Field path of violation.</value>
  [DataMember(Name="field", EmitDefaultValue=false)]
  [JsonPropertyName("field")]
  public string Field { get; set; }

  /// <summary>
  /// Validation error code or description. 
  /// </summary>
  /// <value>Validation error code or description. </value>
  [DataMember(Name="message", EmitDefaultValue=false)]
  [JsonPropertyName("message")]
  public string Message { get; set; }

  /// <summary>
  /// Gets or Sets Model
  /// </summary>
  [DataMember(Name="model", EmitDefaultValue=false)]
  [JsonPropertyName("model")]
  public DataModel Model { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ErrorMessage {\n");
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  Field: ").Append(Field).Append('\n');
    sb.Append("  Message: ").Append(Message).Append('\n');
    sb.Append("  Model: ").Append(Model).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}