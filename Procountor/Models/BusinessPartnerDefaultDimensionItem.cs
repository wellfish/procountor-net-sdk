using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Dimension items connected with dimension.
/// </summary>
[DataContract]
public class BusinessPartnerDefaultDimensionItem {
  /// <summary>
  /// Unique identifier for the dimension item.
  /// </summary>
  /// <value>Unique identifier for the dimension item.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Code name of the dimension item.
  /// </summary>
  /// <value>Code name of the dimension item.</value>
  [DataMember(Name="codeName", EmitDefaultValue=false)]
  [JsonPropertyName("codeName")]
  public string CodeName { get; set; }

  /// <summary>
  /// Percentage value of the dimension item.
  /// </summary>
  /// <value>Percentage value of the dimension item.</value>
  [DataMember(Name="percent", EmitDefaultValue=false)]
  [JsonPropertyName("percent")]
  public decimal? Percent { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerDefaultDimensionItem {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  CodeName: ").Append(CodeName).Append('\n');
      sb.Append("  Percent: ").Append(Percent).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}