using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using Procountor.Models.Paginated;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class FactoringContractSearchResult {
  /// <summary>
  /// Search results.
  /// </summary>
  /// <value>Search results.</value>
  [DataMember(Name="results", EmitDefaultValue=false)]
  [JsonPropertyName("results")]
  public List<FactoringContract> Results { get; set; }

  /// <summary>
  /// Gets or Sets Meta
  /// </summary>
  [DataMember(Name="meta", EmitDefaultValue=false)]
  [JsonPropertyName("meta")]
  public SearchResultMetaData Meta { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class FactoringContractSearchResult {\n");
    sb.Append("  Results: ").Append(Results).Append('\n');
    sb.Append("  Meta: ").Append(Meta).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}