using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contains rights for financial management functionalities.
/// </summary>
[DataContract]
public class FinancialManagementRights {
  /// <summary>
  /// Access level to closing of accounts tool functionality.
  /// </summary>
  /// <value>Access level to closing of accounts tool functionality.</value>
  [DataMember(Name="closingOfAccountsTool", EmitDefaultValue=false)]
  [JsonPropertyName("closingOfAccountsTool")]
  public string ClosingOfAccountsTool { get; set; }

  /// <summary>
  /// Access level to notifications functionality.
  /// </summary>
  /// <value>Access level to notifications functionality.</value>
  [DataMember(Name="notifications", EmitDefaultValue=false)]
  [JsonPropertyName("notifications")]
  public string Notifications { get; set; }

  /// <summary>
  /// Access level to accounting reports functionality.
  /// </summary>
  /// <value>Access level to accounting reports functionality.</value>
  [DataMember(Name="accountReporting", EmitDefaultValue=false)]
  [JsonPropertyName("accountReporting")]
  public string AccountReporting { get; set; }

  /// <summary>
  /// Access level to archive functionality.
  /// </summary>
  /// <value>Access level to archive functionality.</value>
  [DataMember(Name="archive", EmitDefaultValue=false)]
  [JsonPropertyName("archive")]
  public string Archive { get; set; }

  /// <summary>
  /// Access level to import data functionality.
  /// </summary>
  /// <value>Access level to import data functionality.</value>
  [DataMember(Name="importData", EmitDefaultValue=false)]
  [JsonPropertyName("importData")]
  public string ImportData { get; set; }

  /// <summary>
  /// Access level to incomes register certificate functionality. (Only Finnish environment)
  /// </summary>
  /// <value>Access level to incomes register certificate functionality. (Only Finnish environment)</value>
  [DataMember(Name="incomesRegisterCertificate", EmitDefaultValue=false)]
  [JsonPropertyName("incomesRegisterCertificate")]
  public string IncomesRegisterCertificate { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class FinancialManagementRights {\n");
    sb.Append("  ClosingOfAccountsTool: ").Append(ClosingOfAccountsTool).Append('\n');
    sb.Append("  Notifications: ").Append(Notifications).Append('\n');
    sb.Append("  AccountReporting: ").Append(AccountReporting).Append('\n');
    sb.Append("  Archive: ").Append(Archive).Append('\n');
    sb.Append("  ImportData: ").Append(ImportData).Append('\n');
    sb.Append("  IncomesRegisterCertificate: ").Append(IncomesRegisterCertificate).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}