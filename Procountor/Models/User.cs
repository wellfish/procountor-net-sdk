using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class User {
  /// <summary>
  /// User ID of this user.
  /// </summary>
  /// <value>User ID of this user.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// The username of this user. This is the username used to log in to Procountor.
  /// </summary>
  /// <value>The username of this user. This is the username used to log in to Procountor.</value>
  [DataMember(Name="username", EmitDefaultValue=false)]
  [JsonPropertyName("username")]
  public string Username { get; set; }

  /// <summary>
  /// Identifier for the company this user is logged into. NOTE! This field can also be found in /company endpoint, and that is the recommended way to use it. This field will be deprecated and removed from this endpoint at some point in the future.
  /// </summary>
  /// <value>Identifier for the company this user is logged into. NOTE! This field can also be found in /company endpoint, and that is the recommended way to use it. This field will be deprecated and removed from this endpoint at some point in the future.</value>
  [DataMember(Name="companyId", EmitDefaultValue=false)]
  [JsonPropertyName("companyId")]
  public int? CompanyId { get; set; }

  /// <summary>
  /// Country code of current company environment. NOTE! This field can also be found in /company endpoint, and that is the recommended way to use it. This field will be deprecated and removed from this endpoint at some point in the future.
  /// </summary>
  /// <value>Country code of current company environment. NOTE! This field can also be found in /company endpoint, and that is the recommended way to use it. This field will be deprecated and removed from this endpoint at some point in the future.</value>
  [DataMember(Name="countryCode", EmitDefaultValue=false)]
  [JsonPropertyName("countryCode")]
  public string CountryCode { get; set; }

  /// <summary>
  /// Street
  /// </summary>
  /// <value>Street</value>
  [DataMember(Name="street", EmitDefaultValue=false)]
  [JsonPropertyName("street")]
  public string Street { get; set; }

  /// <summary>
  /// Zip
  /// </summary>
  /// <value>Zip</value>
  [DataMember(Name="zip", EmitDefaultValue=false)]
  [JsonPropertyName("zip")]
  public string Zip { get; set; }

  /// <summary>
  /// City
  /// </summary>
  /// <value>City</value>
  [DataMember(Name="city", EmitDefaultValue=false)]
  [JsonPropertyName("city")]
  public string City { get; set; }

  /// <summary>
  /// Business partner ID of the current user.
  /// </summary>
  /// <value>Business partner ID of the current user.</value>
  [DataMember(Name="partnerId", EmitDefaultValue=false)]
  [JsonPropertyName("partnerId")]
  public int? PartnerId { get; set; }

  /// <summary>
  /// Indicates the login method chosen by the user.
  /// </summary>
  /// <value>Indicates the login method chosen by the user.</value>
  [DataMember(Name="loginMethod", EmitDefaultValue=false)]
  [JsonPropertyName("loginMethod")]
  public string LoginMethod { get; set; }

  /// <summary>
  /// The first name of the user.
  /// </summary>
  /// <value>The first name of the user.</value>
  [DataMember(Name="firstName", EmitDefaultValue=false)]
  [JsonPropertyName("firstName")]
  public string FirstName { get; set; }

  /// <summary>
  /// The last name of the user.
  /// </summary>
  /// <value>The last name of the user.</value>
  [DataMember(Name="lastName", EmitDefaultValue=false)]
  [JsonPropertyName("lastName")]
  public string LastName { get; set; }

  /// <summary>
  /// The email address of the user.
  /// </summary>
  /// <value>The email address of the user.</value>
  [DataMember(Name="email", EmitDefaultValue=false)]
  [JsonPropertyName("email")]
  public string Email { get; set; }

  /// <summary>
  /// The mobile phone number of the user.
  /// </summary>
  /// <value>The mobile phone number of the user.</value>
  [DataMember(Name="mobilePhone", EmitDefaultValue=false)]
  [JsonPropertyName("mobilePhone")]
  public string MobilePhone { get; set; }

  /// <summary>
  /// The secondary phone number of the user.
  /// </summary>
  /// <value>The secondary phone number of the user.</value>
  [DataMember(Name="secondaryPhone", EmitDefaultValue=false)]
  [JsonPropertyName("secondaryPhone")]
  public string SecondaryPhone { get; set; }

  /// <summary>
  /// The social security number of the user.
  /// </summary>
  /// <value>The social security number of the user.</value>
  [DataMember(Name="socialSecurityNumber", EmitDefaultValue=false)]
  [JsonPropertyName("socialSecurityNumber")]
  public string SocialSecurityNumber { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class User {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  Username: ").Append(Username).Append('\n');
      sb.Append("  CompanyId: ").Append(CompanyId).Append('\n');
      sb.Append("  CountryCode: ").Append(CountryCode).Append('\n');
      sb.Append("  Street: ").Append(Street).Append('\n');
      sb.Append("  Zip: ").Append(Zip).Append('\n');
      sb.Append("  City: ").Append(City).Append('\n');
      sb.Append("  PartnerId: ").Append(PartnerId).Append('\n');
      sb.Append("  LoginMethod: ").Append(LoginMethod).Append('\n');
      sb.Append("  FirstName: ").Append(FirstName).Append('\n');
      sb.Append("  LastName: ").Append(LastName).Append('\n');
      sb.Append("  Email: ").Append(Email).Append('\n');
      sb.Append("  MobilePhone: ").Append(MobilePhone).Append('\n');
      sb.Append("  SecondaryPhone: ").Append(SecondaryPhone).Append('\n');
      sb.Append("  SocialSecurityNumber: ").Append(SocialSecurityNumber).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}