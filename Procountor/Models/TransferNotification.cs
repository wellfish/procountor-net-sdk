using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Transfer notifications.
/// </summary>
[DataContract]
public class TransferNotification {
  /// <summary>
  /// Transfer notification language.
  /// </summary>
  /// <value>Transfer notification language.</value>
  [DataMember(Name="lang", EmitDefaultValue=false)]
  [JsonPropertyName("lang")]
  public string Lang { get; set; }

  /// <summary>
  /// Transfer notification message.
  /// </summary>
  /// <value>Transfer notification message.</value>
  [DataMember(Name="notification", EmitDefaultValue=false)]
  [JsonPropertyName("notification")]
  public string Notification { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class TransferNotification {\n");
    sb.Append("  Lang: ").Append(Lang).Append('\n');
    sb.Append("  Notification: ").Append(Notification).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}