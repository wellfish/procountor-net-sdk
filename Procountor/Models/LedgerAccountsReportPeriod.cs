using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of report periods.
/// </summary>
[DataContract]
public class LedgerAccountsReportPeriod {
  /// <summary>
  /// Start date of the report period.
  /// </summary>
  /// <value>Start date of the report period.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the report period.
  /// </summary>
  /// <value>End date of the report period.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Report period rows.
  /// </summary>
  /// <value>Report period rows.</value>
  [DataMember(Name="rows", EmitDefaultValue=false)]
  [JsonPropertyName("rows")]
  public List<LedgerAccountsReportRow> Rows { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccountsReportPeriod {\n");
    sb.Append("  StartDate: ").Append(StartDate).Append('\n');
    sb.Append("  EndDate: ").Append(EndDate).Append('\n');
    sb.Append("  Rows: ").Append(Rows).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}