using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Ledger account name translations. If there is no translations available for given account, api response body will not contain translations list at all.
/// </summary>
[DataContract]
public class LedgerAccountTranslation {
  /// <summary>
  /// Language code in ISO 639-1 format.
  /// </summary>
  /// <value>Language code in ISO 639-1 format.</value>
  [DataMember(Name="languageCode", EmitDefaultValue=false)]
  [JsonPropertyName("languageCode")]
  public string LanguageCode { get; set; }

  /// <summary>
  /// Ledger account name translation for the given language code.
  /// </summary>
  /// <value>Ledger account name translation for the given language code.</value>
  [DataMember(Name="translation", EmitDefaultValue=false)]
  [JsonPropertyName("translation")]
  public string Translation { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccountTranslation {\n");
    sb.Append("  LanguageCode: ").Append(LanguageCode).Append('\n');
    sb.Append("  Translation: ").Append(Translation).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}