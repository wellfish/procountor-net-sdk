using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class ReferencePayment {
  /// <summary>
  /// Unique identifier of the reference payment.
  /// </summary>
  /// <value>Unique identifier of the reference payment.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public long? Id { get; set; }

  /// <summary>
  /// Account number for which the reference payment is generated.
  /// </summary>
  /// <value>Account number for which the reference payment is generated.</value>
  [DataMember(Name="accountNumber", EmitDefaultValue=false)]
  [JsonPropertyName("accountNumber")]
  public string AccountNumber { get; set; }

  /// <summary>
  /// Date when the event was registered in the counterpart bank.
  /// </summary>
  /// <value>Date when the event was registered in the counterpart bank.</value>
  [DataMember(Name="valueDate", EmitDefaultValue=false)]
  [JsonPropertyName("valueDate")]
  public DateTime? ValueDate { get; set; }

  /// <summary>
  /// Date when the payment was paid by the payer in his/her own bank.
  /// </summary>
  /// <value>Date when the payment was paid by the payer in his/her own bank.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public DateTime? PaymentDate { get; set; }

  /// <summary>
  /// The total amount for the reference payment.
  /// </summary>
  /// <value>The total amount for the reference payment.</value>
  [DataMember(Name="sum", EmitDefaultValue=false)]
  [JsonPropertyName("sum")]
  public decimal? Sum { get; set; }

  /// <summary>
  /// Name of the counterparty.
  /// </summary>
  /// <value>Name of the counterparty.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// A reference value for the bank.
  /// </summary>
  /// <value>A reference value for the bank.</value>
  [DataMember(Name="bankReference", EmitDefaultValue=false)]
  [JsonPropertyName("bankReference")]
  public string BankReference { get; set; }

  /// <summary>
  /// Archive code of the reference payment. Archive codes are unique in one bank but two events from different banks can share the same archive code.
  /// </summary>
  /// <value>Archive code of the reference payment. Archive codes are unique in one bank but two events from different banks can share the same archive code.</value>
  [DataMember(Name="archiveId", EmitDefaultValue=false)]
  [JsonPropertyName("archiveId")]
  public string ArchiveId { get; set; }

  /// <summary>
  /// Is the reference payment allocated to an invoice. If it is, the event must also have an invoice ID.
  /// </summary>
  /// <value>Is the reference payment allocated to an invoice. If it is, the event must also have an invoice ID.</value>
  [DataMember(Name="allocated", EmitDefaultValue=false)]
  [JsonPropertyName("allocated")]
  public bool? Allocated { get; set; }

  /// <summary>
  /// Unique identifier of the invoice linked to the event.
  /// </summary>
  /// <value>Unique identifier of the invoice linked to the event.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }

  /// <summary>
  /// A list of attachments added to the reference payment.
  /// </summary>
  /// <value>A list of attachments added to the reference payment.</value>
  [DataMember(Name="attachments", EmitDefaultValue=false)]
  [JsonPropertyName("attachments")]
  public List<Attachment> Attachments { get; set; }

  /// <summary>
  /// Metadata allocations for the reference payment.
  /// </summary>
  /// <value>Metadata allocations for the reference payment.</value>
  [DataMember(Name="allocationMetadataRows", EmitDefaultValue=false)]
  [JsonPropertyName("allocationMetadataRows")]
  public List<AllocationMetadataWithFullLedgerAccountDataRow> AllocationMetadataRows { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ReferencePayment {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  AccountNumber: ").Append(AccountNumber).Append('\n');
    sb.Append("  ValueDate: ").Append(ValueDate).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  Sum: ").Append(Sum).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  BankReference: ").Append(BankReference).Append('\n');
    sb.Append("  ArchiveId: ").Append(ArchiveId).Append('\n');
    sb.Append("  Allocated: ").Append(Allocated).Append('\n');
    sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
    sb.Append("  Attachments: ").Append(Attachments).Append('\n');
    sb.Append("  AllocationMetadataRows: ").Append(AllocationMetadataRows).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}