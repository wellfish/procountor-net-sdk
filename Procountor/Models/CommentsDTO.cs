using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CommentsDTO {
  /// <summary>
  /// List of comments.
  /// </summary>
  /// <value>List of comments.</value>
  [DataMember(Name="comments", EmitDefaultValue=false)]
  [JsonPropertyName("comments")]
  public List<CommentDTO> Comments { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CommentsDTO {\n");
    sb.Append("  Comments: ").Append(Comments).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}