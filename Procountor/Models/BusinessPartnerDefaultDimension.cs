using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class BusinessPartnerDefaultDimension {
  /// <summary>
  /// Unique identifier for the dimension.
  /// </summary>
  /// <value>Unique identifier for the dimension.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Name of the dimension.
  /// </summary>
  /// <value>Name of the dimension.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Dimension items connected with dimension.
  /// </summary>
  /// <value>Dimension items connected with dimension.</value>
  [DataMember(Name="items", EmitDefaultValue=false)]
  [JsonPropertyName("items")]
  public List<BusinessPartnerDefaultDimensionItem> Items { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerDefaultDimension {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  Items: ").Append(Items).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}