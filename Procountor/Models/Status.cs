using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class Status {
  /// <summary>
  /// Name of the API
  /// </summary>
  /// <value>Name of the API</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Version of the API
  /// </summary>
  /// <value>Version of the API</value>
  [DataMember(Name="version", EmitDefaultValue=false)]
  [JsonPropertyName("version")]
  public string Version { get; set; }

  /// <summary>
  /// Status of the API health. OK - API is fine, DEGRADED - some external services are not working, FAILING - API has started, but not working properly
  /// </summary>
  /// <value>Status of the API health. OK - API is fine, DEGRADED - some external services are not working, FAILING - API has started, but not working properly</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string _Status { get; set; }

  /// <summary>
  /// Location of the API
  /// </summary>
  /// <value>Location of the API</value>
  [DataMember(Name="base_url", EmitDefaultValue=false)]
  [JsonPropertyName("base_url")]
  public string BaseUrl { get; set; }

  /// <summary>
  /// Documentation of the API
  /// </summary>
  /// <value>Documentation of the API</value>
  [DataMember(Name="documentation", EmitDefaultValue=false)]
  [JsonPropertyName("documentation")]
  public string Documentation { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Status {\n");
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  Version: ").Append(Version).Append('\n');
      sb.Append("  _Status: ").Append(_Status).Append('\n');
      sb.Append("  BaseUrl: ").Append(BaseUrl).Append('\n');
      sb.Append("  Documentation: ").Append(Documentation).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}