using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CurrencyRate {
  /// <summary>
  /// Gets or Sets _CurrencyRate
  /// </summary>
  [DataMember(Name="currencyRate", EmitDefaultValue=false)]
  [JsonPropertyName("currencyRate")]
  public decimal? _CurrencyRate { get; set; }

  /// <summary>
  /// Gets or Sets CurrencyCode
  /// </summary>
  [DataMember(Name="currencyCode", EmitDefaultValue=false)]
  [JsonPropertyName("currencyCode")]
  public string CurrencyCode { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CurrencyRate {\n");
    sb.Append("  _CurrencyRate: ").Append(_CurrencyRate).Append('\n');
    sb.Append("  CurrencyCode: ").Append(CurrencyCode).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}