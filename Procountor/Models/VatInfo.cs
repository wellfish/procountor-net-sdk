using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of VAT percentages
/// </summary>
[DataContract]
public class VatInfo {
  /// <summary>
  /// VAT percentage value
  /// </summary>
  /// <value>VAT percentage value</value>
  [DataMember(Name="vatPercent", EmitDefaultValue=false)]
  [JsonPropertyName("vatPercent")]
  public double? VatPercent { get; set; }

  /// <summary>
  /// Is VAT percentage allowed for sales
  /// </summary>
  /// <value>Is VAT percentage allowed for sales</value>
  [DataMember(Name="sales", EmitDefaultValue=false)]
  [JsonPropertyName("sales")]
  public bool? Sales { get; set; }

  /// <summary>
  /// Is VAT percentage allowed for purchase
  /// </summary>
  /// <value>Is VAT percentage allowed for purchase</value>
  [DataMember(Name="purchase", EmitDefaultValue=false)]
  [JsonPropertyName("purchase")]
  public bool? Purchase { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class VatInfo {\n");
    sb.Append("  VatPercent: ").Append(VatPercent).Append('\n');
    sb.Append("  Sales: ").Append(Sales).Append('\n');
    sb.Append("  Purchase: ").Append(Purchase).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}