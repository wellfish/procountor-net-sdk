using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class DimensionItem {
  /// <summary>
  /// Dimension item ID.
  /// </summary>
  /// <value>Dimension item ID.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Dimension item code name.
  /// </summary>
  /// <value>Dimension item code name.</value>
  [DataMember(Name="codeName", EmitDefaultValue=false)]
  [JsonPropertyName("codeName")]
  public string CodeName { get; set; }

  /// <summary>
  /// Dimension item status. If the dimension item is marked as active, this property is not present. If the dimension item is inactive, the value of this is property is \"P\".
  /// </summary>
  /// <value>Dimension item status. If the dimension item is marked as active, this property is not present. If the dimension item is inactive, the value of this is property is \"P\".</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Dimension item description.
  /// </summary>
  /// <value>Dimension item description.</value>
  [DataMember(Name="description", EmitDefaultValue=false)]
  [JsonPropertyName("description")]
  public string Description { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DimensionItem {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  CodeName: ").Append(CodeName).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  Description: ").Append(Description).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}