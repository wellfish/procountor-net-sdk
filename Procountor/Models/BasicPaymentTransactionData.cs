using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class BasicPaymentTransactionData {
  /// <summary>
  /// Unique payment identifier.
  /// </summary>
  /// <value>Unique payment identifier.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Unique invoice identifier.
  /// </summary>
  /// <value>Unique invoice identifier.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }

  /// <summary>
  /// Date specifying when the payment transaction has to be performed.
  /// </summary>
  /// <value>Date specifying when the payment transaction has to be performed.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public DateTime? PaymentDate { get; set; }

  /// <summary>
  /// The amount of the payment in the given currency. Currency is defined within the invoice.
  /// </summary>
  /// <value>The amount of the payment in the given currency. Currency is defined within the invoice.</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Recipient name.
  /// </summary>
  /// <value>Recipient name.</value>
  [DataMember(Name="receiverName", EmitDefaultValue=false)]
  [JsonPropertyName("receiverName")]
  public string ReceiverName { get; set; }

  /// <summary>
  /// Payment status.
  /// </summary>
  /// <value>Payment status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// If not provided, for Finnish foreign payment it will be automatically set to BOTH_PAY_OWN_FEES.
  /// </summary>
  /// <value>If not provided, for Finnish foreign payment it will be automatically set to BOTH_PAY_OWN_FEES.</value>
  [DataMember(Name="serviceCharge", EmitDefaultValue=false)]
  [JsonPropertyName("serviceCharge")]
  public string ServiceCharge { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BasicPaymentTransactionData {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
      sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
      sb.Append("  Amount: ").Append(Amount).Append('\n');
      sb.Append("  ReceiverName: ").Append(ReceiverName).Append('\n');
      sb.Append("  Status: ").Append(Status).Append('\n');
      sb.Append("  ServiceCharge: ").Append(ServiceCharge).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}