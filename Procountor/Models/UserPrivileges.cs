using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class UserPrivileges {
  /// <summary>
  /// Role of a current user
  /// </summary>
  /// <value>Role of a current user</value>
  [DataMember(Name="role", EmitDefaultValue=false)]
  [JsonPropertyName("role")]
  public string Role { get; set; }

  /// <summary>
  /// Gets or Sets Rights
  /// </summary>
  [DataMember(Name="rights", EmitDefaultValue=false)]
  [JsonPropertyName("rights")]
  public UserRights Rights { get; set; }

  /// <summary>
  /// Gets or Sets Limitations
  /// </summary>
  [DataMember(Name="limitations", EmitDefaultValue=false)]
  [JsonPropertyName("limitations")]
  public UserLimitations Limitations { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class UserPrivileges {\n");
      sb.Append("  Role: ").Append(Role).Append('\n');
      sb.Append("  Rights: ").Append(Rights).Append('\n');
      sb.Append("  Limitations: ").Append(Limitations).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}