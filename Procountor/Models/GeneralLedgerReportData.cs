using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Report data.
/// </summary>
[DataContract]
public class GeneralLedgerReportData {
  /// <summary>
  /// Ledger account code.
  /// </summary>
  /// <value>Ledger account code.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public string Id { get; set; }

  /// <summary>
  /// Ledger account name.
  /// </summary>
  /// <value>Ledger account name.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Sum of accounting value in this ledger account.
  /// </summary>
  /// <value>Sum of accounting value in this ledger account.</value>
  [DataMember(Name="totalAccountingValue", EmitDefaultValue=false)]
  [JsonPropertyName("totalAccountingValue")]
  public decimal? TotalAccountingValue { get; set; }

  /// <summary>
  /// Ledger receipt transactions.
  /// </summary>
  /// <value>Ledger receipt transactions.</value>
  [DataMember(Name="ledgerReceiptTransactions", EmitDefaultValue=false)]
  [JsonPropertyName("ledgerReceiptTransactions")]
  public List<AccountingReportTransaction> LedgerReceiptTransactions { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class GeneralLedgerReportData {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  TotalAccountingValue: ").Append(TotalAccountingValue).Append('\n');
    sb.Append("  LedgerReceiptTransactions: ").Append(LedgerReceiptTransactions).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}