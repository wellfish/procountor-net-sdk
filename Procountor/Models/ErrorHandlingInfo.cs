using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Information about error handling. Can be empty if message is not handled yet.
/// </summary>
[DataContract]
public class ErrorHandlingInfo {
  /// <summary>
  /// Date when the error message was handled.
  /// </summary>
  /// <value>Date when the error message was handled.</value>
  [DataMember(Name="date", EmitDefaultValue=false)]
  [JsonPropertyName("date")]
  public DateTime? Date { get; set; }

  /// <summary>
  /// Unique identifier of the user who handled the error message.
  /// </summary>
  /// <value>Unique identifier of the user who handled the error message.</value>
  [DataMember(Name="userId", EmitDefaultValue=false)]
  [JsonPropertyName("userId")]
  public int? UserId { get; set; }

  /// <summary>
  /// First name of the user who handled the error message.
  /// </summary>
  /// <value>First name of the user who handled the error message.</value>
  [DataMember(Name="firstName", EmitDefaultValue=false)]
  [JsonPropertyName("firstName")]
  public string FirstName { get; set; }

  /// <summary>
  /// Last name of the user who handled the error message.
  /// </summary>
  /// <value>Last name of the user who handled the error message.</value>
  [DataMember(Name="lastName", EmitDefaultValue=false)]
  [JsonPropertyName("lastName")]
  public string LastName { get; set; }

  /// <summary>
  /// Additional comment for handling the error message.
  /// </summary>
  /// <value>Additional comment for handling the error message.</value>
  [DataMember(Name="comment", EmitDefaultValue=false)]
  [JsonPropertyName("comment")]
  public string Comment { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ErrorHandlingInfo {\n");
    sb.Append("  Date: ").Append(Date).Append('\n');
    sb.Append("  UserId: ").Append(UserId).Append('\n');
    sb.Append("  FirstName: ").Append(FirstName).Append('\n');
    sb.Append("  LastName: ").Append(LastName).Append('\n');
    sb.Append("  Comment: ").Append(Comment).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}