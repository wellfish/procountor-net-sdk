using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class ProductGroups {
  /// <summary>
  /// List of product groups
  /// </summary>
  /// <value>List of product groups</value>
  [DataMember(Name="productGroups", EmitDefaultValue=false)]
  [JsonPropertyName("productGroups")]
  public List<ProductGroup> _ProductGroups { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ProductGroups {\n");
      sb.Append("  _ProductGroups: ").Append(_ProductGroups).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}