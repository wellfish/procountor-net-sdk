using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contains rights for payment transactions functionalities.
/// </summary>
[DataContract]
public class PaymentTransactionsRights {
  /// <summary>
  /// Access level to payment functionality.
  /// </summary>
  /// <value>Access level to payment functionality.</value>
  [DataMember(Name="payment", EmitDefaultValue=false)]
  [JsonPropertyName("payment")]
  public string Payment { get; set; }

  /// <summary>
  /// Access level to approval functionality.
  /// </summary>
  /// <value>Access level to approval functionality.</value>
  [DataMember(Name="approval", EmitDefaultValue=false)]
  [JsonPropertyName("approval")]
  public string Approval { get; set; }

  /// <summary>
  /// Access level to payment allocation functionality.
  /// </summary>
  /// <value>Access level to payment allocation functionality.</value>
  [DataMember(Name="paymentAllocation", EmitDefaultValue=false)]
  [JsonPropertyName("paymentAllocation")]
  public string PaymentAllocation { get; set; }

  /// <summary>
  /// Access level to bank statement and reference payments functionality.
  /// </summary>
  /// <value>Access level to bank statement and reference payments functionality.</value>
  [DataMember(Name="bankStatementAndReferencePayment", EmitDefaultValue=false)]
  [JsonPropertyName("bankStatementAndReferencePayment")]
  public string BankStatementAndReferencePayment { get; set; }

  /// <summary>
  /// Access level to mark paid elsewhere functionality.
  /// </summary>
  /// <value>Access level to mark paid elsewhere functionality.</value>
  [DataMember(Name="markPaidElsewhere", EmitDefaultValue=false)]
  [JsonPropertyName("markPaidElsewhere")]
  public string MarkPaidElsewhere { get; set; }

  /// <summary>
  /// Access level to salary payments functionality.
  /// </summary>
  /// <value>Access level to salary payments functionality.</value>
  [DataMember(Name="salaryPayments", EmitDefaultValue=false)]
  [JsonPropertyName("salaryPayments")]
  public string SalaryPayments { get; set; }

  /// <summary>
  /// Access level to direct bank transfer functionality.
  /// </summary>
  /// <value>Access level to direct bank transfer functionality.</value>
  [DataMember(Name="directBankTransfer", EmitDefaultValue=false)]
  [JsonPropertyName("directBankTransfer")]
  public string DirectBankTransfer { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentTransactionsRights {\n");
    sb.Append("  Payment: ").Append(Payment).Append('\n');
    sb.Append("  Approval: ").Append(Approval).Append('\n');
    sb.Append("  PaymentAllocation: ").Append(PaymentAllocation).Append('\n');
    sb.Append("  BankStatementAndReferencePayment: ").Append(BankStatementAndReferencePayment).Append('\n');
    sb.Append("  MarkPaidElsewhere: ").Append(MarkPaidElsewhere).Append('\n');
    sb.Append("  SalaryPayments: ").Append(SalaryPayments).Append('\n');
    sb.Append("  DirectBankTransfer: ").Append(DirectBankTransfer).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}