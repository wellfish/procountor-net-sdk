using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoice circulation related approval information.
/// </summary>
[DataContract]
public class InvoiceApprovalInformation {
  /// <summary>
  /// List of invoice acceptors.
  /// </summary>
  /// <value>List of invoice acceptors.</value>
  [DataMember(Name="acceptors", EmitDefaultValue=false)]
  [JsonPropertyName("acceptors")]
  public List<InvoiceCheckerInformation> Acceptors { get; set; }

  /// <summary>
  /// List of invoice verifiers.
  /// </summary>
  /// <value>List of invoice verifiers.</value>
  [DataMember(Name="verifiers", EmitDefaultValue=false)]
  [JsonPropertyName("verifiers")]
  public List<InvoiceCheckerInformation> Verifiers { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceApprovalInformation {\n");
    sb.Append("  Acceptors: ").Append(Acceptors).Append('\n');
    sb.Append("  Verifiers: ").Append(Verifiers).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}