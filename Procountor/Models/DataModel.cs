using System.Runtime.Serialization;
using System.Text;

namespace Procountor.Models;

/// <summary>
/// Data model for which constraint violations occurred.
/// </summary>
[DataContract]
public class DataModel {

  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DataModel {\n");
    sb.Append("}\n");
    return sb.ToString();
  }
}