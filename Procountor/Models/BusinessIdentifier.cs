using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// The recipient code data.
/// </summary>
[DataContract]
public class BusinessIdentifier {
  /// <summary>
  /// Business Registration Number (BRN) type of current company identifier. (example values: HETU, SSN, FI, NO,...)
  /// </summary>
  /// <value>Business Registration Number (BRN) type of current company identifier. (example values: HETU, SSN, FI, NO,...)</value>
  [DataMember(Name="brnType", EmitDefaultValue=false)]
  [JsonPropertyName("brnType")]
  public string BrnType { get; set; }

  /// <summary>
  /// Business Registration Number (BRN) code (Example value: '1234567-8', basically BRN without type prefix).
  /// </summary>
  /// <value>Business Registration Number (BRN) code (Example value: '1234567-8', basically BRN without type prefix).</value>
  [DataMember(Name="code", EmitDefaultValue=false)]
  [JsonPropertyName("code")]
  public string Code { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessIdentifier {\n");
      sb.Append("  BrnType: ").Append(BrnType).Append('\n');
      sb.Append("  Code: ").Append(Code).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}