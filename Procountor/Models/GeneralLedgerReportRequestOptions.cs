using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Report request options.
/// </summary>
[DataContract]
public class GeneralLedgerReportRequestOptions {
  /// <summary>
  /// Receipt types that will be included in the report.
  /// </summary>
  /// <value>Receipt types that will be included in the report.</value>
  [DataMember(Name="receiptType", EmitDefaultValue=false)]
  [JsonPropertyName("receiptType")]
  public List<string> ReceiptType { get; set; }

  /// <summary>
  /// Currency for the receipt in ISO 4217 format.
  /// </summary>
  /// <value>Currency for the receipt in ISO 4217 format.</value>
  [DataMember(Name="receiptCurrency", EmitDefaultValue=false)]
  [JsonPropertyName("receiptCurrency")]
  public string ReceiptCurrency { get; set; }

  /// <summary>
  /// Receipt name that will be used in the report.
  /// </summary>
  /// <value>Receipt name that will be used in the report.</value>
  [DataMember(Name="receiptName", EmitDefaultValue=false)]
  [JsonPropertyName("receiptName")]
  public string ReceiptName { get; set; }

  /// <summary>
  /// Entry period start date that will be used in the report. Date will be rounded down to start of the month.
  /// </summary>
  /// <value>Entry period start date that will be used in the report. Date will be rounded down to start of the month.</value>
  [DataMember(Name="entryPeriodStart", EmitDefaultValue=false)]
  [JsonPropertyName("entryPeriodStart")]
  public DateTime? EntryPeriodStart { get; set; }

  /// <summary>
  /// Entry period end date that will be used in the report. Date will be rounded up to end of the month.
  /// </summary>
  /// <value>Entry period end date that will be used in the report. Date will be rounded up to end of the month.</value>
  [DataMember(Name="entryPeriodEnd", EmitDefaultValue=false)]
  [JsonPropertyName("entryPeriodEnd")]
  public DateTime? EntryPeriodEnd { get; set; }

  /// <summary>
  /// Transaction value that will be used in the report.
  /// </summary>
  /// <value>Transaction value that will be used in the report.</value>
  [DataMember(Name="transactionValue", EmitDefaultValue=false)]
  [JsonPropertyName("transactionValue")]
  public string TransactionValue { get; set; }

  /// <summary>
  /// Transaction currency that will be used in the report. Not considered if transaction value is not given.
  /// </summary>
  /// <value>Transaction currency that will be used in the report. Not considered if transaction value is not given.</value>
  [DataMember(Name="transactionCurrency", EmitDefaultValue=false)]
  [JsonPropertyName("transactionCurrency")]
  public string TransactionCurrency { get; set; }

  /// <summary>
  /// Language that will be used in the report.
  /// </summary>
  /// <value>Language that will be used in the report.</value>
  [DataMember(Name="reportLanguage", EmitDefaultValue=false)]
  [JsonPropertyName("reportLanguage")]
  public string ReportLanguage { get; set; }

  /// <summary>
  /// Receipt Numbers that will be used in the general ledger report.
  /// </summary>
  /// <value>Receipt Numbers that will be used in the general ledger report.</value>
  [DataMember(Name="receiptNumbers", EmitDefaultValue=false)]
  [JsonPropertyName("receiptNumbers")]
  public string ReceiptNumbers { get; set; }

  /// <summary>
  /// Transaction Description that will be used in the general ledger report.
  /// </summary>
  /// <value>Transaction Description that will be used in the general ledger report.</value>
  [DataMember(Name="transactionDescription", EmitDefaultValue=false)]
  [JsonPropertyName("transactionDescription")]
  public string TransactionDescription { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class GeneralLedgerReportRequestOptions {\n");
    sb.Append("  ReceiptType: ").Append(ReceiptType).Append('\n');
    sb.Append("  ReceiptCurrency: ").Append(ReceiptCurrency).Append('\n');
    sb.Append("  ReceiptName: ").Append(ReceiptName).Append('\n');
    sb.Append("  EntryPeriodStart: ").Append(EntryPeriodStart).Append('\n');
    sb.Append("  EntryPeriodEnd: ").Append(EntryPeriodEnd).Append('\n');
    sb.Append("  TransactionValue: ").Append(TransactionValue).Append('\n');
    sb.Append("  TransactionCurrency: ").Append(TransactionCurrency).Append('\n');
    sb.Append("  ReportLanguage: ").Append(ReportLanguage).Append('\n');
    sb.Append("  ReceiptNumbers: ").Append(ReceiptNumbers).Append('\n');
    sb.Append("  TransactionDescription: ").Append(TransactionDescription).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}