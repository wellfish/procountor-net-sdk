using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CommentDTO {
  /// <summary>
  /// Identifier for the comment.
  /// </summary>
  /// <value>Identifier for the comment.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Display name for comment author.
  /// </summary>
  /// <value>Display name for comment author.</value>
  [DataMember(Name="author", EmitDefaultValue=false)]
  [JsonPropertyName("author")]
  public string Author { get; set; }

  /// <summary>
  /// Comment date.
  /// </summary>
  /// <value>Comment date.</value>
  [DataMember(Name="dateTime", EmitDefaultValue=false)]
  [JsonPropertyName("dateTime")]
  public DateTime? DateTime { get; set; }

  /// <summary>
  /// Comment on the related item.
  /// </summary>
  /// <value>Comment on the related item.</value>
  [DataMember(Name="comment", EmitDefaultValue=false)]
  [JsonPropertyName("comment")]
  public string Comment { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CommentDTO {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Author: ").Append(Author).Append('\n');
    sb.Append("  DateTime: ").Append(DateTime).Append('\n');
    sb.Append("  Comment: ").Append(Comment).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}