using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoicing info data of the partner.
/// </summary>
[DataContract]
public class InvoicingInfo {
  /// <summary>
  /// Customer number of the partner.
  /// </summary>
  /// <value>Customer number of the partner.</value>
  [DataMember(Name="customerNumber", EmitDefaultValue=false)]
  [JsonPropertyName("customerNumber")]
  public string CustomerNumber { get; set; }

  /// <summary>
  /// Identifier of the partner.
  /// </summary>
  /// <value>Identifier of the partner.</value>
  [DataMember(Name="identifier", EmitDefaultValue=false)]
  [JsonPropertyName("identifier")]
  public string Identifier { get; set; }

  /// <summary>
  /// Identifier type of the partner.
  /// </summary>
  /// <value>Identifier type of the partner.</value>
  [DataMember(Name="identifierType", EmitDefaultValue=false)]
  [JsonPropertyName("identifierType")]
  public string IdentifierType { get; set; }

  /// <summary>
  /// OVT of the partner.
  /// </summary>
  /// <value>OVT of the partner.</value>
  [DataMember(Name="ovt", EmitDefaultValue=false)]
  [JsonPropertyName("ovt")]
  public string Ovt { get; set; }

  /// <summary>
  /// Invoice channel of the partner.
  /// </summary>
  /// <value>Invoice channel of the partner.</value>
  [DataMember(Name="invoiceChannel", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceChannel")]
  public string InvoiceChannel { get; set; }

  /// <summary>
  /// Contact person of the partner.
  /// </summary>
  /// <value>Contact person of the partner.</value>
  [DataMember(Name="contactPerson", EmitDefaultValue=false)]
  [JsonPropertyName("contactPerson")]
  public string ContactPerson { get; set; }

  /// <summary>
  /// Language of the partner.
  /// </summary>
  /// <value>Language of the partner.</value>
  [DataMember(Name="language", EmitDefaultValue=false)]
  [JsonPropertyName("language")]
  public string Language { get; set; }

  /// <summary>
  /// Email of the partner.
  /// </summary>
  /// <value>Email of the partner.</value>
  [DataMember(Name="email", EmitDefaultValue=false)]
  [JsonPropertyName("email")]
  public string Email { get; set; }

  /// <summary>
  /// Invoice operator of the partner.
  /// </summary>
  /// <value>Invoice operator of the partner.</value>
  [DataMember(Name="einvoiceOperator", EmitDefaultValue=false)]
  [JsonPropertyName("einvoiceOperator")]
  public string EinvoiceOperator { get; set; }

  /// <summary>
  /// Invoice address of the partner.
  /// </summary>
  /// <value>Invoice address of the partner.</value>
  [DataMember(Name="einvoiceAddress", EmitDefaultValue=false)]
  [JsonPropertyName("einvoiceAddress")]
  public string EinvoiceAddress { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoicingInfo {\n");
    sb.Append("  CustomerNumber: ").Append(CustomerNumber).Append('\n');
    sb.Append("  Identifier: ").Append(Identifier).Append('\n');
    sb.Append("  IdentifierType: ").Append(IdentifierType).Append('\n');
    sb.Append("  Ovt: ").Append(Ovt).Append('\n');
    sb.Append("  InvoiceChannel: ").Append(InvoiceChannel).Append('\n');
    sb.Append("  ContactPerson: ").Append(ContactPerson).Append('\n');
    sb.Append("  Language: ").Append(Language).Append('\n');
    sb.Append("  Email: ").Append(Email).Append('\n');
    sb.Append("  EinvoiceOperator: ").Append(EinvoiceOperator).Append('\n');
    sb.Append("  EinvoiceAddress: ").Append(EinvoiceAddress).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}