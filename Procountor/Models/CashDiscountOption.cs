using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Up to three cash discount options can be provided. Any additional values enteredwill be ignored. Provided cash discount options will be saved in ascending order by numberOfDays.
/// </summary>
[DataContract]
public class CashDiscountOption {
  /// <summary>
  /// Days specified in cash discount
  /// </summary>
  /// <value>Days specified in cash discount</value>
  [DataMember(Name="numberOfDays", EmitDefaultValue=false)]
  [JsonPropertyName("numberOfDays")]
  public int? NumberOfDays { get; set; }

  /// <summary>
  /// Discount percentage specified in cash discount
  /// </summary>
  /// <value>Discount percentage specified in cash discount</value>
  [DataMember(Name="discountPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("discountPercentage")]
  public decimal? DiscountPercentage { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CashDiscountOption {\n");
    sb.Append("  NumberOfDays: ").Append(NumberOfDays).Append('\n');
    sb.Append("  DiscountPercentage: ").Append(DiscountPercentage).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}