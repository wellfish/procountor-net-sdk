using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of acceptors.
/// </summary>
[DataContract]
public class Verifier {
  /// <summary>
  /// First name of the user.
  /// </summary>
  /// <value>First name of the user.</value>
  [DataMember(Name="firstname", EmitDefaultValue=false)]
  [JsonPropertyName("firstname")]
  public string Firstname { get; set; }

  /// <summary>
  /// Surname (last name) of the user.
  /// </summary>
  /// <value>Surname (last name) of the user.</value>
  [DataMember(Name="surname", EmitDefaultValue=false)]
  [JsonPropertyName("surname")]
  public string Surname { get; set; }

  /// <summary>
  /// Id of the user.
  /// </summary>
  /// <value>Id of the user.</value>
  [DataMember(Name="userId", EmitDefaultValue=false)]
  [JsonPropertyName("userId")]
  public int? UserId { get; set; }

  /// <summary>
  /// Order number of the verifier.
  /// </summary>
  /// <value>Order number of the verifier.</value>
  [DataMember(Name="orderNo", EmitDefaultValue=false)]
  [JsonPropertyName("orderNo")]
  public int? OrderNo { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Verifier {\n");
      sb.Append("  Firstname: ").Append(Firstname).Append('\n');
      sb.Append("  Surname: ").Append(Surname).Append('\n');
      sb.Append("  UserId: ").Append(UserId).Append('\n');
      sb.Append("  OrderNo: ").Append(OrderNo).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}