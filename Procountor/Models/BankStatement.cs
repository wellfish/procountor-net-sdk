using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class BankStatement {
  /// <summary>
  /// Unique identifier of the bank statement.
  /// </summary>
  /// <value>Unique identifier of the bank statement.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public long? Id { get; set; }

  /// <summary>
  /// Account number for which the statement is generated.
  /// </summary>
  /// <value>Account number for which the statement is generated.</value>
  [DataMember(Name="accountNumber", EmitDefaultValue=false)]
  [JsonPropertyName("accountNumber")]
  public string AccountNumber { get; set; }

  /// <summary>
  /// Start date of the bank statement.
  /// </summary>
  /// <value>Start date of the bank statement.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the bank statement.
  /// </summary>
  /// <value>End date of the bank statement.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Currency of the bank statement in ISO 4217 format.
  /// </summary>
  /// <value>Currency of the bank statement in ISO 4217 format.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Number of deposits during the bank statement period.
  /// </summary>
  /// <value>Number of deposits during the bank statement period.</value>
  [DataMember(Name="numberOfDeposits", EmitDefaultValue=false)]
  [JsonPropertyName("numberOfDeposits")]
  public int? NumberOfDeposits { get; set; }

  /// <summary>
  /// Sum of the deposits.
  /// </summary>
  /// <value>Sum of the deposits.</value>
  [DataMember(Name="depositSum", EmitDefaultValue=false)]
  [JsonPropertyName("depositSum")]
  public decimal? DepositSum { get; set; }

  /// <summary>
  /// Number of withdrawals during the bank statement period.
  /// </summary>
  /// <value>Number of withdrawals during the bank statement period.</value>
  [DataMember(Name="numberOfWithdrawals", EmitDefaultValue=false)]
  [JsonPropertyName("numberOfWithdrawals")]
  public int? NumberOfWithdrawals { get; set; }

  /// <summary>
  /// Sum of the withdrawals.
  /// </summary>
  /// <value>Sum of the withdrawals.</value>
  [DataMember(Name="withdrawalSum", EmitDefaultValue=false)]
  [JsonPropertyName("withdrawalSum")]
  public decimal? WithdrawalSum { get; set; }

  /// <summary>
  /// Start balance of the account before this bank statement.
  /// </summary>
  /// <value>Start balance of the account before this bank statement.</value>
  [DataMember(Name="startBalance", EmitDefaultValue=false)]
  [JsonPropertyName("startBalance")]
  public decimal? StartBalance { get; set; }

  /// <summary>
  /// End balance of the account after this bank statement.
  /// </summary>
  /// <value>End balance of the account after this bank statement.</value>
  [DataMember(Name="endBalance", EmitDefaultValue=false)]
  [JsonPropertyName("endBalance")]
  public decimal? EndBalance { get; set; }

  /// <summary>
  /// Number of bank statement.
  /// </summary>
  /// <value>Number of bank statement.</value>
  [DataMember(Name="statementNumber", EmitDefaultValue=false)]
  [JsonPropertyName("statementNumber")]
  public int? StatementNumber { get; set; }

  /// <summary>
  /// List of bank statement events. Events can be nested.
  /// </summary>
  /// <value>List of bank statement events. Events can be nested.</value>
  [DataMember(Name="events", EmitDefaultValue=false)]
  [JsonPropertyName("events")]
  public List<BankStatementEvent> Events { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BankStatement {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  AccountNumber: ").Append(AccountNumber).Append('\n');
      sb.Append("  StartDate: ").Append(StartDate).Append('\n');
      sb.Append("  EndDate: ").Append(EndDate).Append('\n');
      sb.Append("  Currency: ").Append(Currency).Append('\n');
      sb.Append("  NumberOfDeposits: ").Append(NumberOfDeposits).Append('\n');
      sb.Append("  DepositSum: ").Append(DepositSum).Append('\n');
      sb.Append("  NumberOfWithdrawals: ").Append(NumberOfWithdrawals).Append('\n');
      sb.Append("  WithdrawalSum: ").Append(WithdrawalSum).Append('\n');
      sb.Append("  StartBalance: ").Append(StartBalance).Append('\n');
      sb.Append("  EndBalance: ").Append(EndBalance).Append('\n');
      sb.Append("  StatementNumber: ").Append(StatementNumber).Append('\n');
      sb.Append("  Events: ").Append(Events).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}