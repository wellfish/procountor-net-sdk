using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Billing address of current company.
/// </summary>
[DataContract]
public class CompanyBillingAddress {
  /// <summary>
  /// Street of company billing address.
  /// </summary>
  /// <value>Street of company billing address.</value>
  [DataMember(Name="street", EmitDefaultValue=false)]
  [JsonPropertyName("street")]
  public string Street { get; set; }

  /// <summary>
  /// Postal code of company billing address.
  /// </summary>
  /// <value>Postal code of company billing address.</value>
  [DataMember(Name="zip", EmitDefaultValue=false)]
  [JsonPropertyName("zip")]
  public string Zip { get; set; }

  /// <summary>
  /// City of company billing address.
  /// </summary>
  /// <value>City of company billing address.</value>
  [DataMember(Name="city", EmitDefaultValue=false)]
  [JsonPropertyName("city")]
  public string City { get; set; }

  /// <summary>
  /// Country of company billing address.
  /// </summary>
  /// <value>Country of company billing address.</value>
  [DataMember(Name="country", EmitDefaultValue=false)]
  [JsonPropertyName("country")]
  public string Country { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CompanyBillingAddress {\n");
    sb.Append("  Street: ").Append(Street).Append('\n');
    sb.Append("  Zip: ").Append(Zip).Append('\n');
    sb.Append("  City: ").Append(City).Append('\n');
    sb.Append("  Country: ").Append(Country).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}