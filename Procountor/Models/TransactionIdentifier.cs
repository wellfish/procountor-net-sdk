using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class TransactionIdentifier {
  /// <summary>
  /// Value of 2-factor transaction identifier.
  /// </summary>
  /// <value>Value of 2-factor transaction identifier.</value>
  [DataMember(Name="transactionIdentifier", EmitDefaultValue=false)]
  [JsonPropertyName("transactionIdentifier")]
  public string _TransactionIdentifier { get; set; }

  /// <summary>
  /// Multifactor authentication confirmation handler application.
  /// </summary>
  /// <value>Multifactor authentication confirmation handler application.</value>
  [DataMember(Name="confirmationApp", EmitDefaultValue=false)]
  [JsonPropertyName("confirmationApp")]
  public string ConfirmationApp { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TransactionIdentifier {\n");
      sb.Append("  _TransactionIdentifier: ").Append(_TransactionIdentifier).Append('\n');
      sb.Append("  ConfirmationApp: ").Append(ConfirmationApp).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}