using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Registry info data of the partner.
/// </summary>
[DataContract]
public class RegistryInfo {
  /// <summary>
  /// Status of the partner.
  /// </summary>
  /// <value>Status of the partner.</value>
  [DataMember(Name="active", EmitDefaultValue=false)]
  [JsonPropertyName("active")]
  public bool? Active { get; set; }

  /// <summary>
  /// Phone number of the partner.
  /// </summary>
  /// <value>Phone number of the partner.</value>
  [DataMember(Name="phone", EmitDefaultValue=false)]
  [JsonPropertyName("phone")]
  public string Phone { get; set; }

  /// <summary>
  /// Mobile phone number of the partner.
  /// </summary>
  /// <value>Mobile phone number of the partner.</value>
  [DataMember(Name="mobilePhone", EmitDefaultValue=false)]
  [JsonPropertyName("mobilePhone")]
  public string MobilePhone { get; set; }

  /// <summary>
  /// Fax number of the partner.
  /// </summary>
  /// <value>Fax number of the partner.</value>
  [DataMember(Name="fax", EmitDefaultValue=false)]
  [JsonPropertyName("fax")]
  public string Fax { get; set; }

  /// <summary>
  /// Section of the partner.
  /// </summary>
  /// <value>Section of the partner.</value>
  [DataMember(Name="section", EmitDefaultValue=false)]
  [JsonPropertyName("section")]
  public string Section { get; set; }

  /// <summary>
  /// Home municipality of the partner.
  /// </summary>
  /// <value>Home municipality of the partner.</value>
  [DataMember(Name="homeMunicipality", EmitDefaultValue=false)]
  [JsonPropertyName("homeMunicipality")]
  public string HomeMunicipality { get; set; }

  /// <summary>
  /// Report group name of the partner.
  /// </summary>
  /// <value>Report group name of the partner.</value>
  [DataMember(Name="reportGroup", EmitDefaultValue=false)]
  [JsonPropertyName("reportGroup")]
  public string ReportGroup { get; set; }

  /// <summary>
  /// Report group id of the partner.
  /// </summary>
  /// <value>Report group id of the partner.</value>
  [DataMember(Name="reportGroupId", EmitDefaultValue=false)]
  [JsonPropertyName("reportGroupId")]
  public int? ReportGroupId { get; set; }

  /// <summary>
  /// Partner groups of the partner.
  /// </summary>
  /// <value>Partner groups of the partner.</value>
  [DataMember(Name="partnerGroups", EmitDefaultValue=false)]
  [JsonPropertyName("partnerGroups")]
  public List<BusinessPartnerGroup> PartnerGroups { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class RegistryInfo {\n");
    sb.Append("  Active: ").Append(Active).Append('\n');
    sb.Append("  Phone: ").Append(Phone).Append('\n');
    sb.Append("  MobilePhone: ").Append(MobilePhone).Append('\n');
    sb.Append("  Fax: ").Append(Fax).Append('\n');
    sb.Append("  Section: ").Append(Section).Append('\n');
    sb.Append("  HomeMunicipality: ").Append(HomeMunicipality).Append('\n');
    sb.Append("  ReportGroup: ").Append(ReportGroup).Append('\n');
    sb.Append("  ReportGroupId: ").Append(ReportGroupId).Append('\n');
    sb.Append("  PartnerGroups: ").Append(PartnerGroups).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}