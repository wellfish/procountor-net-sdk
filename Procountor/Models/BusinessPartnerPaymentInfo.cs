using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Payment info of the partner.
/// </summary>
[DataContract]
public class BusinessPartnerPaymentInfo {
  /// <summary>
  /// Factoring contract ID of the partner.
  /// </summary>
  /// <value>Factoring contract ID of the partner.</value>
  [DataMember(Name="factoringContractId", EmitDefaultValue=false)]
  [JsonPropertyName("factoringContractId")]
  public int? FactoringContractId { get; set; }

  /// <summary>
  /// Payment method of the partner. The DKLMPKRE method is deprecated and read only, DOMESTIC_PAYMENT_CREDITOR should be used instead.
  /// </summary>
  /// <value>Payment method of the partner. The DKLMPKRE method is deprecated and read only, DOMESTIC_PAYMENT_CREDITOR should be used instead.</value>
  [DataMember(Name="paymentMethod", EmitDefaultValue=false)]
  [JsonPropertyName("paymentMethod")]
  public string PaymentMethod { get; set; }

  /// <summary>
  /// Bank account ID of the partner.
  /// </summary>
  /// <value>Bank account ID of the partner.</value>
  [DataMember(Name="bankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("bankAccount")]
  public string BankAccount { get; set; }

  /// <summary>
  /// Payment term days of the partner.
  /// </summary>
  /// <value>Payment term days of the partner.</value>
  [DataMember(Name="paymentTermDays", EmitDefaultValue=false)]
  [JsonPropertyName("paymentTermDays")]
  public string PaymentTermDays { get; set; }

  /// <summary>
  /// Payment term percentage of the partner.
  /// </summary>
  /// <value>Payment term percentage of the partner.</value>
  [DataMember(Name="paymentTermPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("paymentTermPercentage")]
  public decimal? PaymentTermPercentage { get; set; }

  /// <summary>
  /// Penal interest rate of the partner.
  /// </summary>
  /// <value>Penal interest rate of the partner.</value>
  [DataMember(Name="penalInterestRate", EmitDefaultValue=false)]
  [JsonPropertyName("penalInterestRate")]
  public decimal? PenalInterestRate { get; set; }

  /// <summary>
  /// Discount percentage of the partner.
  /// </summary>
  /// <value>Discount percentage of the partner.</value>
  [DataMember(Name="discountPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("discountPercentage")]
  public decimal? DiscountPercentage { get; set; }

  /// <summary>
  /// Gets or Sets CashDiscount
  /// </summary>
  [DataMember(Name="cashDiscount", EmitDefaultValue=false)]
  [JsonPropertyName("cashDiscount")]
  public CashDiscount CashDiscount { get; set; }

  /// <summary>
  /// Currency of the partner.
  /// </summary>
  /// <value>Currency of the partner.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Delivery method of the partner.
  /// </summary>
  /// <value>Delivery method of the partner.</value>
  [DataMember(Name="deliveryMethod", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryMethod")]
  public string DeliveryMethod { get; set; }

  /// <summary>
  /// Clearing code of the bank account.
  /// </summary>
  /// <value>Clearing code of the bank account.</value>
  [DataMember(Name="clearingCode", EmitDefaultValue=false)]
  [JsonPropertyName("clearingCode")]
  public string ClearingCode { get; set; }

  /// <summary>
  /// BIC of the bank account.
  /// </summary>
  /// <value>BIC of the bank account.</value>
  [DataMember(Name="bic", EmitDefaultValue=false)]
  [JsonPropertyName("bic")]
  public string Bic { get; set; }

  /// <summary>
  /// Customer account number used only for NETS payments
  /// </summary>
  /// <value>Customer account number used only for NETS payments</value>
  [DataMember(Name="customerAccountNumber", EmitDefaultValue=false)]
  [JsonPropertyName("customerAccountNumber")]
  public string CustomerAccountNumber { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerPaymentInfo {\n");
      sb.Append("  FactoringContractId: ").Append(FactoringContractId).Append('\n');
      sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append('\n');
      sb.Append("  BankAccount: ").Append(BankAccount).Append('\n');
      sb.Append("  PaymentTermDays: ").Append(PaymentTermDays).Append('\n');
      sb.Append("  PaymentTermPercentage: ").Append(PaymentTermPercentage).Append('\n');
      sb.Append("  PenalInterestRate: ").Append(PenalInterestRate).Append('\n');
      sb.Append("  DiscountPercentage: ").Append(DiscountPercentage).Append('\n');
      sb.Append("  CashDiscount: ").Append(CashDiscount).Append('\n');
      sb.Append("  Currency: ").Append(Currency).Append('\n');
      sb.Append("  DeliveryMethod: ").Append(DeliveryMethod).Append('\n');
      sb.Append("  ClearingCode: ").Append(ClearingCode).Append('\n');
      sb.Append("  Bic: ").Append(Bic).Append('\n');
      sb.Append("  CustomerAccountNumber: ").Append(CustomerAccountNumber).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}