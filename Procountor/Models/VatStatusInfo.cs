using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of available VAT statuses.
/// </summary>
[DataContract]
public class VatStatusInfo {
  /// <summary>
  /// VAT status ID
  /// </summary>
  /// <value>VAT status ID</value>
  [DataMember(Name="vatStatus", EmitDefaultValue=false)]
  [JsonPropertyName("vatStatus")]
  public int? VatStatus { get; set; }

  /// <summary>
  /// VAT status description
  /// </summary>
  /// <value>VAT status description</value>
  [DataMember(Name="description", EmitDefaultValue=false)]
  [JsonPropertyName("description")]
  public string Description { get; set; }

  /// <summary>
  /// Is VAT status allowed for sales
  /// </summary>
  /// <value>Is VAT status allowed for sales</value>
  [DataMember(Name="sales", EmitDefaultValue=false)]
  [JsonPropertyName("sales")]
  public bool? Sales { get; set; }

  /// <summary>
  /// Is VAT status allowed for purchase
  /// </summary>
  /// <value>Is VAT status allowed for purchase</value>
  [DataMember(Name="purchase", EmitDefaultValue=false)]
  [JsonPropertyName("purchase")]
  public bool? Purchase { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class VatStatusInfo {\n");
    sb.Append("  VatStatus: ").Append(VatStatus).Append('\n');
    sb.Append("  Description: ").Append(Description).Append('\n');
    sb.Append("  Sales: ").Append(Sales).Append('\n');
    sb.Append("  Purchase: ").Append(Purchase).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}