using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class VatInformation {
  /// <summary>
  /// List of available VATs in different countries.
  /// </summary>
  /// <value>List of available VATs in different countries.</value>
  [DataMember(Name="vatInformation", EmitDefaultValue=false)]
  [JsonPropertyName("vatInformation")]
  public List<VatCountryInfo> _VatInformation { get; set; }

  /// <summary>
  /// List of available VAT statuses.
  /// </summary>
  /// <value>List of available VAT statuses.</value>
  [DataMember(Name="vatStatuses", EmitDefaultValue=false)]
  [JsonPropertyName("vatStatuses")]
  public List<VatStatusInfo> VatStatuses { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class VatInformation {\n");
      sb.Append("  _VatInformation: ").Append(_VatInformation).Append('\n');
      sb.Append("  VatStatuses: ").Append(VatStatuses).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}