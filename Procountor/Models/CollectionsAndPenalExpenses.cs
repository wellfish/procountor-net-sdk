using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CollectionsAndPenalExpenses {
  /// <summary>
  /// Indicates if automatic handling of collection and penal costs is enabled.
  /// </summary>
  /// <value>Indicates if automatic handling of collection and penal costs is enabled.</value>
  [DataMember(Name="useAutomaticHandling", EmitDefaultValue=false)]
  [JsonPropertyName("useAutomaticHandling")]
  public bool? UseAutomaticHandling { get; set; }

  /// <summary>
  /// Gets or Sets CollectionCostsProduct
  /// </summary>
  [DataMember(Name="collectionCostsProduct", EmitDefaultValue=false)]
  [JsonPropertyName("collectionCostsProduct")]
  public ProductBasicInfo CollectionCostsProduct { get; set; }

  /// <summary>
  /// Gets or Sets PenalExpenseProduct
  /// </summary>
  [DataMember(Name="penalExpenseProduct", EmitDefaultValue=false)]
  [JsonPropertyName("penalExpenseProduct")]
  public ProductBasicInfo PenalExpenseProduct { get; set; }

  /// <summary>
  /// Penal expense date margin.
  /// </summary>
  /// <value>Penal expense date margin.</value>
  [DataMember(Name="penalInterestDateMargin", EmitDefaultValue=false)]
  [JsonPropertyName("penalInterestDateMargin")]
  public int? PenalInterestDateMargin { get; set; }

  /// <summary>
  /// The minimum sum of penal interest to be calculated.
  /// </summary>
  /// <value>The minimum sum of penal interest to be calculated.</value>
  [DataMember(Name="minimumPenalInterestSum", EmitDefaultValue=false)]
  [JsonPropertyName("minimumPenalInterestSum")]
  public decimal? MinimumPenalInterestSum { get; set; }

  /// <summary>
  /// Default sales invoice interest rate.
  /// </summary>
  /// <value>Default sales invoice interest rate.</value>
  [DataMember(Name="defaultSalesInvoiceInterest", EmitDefaultValue=false)]
  [JsonPropertyName("defaultSalesInvoiceInterest")]
  public decimal? DefaultSalesInvoiceInterest { get; set; }

  /// <summary>
  /// Default terms of payment for sales invoices.
  /// </summary>
  /// <value>Default terms of payment for sales invoices.</value>
  [DataMember(Name="defaultSalesInvoicePaymentTerms", EmitDefaultValue=false)]
  [JsonPropertyName("defaultSalesInvoicePaymentTerms")]
  public int? DefaultSalesInvoicePaymentTerms { get; set; }

  /// <summary>
  /// Collection partner.
  /// </summary>
  /// <value>Collection partner.</value>
  [DataMember(Name="collectionPartner", EmitDefaultValue=false)]
  [JsonPropertyName("collectionPartner")]
  public string CollectionPartner { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CollectionsAndPenalExpenses {\n");
    sb.Append("  UseAutomaticHandling: ").Append(UseAutomaticHandling).Append('\n');
    sb.Append("  CollectionCostsProduct: ").Append(CollectionCostsProduct).Append('\n');
    sb.Append("  PenalExpenseProduct: ").Append(PenalExpenseProduct).Append('\n');
    sb.Append("  PenalInterestDateMargin: ").Append(PenalInterestDateMargin).Append('\n');
    sb.Append("  MinimumPenalInterestSum: ").Append(MinimumPenalInterestSum).Append('\n');
    sb.Append("  DefaultSalesInvoiceInterest: ").Append(DefaultSalesInvoiceInterest).Append('\n');
    sb.Append("  DefaultSalesInvoicePaymentTerms: ").Append(DefaultSalesInvoicePaymentTerms).Append('\n');
    sb.Append("  CollectionPartner: ").Append(CollectionPartner).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}