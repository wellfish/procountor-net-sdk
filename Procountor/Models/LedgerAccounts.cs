using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class LedgerAccounts {
  /// <summary>
  /// List of ledger accounts.
  /// </summary>
  /// <value>List of ledger accounts.</value>
  [DataMember(Name="ledgerAccounts", EmitDefaultValue=false)]
  [JsonPropertyName("ledgerAccounts")]
  public List<LedgerAccount> _LedgerAccounts { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccounts {\n");
    sb.Append("  _LedgerAccounts: ").Append(_LedgerAccounts).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}