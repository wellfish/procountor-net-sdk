using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of identifiers of added payments.
/// </summary>
[DataContract]
public class PaymentSummary {
  /// <summary>
  /// Identifier of created payment, provided by the application.
  /// </summary>
  /// <value>Identifier of created payment, provided by the application.</value>
  [DataMember(Name="paymentId", EmitDefaultValue=false)]
  [JsonPropertyName("paymentId")]
  public int? PaymentId { get; set; }

  /// <summary>
  /// Optional identifier of requested payment object to be created. For example order number for a created direct bank transfer. Provided externally.
  /// </summary>
  /// <value>Optional identifier of requested payment object to be created. For example order number for a created direct bank transfer. Provided externally.</value>
  [DataMember(Name="customId", EmitDefaultValue=false)]
  [JsonPropertyName("customId")]
  public string CustomId { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentSummary {\n");
    sb.Append("  PaymentId: ").Append(PaymentId).Append('\n');
    sb.Append("  CustomId: ").Append(CustomId).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}