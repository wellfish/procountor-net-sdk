using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class DefaultProduct {
  /// <summary>
  /// Identifier of the default product
  /// </summary>
  /// <value>Identifier of the default product</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Gets or Sets ProductInfo
  /// </summary>
  [DataMember(Name="productInfo", EmitDefaultValue=false)]
  [JsonPropertyName("productInfo")]
  public Product ProductInfo { get; set; }

  /// <summary>
  /// Gets or Sets DefaultSettings
  /// </summary>
  [DataMember(Name="defaultSettings", EmitDefaultValue=false)]
  [JsonPropertyName("defaultSettings")]
  public DefaultProductSettings DefaultSettings { get; set; }

  /// <summary>
  /// Validity periods of the default product
  /// </summary>
  /// <value>Validity periods of the default product</value>
  [DataMember(Name="validityPeriods", EmitDefaultValue=false)]
  [JsonPropertyName("validityPeriods")]
  public List<ProductValidityPeriod> ValidityPeriods { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DefaultProduct {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  ProductInfo: ").Append(ProductInfo).Append('\n');
    sb.Append("  DefaultSettings: ").Append(DefaultSettings).Append('\n');
    sb.Append("  ValidityPeriods: ").Append(ValidityPeriods).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}