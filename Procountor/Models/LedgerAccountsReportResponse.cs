using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class LedgerAccountsReportResponse {
  /// <summary>
  /// Gets or Sets ReportParameters
  /// </summary>
  [DataMember(Name="reportParameters", EmitDefaultValue=false)]
  [JsonPropertyName("reportParameters")]
  public LedgerAccountsReportRequest ReportParameters { get; set; }

  /// <summary>
  /// Gets or Sets ReportData
  /// </summary>
  [DataMember(Name="reportData", EmitDefaultValue=false)]
  [JsonPropertyName("reportData")]
  public LedgerAccountsReportData ReportData { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccountsReportResponse {\n");
    sb.Append("  ReportParameters: ").Append(ReportParameters).Append('\n');
    sb.Append("  ReportData: ").Append(ReportData).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}