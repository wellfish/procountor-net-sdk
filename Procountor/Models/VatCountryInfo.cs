using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of available VATs in different countries.
/// </summary>
[DataContract]
public class VatCountryInfo {
  /// <summary>
  /// VAT country
  /// </summary>
  /// <value>VAT country</value>
  [DataMember(Name="country", EmitDefaultValue=false)]
  [JsonPropertyName("country")]
  public string Country { get; set; }

  /// <summary>
  /// List of VAT percentages
  /// </summary>
  /// <value>List of VAT percentages</value>
  [DataMember(Name="percentages", EmitDefaultValue=false)]
  [JsonPropertyName("percentages")]
  public List<VatInfo> Percentages { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class VatCountryInfo {\n");
      sb.Append("  Country: ").Append(Country).Append('\n');
      sb.Append("  Percentages: ").Append(Percentages).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}