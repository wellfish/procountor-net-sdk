using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class FactoringContract {
  /// <summary>
  /// Factoring contract identifier.
  /// </summary>
  /// <value>Factoring contract identifier.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Gets or Sets FinancingCompany
  /// </summary>
  [DataMember(Name="financingCompany", EmitDefaultValue=false)]
  [JsonPropertyName("financingCompany")]
  public FinancingCompany FinancingCompany { get; set; }

  /// <summary>
  /// Financing company bank account.
  /// </summary>
  /// <value>Financing company bank account.</value>
  [DataMember(Name="financingCompanyBankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("financingCompanyBankAccount")]
  public string FinancingCompanyBankAccount { get; set; }

  /// <summary>
  /// Agreegment number.
  /// </summary>
  /// <value>Agreegment number.</value>
  [DataMember(Name="contractNumber", EmitDefaultValue=false)]
  [JsonPropertyName("contractNumber")]
  public string ContractNumber { get; set; }

  /// <summary>
  /// Bank reference code separator.
  /// </summary>
  /// <value>Bank reference code separator.</value>
  [DataMember(Name="bankReferenceCodeSeparator", EmitDefaultValue=false)]
  [JsonPropertyName("bankReferenceCodeSeparator")]
  public string BankReferenceCodeSeparator { get; set; }

  /// <summary>
  /// Personal Finvoice address. 
  /// </summary>
  /// <value>Personal Finvoice address. </value>
  [DataMember(Name="personalFinvoiceAddress", EmitDefaultValue=false)]
  [JsonPropertyName("personalFinvoiceAddress")]
  public string PersonalFinvoiceAddress { get; set; }

  /// <summary>
  /// Financing company Finvoice address.
  /// </summary>
  /// <value>Financing company Finvoice address.</value>
  [DataMember(Name="financingCompanyFinvoiceAddress", EmitDefaultValue=false)]
  [JsonPropertyName("financingCompanyFinvoiceAddress")]
  public string FinancingCompanyFinvoiceAddress { get; set; }

  /// <summary>
  /// OP Factoring counter account.
  /// </summary>
  /// <value>OP Factoring counter account.</value>
  [DataMember(Name="factoringCounterAccount", EmitDefaultValue=false)]
  [JsonPropertyName("factoringCounterAccount")]
  public string FactoringCounterAccount { get; set; }

  /// <summary>
  /// Transfer notifications.
  /// </summary>
  /// <value>Transfer notifications.</value>
  [DataMember(Name="transferNotification", EmitDefaultValue=false)]
  [JsonPropertyName("transferNotification")]
  public List<TransferNotification> TransferNotification { get; set; }

  /// <summary>
  /// WS customer number.
  /// </summary>
  /// <value>WS customer number.</value>
  [DataMember(Name="wsCustomerNumber", EmitDefaultValue=false)]
  [JsonPropertyName("wsCustomerNumber")]
  public string WsCustomerNumber { get; set; }

  /// <summary>
  /// Contract status.
  /// </summary>
  /// <value>Contract status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class FactoringContract {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  FinancingCompany: ").Append(FinancingCompany).Append('\n');
    sb.Append("  FinancingCompanyBankAccount: ").Append(FinancingCompanyBankAccount).Append('\n');
    sb.Append("  ContractNumber: ").Append(ContractNumber).Append('\n');
    sb.Append("  BankReferenceCodeSeparator: ").Append(BankReferenceCodeSeparator).Append('\n');
    sb.Append("  PersonalFinvoiceAddress: ").Append(PersonalFinvoiceAddress).Append('\n');
    sb.Append("  FinancingCompanyFinvoiceAddress: ").Append(FinancingCompanyFinvoiceAddress).Append('\n');
    sb.Append("  FactoringCounterAccount: ").Append(FactoringCounterAccount).Append('\n');
    sb.Append("  TransferNotification: ").Append(TransferNotification).Append('\n');
    sb.Append("  WsCustomerNumber: ").Append(WsCustomerNumber).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}