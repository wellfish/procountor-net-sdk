using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class GeneralLedgerReportResponse {
  /// <summary>
  /// Gets or Sets ReportParameters
  /// </summary>
  [DataMember(Name="reportParameters", EmitDefaultValue=false)]
  [JsonPropertyName("reportParameters")]
  public GeneralLedgerReportRequest ReportParameters { get; set; }

  /// <summary>
  /// Gets or Sets LedgerAccount
  /// </summary>
  [DataMember(Name="ledgerAccount", EmitDefaultValue=false)]
  [JsonPropertyName("ledgerAccount")]
  public GeneralLedgerReportData LedgerAccount { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class GeneralLedgerReportResponse {\n");
    sb.Append("  ReportParameters: ").Append(ReportParameters).Append('\n');
    sb.Append("  LedgerAccount: ").Append(LedgerAccount).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}