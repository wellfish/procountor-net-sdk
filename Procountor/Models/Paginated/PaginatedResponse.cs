using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Procountor.Models.Paginated;

public sealed class PaginatedResponse<T>
{
    /// <summary>
    /// Gets or Sets Meta
    /// </summary>
    [DataMember(Name = "Meta", EmitDefaultValue = false)]
    [JsonPropertyName("Meta")]
    public SearchResultMetaData Meta { get; set; }

    /// <summary>
    /// Gets or Sets Data
    /// </summary>
    [DataMember(Name = "results", EmitDefaultValue = false)]
    [JsonPropertyName("results")]
    public List<T> Results { get; set; }
}