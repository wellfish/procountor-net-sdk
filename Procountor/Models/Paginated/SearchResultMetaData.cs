using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models.Paginated;

/// <summary>
/// Search result metadata.
/// </summary>
[DataContract]
public class SearchResultMetaData {
  /// <summary>
  /// Number of the page.
  /// </summary>
  /// <value>Number of the page.</value>
  [DataMember(Name="pageNumber", EmitDefaultValue=false)]
  [JsonPropertyName("pageNumber")]
  public int? PageNumber { get; set; }

  /// <summary>
  /// Size of the page.
  /// </summary>
  /// <value>Size of the page.</value>
  [DataMember(Name="pageSize", EmitDefaultValue=false)]
  [JsonPropertyName("pageSize")]
  public int? PageSize { get; set; }

  /// <summary>
  /// The number of results per page.
  /// </summary>
  /// <value>The number of results per page.</value>
  [DataMember(Name="resultCount", EmitDefaultValue=false)]
  [JsonPropertyName("resultCount")]
  public int? ResultCount { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class SearchResultMetaData {\n");
    sb.Append("  PageNumber: ").Append(PageNumber).Append('\n');
    sb.Append("  PageSize: ").Append(PageSize).Append('\n');
    sb.Append("  ResultCount: ").Append(ResultCount).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}