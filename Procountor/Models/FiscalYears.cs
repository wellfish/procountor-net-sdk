using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class FiscalYears {
  /// <summary>
  /// List of fiscal years.
  /// </summary>
  /// <value>List of fiscal years.</value>
  [DataMember(Name="fiscalYears", EmitDefaultValue=false)]
  [JsonPropertyName("fiscalYears")]
  public List<FiscalYear> _FiscalYears { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class FiscalYears {\n");
    sb.Append("  _FiscalYears: ").Append(_FiscalYears).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}