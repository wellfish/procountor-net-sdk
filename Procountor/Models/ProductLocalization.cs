using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Product name localizations.
/// </summary>
[DataContract]
public class ProductLocalization {
  /// <summary>
  /// Product language
  /// </summary>
  /// <value>Product language</value>
  [DataMember(Name="language", EmitDefaultValue=false)]
  [JsonPropertyName("language")]
  public string Language { get; set; }

  /// <summary>
  /// Product name
  /// </summary>
  /// <value>Product name</value>
  [DataMember(Name="productName", EmitDefaultValue=false)]
  [JsonPropertyName("productName")]
  public string ProductName { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ProductLocalization {\n");
    sb.Append("  Language: ").Append(Language).Append('\n');
    sb.Append("  ProductName: ").Append(ProductName).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}