using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Report data.
/// </summary>
[DataContract]
public class LedgerAccountsReportData {
  /// <summary>
  /// List of report periods.
  /// </summary>
  /// <value>List of report periods.</value>
  [DataMember(Name="periods", EmitDefaultValue=false)]
  [JsonPropertyName("periods")]
  public List<LedgerAccountsReportPeriod> Periods { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccountsReportData {\n");
    sb.Append("  Periods: ").Append(Periods).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}