using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// User rights in environment
/// </summary>
[DataContract]
public class UserRights {
  /// <summary>
  /// Gets or Sets Management
  /// </summary>
  [DataMember(Name="management", EmitDefaultValue=false)]
  [JsonPropertyName("management")]
  public ManagementRights Management { get; set; }

  /// <summary>
  /// Gets or Sets Sales
  /// </summary>
  [DataMember(Name="sales", EmitDefaultValue=false)]
  [JsonPropertyName("sales")]
  public SalesRights Sales { get; set; }

  /// <summary>
  /// Gets or Sets Purchases
  /// </summary>
  [DataMember(Name="purchases", EmitDefaultValue=false)]
  [JsonPropertyName("purchases")]
  public PurchasesRights Purchases { get; set; }

  /// <summary>
  /// Gets or Sets Salaries
  /// </summary>
  [DataMember(Name="salaries", EmitDefaultValue=false)]
  [JsonPropertyName("salaries")]
  public SalariesRights Salaries { get; set; }

  /// <summary>
  /// Gets or Sets PaymentTransactions
  /// </summary>
  [DataMember(Name="paymentTransactions", EmitDefaultValue=false)]
  [JsonPropertyName("paymentTransactions")]
  public PaymentTransactionsRights PaymentTransactions { get; set; }

  /// <summary>
  /// Gets or Sets FinancialManagement
  /// </summary>
  [DataMember(Name="financialManagement", EmitDefaultValue=false)]
  [JsonPropertyName("financialManagement")]
  public FinancialManagementRights FinancialManagement { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class UserRights {\n");
    sb.Append("  Management: ").Append(Management).Append('\n');
    sb.Append("  Sales: ").Append(Sales).Append('\n');
    sb.Append("  Purchases: ").Append(Purchases).Append('\n');
    sb.Append("  Salaries: ").Append(Salaries).Append('\n');
    sb.Append("  PaymentTransactions: ").Append(PaymentTransactions).Append('\n');
    sb.Append("  FinancialManagement: ").Append(FinancialManagement).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}