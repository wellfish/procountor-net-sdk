using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CheckingEventDTO {
  /// <summary>
  /// Comment for verification or approval event.
  /// </summary>
  /// <value>Comment for verification or approval event.</value>
  [DataMember(Name="comment", EmitDefaultValue=false)]
  [JsonPropertyName("comment")]
  public string Comment { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CheckingEventDTO {\n");
    sb.Append("  Comment: ").Append(Comment).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}