using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class AccountingReportRequest {
  /// <summary>
  /// Report start date. Optional, if missing, starting date of the tracking period 'endDate' is in will be used. (financial year if not available)
  /// </summary>
  /// <value>Report start date. Optional, if missing, starting date of the tracking period 'endDate' is in will be used. (financial year if not available)</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// Report end date.
  /// </summary>
  /// <value>Report end date.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// List of receipt statuses for report. Default value depends on company rights.
  /// </summary>
  /// <value>List of receipt statuses for report. Default value depends on company rights.</value>
  [DataMember(Name="receiptStatus", EmitDefaultValue=false)]
  [JsonPropertyName("receiptStatus")]
  public List<string> ReceiptStatus { get; set; }

  /// <summary>
  /// Report type.
  /// </summary>
  /// <value>Report type.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Gets or Sets Options
  /// </summary>
  [DataMember(Name="options", EmitDefaultValue=false)]
  [JsonPropertyName("options")]
  public AccountingReportRequestOptions Options { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class AccountingReportRequest {\n");
    sb.Append("  StartDate: ").Append(StartDate).Append('\n');
    sb.Append("  EndDate: ").Append(EndDate).Append('\n');
    sb.Append("  ReceiptStatus: ").Append(ReceiptStatus).Append('\n');
    sb.Append("  Type: ").Append(Type).Append('\n');
    sb.Append("  Options: ").Append(Options).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}