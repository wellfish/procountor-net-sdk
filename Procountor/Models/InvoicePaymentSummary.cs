using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of identifiers of added payments.
/// </summary>
[DataContract]
public class InvoicePaymentSummary {
  /// <summary>
  /// Identifier of created payment, provided by the application.
  /// </summary>
  /// <value>Identifier of created payment, provided by the application.</value>
  [DataMember(Name="paymentId", EmitDefaultValue=false)]
  [JsonPropertyName("paymentId")]
  public int? PaymentId { get; set; }

  /// <summary>
  /// Identifier of invoice associated with the payment.
  /// </summary>
  /// <value>Identifier of invoice associated with the payment.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoicePaymentSummary {\n");
    sb.Append("  PaymentId: ").Append(PaymentId).Append('\n');
    sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}