using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class VatPercentages {
  /// <summary>
  /// List of available VAT percentages.
  /// </summary>
  /// <value>List of available VAT percentages.</value>
  [DataMember(Name="vatPercentages", EmitDefaultValue=false)]
  [JsonPropertyName("vatPercentages")]
  public List<double?> _VatPercentages { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class VatPercentages {\n");
    sb.Append("  _VatPercentages: ").Append(_VatPercentages).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}