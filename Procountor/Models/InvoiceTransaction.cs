using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of invoice transactions.
/// </summary>
[DataContract]
public class InvoiceTransaction {
  /// <summary>
  /// Transaction action.
  /// </summary>
  /// <value>Transaction action.</value>
  [DataMember(Name="action", EmitDefaultValue=false)]
  [JsonPropertyName("action")]
  public string Action { get; set; }

  /// <summary>
  /// Transaction name.
  /// </summary>
  /// <value>Transaction name.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Transaction date.
  /// </summary>
  /// <value>Transaction date.</value>
  [DataMember(Name="date", EmitDefaultValue=false)]
  [JsonPropertyName("date")]
  public DateTime? Date { get; set; }

  /// <summary>
  /// Transaction details.
  /// </summary>
  /// <value>Transaction details.</value>
  [DataMember(Name="details", EmitDefaultValue=false)]
  [JsonPropertyName("details")]
  public string Details { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceTransaction {\n");
    sb.Append("  Action: ").Append(Action).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Date: ").Append(Date).Append('\n');
    sb.Append("  Details: ").Append(Details).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}