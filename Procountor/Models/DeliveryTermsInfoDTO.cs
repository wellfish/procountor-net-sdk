using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoice delivery terms info.
/// </summary>
[DataContract]
public class DeliveryTermsInfoDTO {
  /// <summary>
  /// Name for terms of delivery.
  /// </summary>
  /// <value>Name for terms of delivery.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Delivery place
  /// </summary>
  /// <value>Delivery place</value>
  [DataMember(Name="municipality", EmitDefaultValue=false)]
  [JsonPropertyName("municipality")]
  public string Municipality { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DeliveryTermsInfoDTO {\n");
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Municipality: ").Append(Municipality).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}