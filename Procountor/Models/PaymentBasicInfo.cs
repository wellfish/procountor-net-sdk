using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of the payment IDs
/// </summary>
[DataContract]
public class PaymentBasicInfo {
  /// <summary>
  /// Id of the payment
  /// </summary>
  /// <value>Id of the payment</value>
  [DataMember(Name="paymentId", EmitDefaultValue=false)]
  [JsonPropertyName("paymentId")]
  public int? PaymentId { get; set; }

  /// <summary>
  /// Custom Id of the payment
  /// </summary>
  /// <value>Custom Id of the payment</value>
  [DataMember(Name="customId", EmitDefaultValue=false)]
  [JsonPropertyName("customId")]
  public string CustomId { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentBasicInfo {\n");
    sb.Append("  PaymentId: ").Append(PaymentId).Append('\n');
    sb.Append("  CustomId: ").Append(CustomId).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}