using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class DeliveryTerms {
  /// <summary>
  /// List of delivery terms.
  /// </summary>
  /// <value>List of delivery terms.</value>
  [DataMember(Name="deliveryTerms", EmitDefaultValue=false)]
  [JsonPropertyName("deliveryTerms")]
  public List<string> _DeliveryTerms { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DeliveryTerms {\n");
    sb.Append("  _DeliveryTerms: ").Append(_DeliveryTerms).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}