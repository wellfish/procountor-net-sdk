using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class SessionInfo {
  /// <summary>
  /// ID of current company.
  /// </summary>
  /// <value>ID of current company.</value>
  [DataMember(Name="companyId", EmitDefaultValue=false)]
  [JsonPropertyName("companyId")]
  public int? CompanyId { get; set; }

  /// <summary>
  /// Name of current company.
  /// </summary>
  /// <value>Name of current company.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Country of current company.
  /// </summary>
  /// <value>Country of current company.</value>
  [DataMember(Name="country", EmitDefaultValue=false)]
  [JsonPropertyName("country")]
  public string Country { get; set; }

  /// <summary>
  /// Product version of current company.
  /// </summary>
  /// <value>Product version of current company.</value>
  [DataMember(Name="productVersion", EmitDefaultValue=false)]
  [JsonPropertyName("productVersion")]
  public string ProductVersion { get; set; }

  /// <summary>
  /// Current user id.
  /// </summary>
  /// <value>Current user id.</value>
  [DataMember(Name="userId", EmitDefaultValue=false)]
  [JsonPropertyName("userId")]
  public int? UserId { get; set; }

  /// <summary>
  /// Current user name.
  /// </summary>
  /// <value>Current user name.</value>
  [DataMember(Name="userName", EmitDefaultValue=false)]
  [JsonPropertyName("userName")]
  public string UserName { get; set; }

  /// <summary>
  /// Gets or Sets CompanySettings
  /// </summary>
  [DataMember(Name="companySettings", EmitDefaultValue=false)]
  [JsonPropertyName("companySettings")]
  public CompanySettings CompanySettings { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class SessionInfo {\n");
    sb.Append("  CompanyId: ").Append(CompanyId).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Country: ").Append(Country).Append('\n');
    sb.Append("  ProductVersion: ").Append(ProductVersion).Append('\n');
    sb.Append("  UserId: ").Append(UserId).Append('\n');
    sb.Append("  UserName: ").Append(UserName).Append('\n');
    sb.Append("  CompanySettings: ").Append(CompanySettings).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}