using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoicing info data of the partner.
/// </summary>
[DataContract]
public class BasicInvoicingInfo {
  /// <summary>
  /// Customer number of the partner.
  /// </summary>
  /// <value>Customer number of the partner.</value>
  [DataMember(Name="customerNumber", EmitDefaultValue=false)]
  [JsonPropertyName("customerNumber")]
  public string CustomerNumber { get; set; }

  /// <summary>
  /// Identifier of the partner.
  /// </summary>
  /// <value>Identifier of the partner.</value>
  [DataMember(Name="identifier", EmitDefaultValue=false)]
  [JsonPropertyName("identifier")]
  public string Identifier { get; set; }

  /// <summary>
  /// Identifier type of the partner.
  /// </summary>
  /// <value>Identifier type of the partner.</value>
  [DataMember(Name="identifierType", EmitDefaultValue=false)]
  [JsonPropertyName("identifierType")]
  public string IdentifierType { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BasicInvoicingInfo {\n");
      sb.Append("  CustomerNumber: ").Append(CustomerNumber).Append('\n');
      sb.Append("  Identifier: ").Append(Identifier).Append('\n');
      sb.Append("  IdentifierType: ").Append(IdentifierType).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}