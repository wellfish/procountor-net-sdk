using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoice operator related information. It is a read-only property shown only for purchase invoices.
/// </summary>
[DataContract]
public class InvoiceOperatorInfo {
  /// <summary>
  /// Invoice operator.
  /// </summary>
  /// <value>Invoice operator.</value>
  [DataMember(Name="operator", EmitDefaultValue=false)]
  [JsonPropertyName("operator")]
  public string _Operator { get; set; }

  /// <summary>
  /// Receiving address of operator.
  /// </summary>
  /// <value>Receiving address of operator.</value>
  [DataMember(Name="receivingAddress", EmitDefaultValue=false)]
  [JsonPropertyName("receivingAddress")]
  public string ReceivingAddress { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceOperatorInfo {\n");
    sb.Append("  _Operator: ").Append(_Operator).Append('\n');
    sb.Append("  ReceivingAddress: ").Append(ReceivingAddress).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}