using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Payment information for the person.
/// </summary>
[DataContract]
public class BusinessPartnerPaymentDetails {
  /// <summary>
  /// Payment method.
  /// </summary>
  /// <value>Payment method.</value>
  [DataMember(Name="paymentMethod", EmitDefaultValue=false)]
  [JsonPropertyName("paymentMethod")]
  public string PaymentMethod { get; set; }

  /// <summary>
  /// Default currency.
  /// </summary>
  /// <value>Default currency.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Gets or Sets BankAccount
  /// </summary>
  [DataMember(Name="bankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("bankAccount")]
  public BankAccount BankAccount { get; set; }

  /// <summary>
  /// Payment term days.
  /// </summary>
  /// <value>Payment term days.</value>
  [DataMember(Name="paymentTermDays", EmitDefaultValue=false)]
  [JsonPropertyName("paymentTermDays")]
  public string PaymentTermDays { get; set; }

  /// <summary>
  /// Payment term percentage.
  /// </summary>
  /// <value>Payment term percentage.</value>
  [DataMember(Name="paymentTermPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("paymentTermPercentage")]
  public decimal? PaymentTermPercentage { get; set; }

  /// <summary>
  /// Penal interest rate.
  /// </summary>
  /// <value>Penal interest rate.</value>
  [DataMember(Name="penalInterestRate", EmitDefaultValue=false)]
  [JsonPropertyName("penalInterestRate")]
  public decimal? PenalInterestRate { get; set; }

  /// <summary>
  /// Discount percentage.
  /// </summary>
  /// <value>Discount percentage.</value>
  [DataMember(Name="discountPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("discountPercentage")]
  public decimal? DiscountPercentage { get; set; }

  /// <summary>
  /// Clearing code
  /// </summary>
  /// <value>Clearing code</value>
  [DataMember(Name="clearingCode", EmitDefaultValue=false)]
  [JsonPropertyName("clearingCode")]
  public string ClearingCode { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerPaymentDetails {\n");
      sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append('\n');
      sb.Append("  Currency: ").Append(Currency).Append('\n');
      sb.Append("  BankAccount: ").Append(BankAccount).Append('\n');
      sb.Append("  PaymentTermDays: ").Append(PaymentTermDays).Append('\n');
      sb.Append("  PaymentTermPercentage: ").Append(PaymentTermPercentage).Append('\n');
      sb.Append("  PenalInterestRate: ").Append(PenalInterestRate).Append('\n');
      sb.Append("  DiscountPercentage: ").Append(DiscountPercentage).Append('\n');
      sb.Append("  ClearingCode: ").Append(ClearingCode).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}