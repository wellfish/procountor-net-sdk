using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of tracking periods contained in the fiscal year.
/// </summary>
[DataContract]
public class TrackingPeriod {
  /// <summary>
  /// Unique identifier of the tracking period.
  /// </summary>
  /// <value>Unique identifier of the tracking period.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Start date of the tracking period.
  /// </summary>
  /// <value>Start date of the tracking period.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the tracking period.
  /// </summary>
  /// <value>End date of the tracking period.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Status of the tracking period.
  /// </summary>
  /// <value>Status of the tracking period.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Time of last edition of tracking period.
  /// </summary>
  /// <value>Time of last edition of tracking period.</value>
  [DataMember(Name="modified", EmitDefaultValue=false)]
  [JsonPropertyName("modified")]
  public DateTime? Modified { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TrackingPeriod {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  StartDate: ").Append(StartDate).Append('\n');
      sb.Append("  EndDate: ").Append(EndDate).Append('\n');
      sb.Append("  Status: ").Append(Status).Append('\n');
      sb.Append("  Modified: ").Append(Modified).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}