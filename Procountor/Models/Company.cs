using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class Company {
  /// <summary>
  /// ID of current company.
  /// </summary>
  /// <value>ID of current company.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Name of current company.
  /// </summary>
  /// <value>Name of current company.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Country of current company.
  /// </summary>
  /// <value>Country of current company.</value>
  [DataMember(Name="country", EmitDefaultValue=false)]
  [JsonPropertyName("country")]
  public string Country { get; set; }

  /// <summary>
  /// Product version of current company.
  /// </summary>
  /// <value>Product version of current company.</value>
  [DataMember(Name="productVersion", EmitDefaultValue=false)]
  [JsonPropertyName("productVersion")]
  public string ProductVersion { get; set; }

  /// <summary>
  /// Operation type of current company.
  /// </summary>
  /// <value>Operation type of current company.</value>
  [DataMember(Name="operationType", EmitDefaultValue=false)]
  [JsonPropertyName("operationType")]
  public string OperationType { get; set; }

  /// <summary>
  /// Accounting name of current company.
  /// </summary>
  /// <value>Accounting name of current company.</value>
  [DataMember(Name="accountingOfficeName", EmitDefaultValue=false)]
  [JsonPropertyName("accountingOfficeName")]
  public string AccountingOfficeName { get; set; }

  /// <summary>
  /// Accounting office identifier of current company.
  /// </summary>
  /// <value>Accounting office identifier of current company.</value>
  [DataMember(Name="accountingOfficeId", EmitDefaultValue=false)]
  [JsonPropertyName("accountingOfficeId")]
  public int? AccountingOfficeId { get; set; }

  /// <summary>
  /// Gets or Sets CompanyAddress
  /// </summary>
  [DataMember(Name="companyAddress", EmitDefaultValue=false)]
  [JsonPropertyName("companyAddress")]
  public CompanyAddress CompanyAddress { get; set; }

  /// <summary>
  /// Gets or Sets BillingAddress
  /// </summary>
  [DataMember(Name="billingAddress", EmitDefaultValue=false)]
  [JsonPropertyName("billingAddress")]
  public CompanyBillingAddress BillingAddress { get; set; }

  /// <summary>
  /// MVA of current company.
  /// </summary>
  /// <value>MVA of current company.</value>
  [DataMember(Name="mva", EmitDefaultValue=false)]
  [JsonPropertyName("mva")]
  public bool? Mva { get; set; }

  /// <summary>
  /// Is the current company in trade register.
  /// </summary>
  /// <value>Is the current company in trade register.</value>
  [DataMember(Name="inTradeRegister", EmitDefaultValue=false)]
  [JsonPropertyName("inTradeRegister")]
  public bool? InTradeRegister { get; set; }

  /// <summary>
  /// Gets or Sets BusinessIdentifier
  /// </summary>
  [DataMember(Name="businessIdentifier", EmitDefaultValue=false)]
  [JsonPropertyName("businessIdentifier")]
  public BusinessIdentifier BusinessIdentifier { get; set; }

  /// <summary>
  /// Languages supported in coa for given environment.
  /// </summary>
  /// <value>Languages supported in coa for given environment.</value>
  [DataMember(Name="accountingCoaLanguages", EmitDefaultValue=false)]
  [JsonPropertyName("accountingCoaLanguages")]
  public List<string> AccountingCoaLanguages { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class Company {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Country: ").Append(Country).Append('\n');
    sb.Append("  ProductVersion: ").Append(ProductVersion).Append('\n');
    sb.Append("  OperationType: ").Append(OperationType).Append('\n');
    sb.Append("  AccountingOfficeName: ").Append(AccountingOfficeName).Append('\n');
    sb.Append("  AccountingOfficeId: ").Append(AccountingOfficeId).Append('\n');
    sb.Append("  CompanyAddress: ").Append(CompanyAddress).Append('\n');
    sb.Append("  BillingAddress: ").Append(BillingAddress).Append('\n');
    sb.Append("  Mva: ").Append(Mva).Append('\n');
    sb.Append("  InTradeRegister: ").Append(InTradeRegister).Append('\n');
    sb.Append("  BusinessIdentifier: ").Append(BusinessIdentifier).Append('\n');
    sb.Append("  AccountingCoaLanguages: ").Append(AccountingCoaLanguages).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}