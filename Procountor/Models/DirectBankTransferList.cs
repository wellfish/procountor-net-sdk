using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class DirectBankTransferList {
  /// <summary>
  /// List containing DirectBankTransfer objects.
  /// </summary>
  /// <value>List containing DirectBankTransfer objects.</value>
  [DataMember(Name="directBankTransfers", EmitDefaultValue=false)]
  [JsonPropertyName("directBankTransfers")]
  public List<DirectBankTransfer> DirectBankTransfers { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DirectBankTransferList {\n");
    sb.Append("  DirectBankTransfers: ").Append(DirectBankTransfers).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}