using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Aliases for the bank account if any exists in the given environment. Used on Swedish and Danish environments.
/// </summary>
[DataContract]
public class BankAccountAlias {
  /// <summary>
  /// Type of associated account number.
  /// </summary>
  /// <value>Type of associated account number.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Associated account number.
  /// </summary>
  /// <value>Associated account number.</value>
  [DataMember(Name="accountNumber", EmitDefaultValue=false)]
  [JsonPropertyName("accountNumber")]
  public string AccountNumber { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BankAccountAlias {\n");
      sb.Append("  Type: ").Append(Type).Append('\n');
      sb.Append("  AccountNumber: ").Append(AccountNumber).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}