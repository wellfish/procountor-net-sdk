using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Checkers are supported for PURCHASE_INVOICE, TRAVEL_INVOICE, BILL_OF_CHARGES
/// </summary>
[DataContract]
public class InvoiceCheckerInformation {
  /// <summary>
  /// User ID of the given checker.If invoice checker is given then this field is required
  /// </summary>
  /// <value>User ID of the given checker.If invoice checker is given then this field is required</value>
  [DataMember(Name="userId", EmitDefaultValue=false)]
  [JsonPropertyName("userId")]
  public int? UserId { get; set; }

  /// <summary>
  /// Time when event was performed. Not present if event has not been performed.
  /// </summary>
  /// <value>Time when event was performed. Not present if event has not been performed.</value>
  [DataMember(Name="eventPerformed", EmitDefaultValue=false)]
  [JsonPropertyName("eventPerformed")]
  public DateTime? EventPerformed { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceCheckerInformation {\n");
    sb.Append("  UserId: ").Append(UserId).Append('\n');
    sb.Append("  EventPerformed: ").Append(EventPerformed).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}