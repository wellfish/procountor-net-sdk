using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class PaymentGroup {
  /// <summary>
  /// Id of the payment list.
  /// </summary>
  /// <value>Id of the payment list.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// List name.
  /// </summary>
  /// <value>List name.</value>
  [DataMember(Name="listName", EmitDefaultValue=false)]
  [JsonPropertyName("listName")]
  public string ListName { get; set; }

  /// <summary>
  /// List creation time.
  /// </summary>
  /// <value>List creation time.</value>
  [DataMember(Name="created", EmitDefaultValue=false)]
  [JsonPropertyName("created")]
  public DateTime? Created { get; set; }

  /// <summary>
  /// Payment status.
  /// </summary>
  /// <value>Payment status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Payment method.
  /// </summary>
  /// <value>Payment method.</value>
  [DataMember(Name="paymentMethod", EmitDefaultValue=false)]
  [JsonPropertyName("paymentMethod")]
  public string PaymentMethod { get; set; }

  /// <summary>
  /// Date of the payment
  /// </summary>
  /// <value>Date of the payment</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public DateTime? PaymentDate { get; set; }

  /// <summary>
  /// Bank account number of the payer
  /// </summary>
  /// <value>Bank account number of the payer</value>
  [DataMember(Name="payerBankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("payerBankAccount")]
  public string PayerBankAccount { get; set; }

  /// <summary>
  /// Total sum of the payments
  /// </summary>
  /// <value>Total sum of the payments</value>
  [DataMember(Name="totalSum", EmitDefaultValue=false)]
  [JsonPropertyName("totalSum")]
  public decimal? TotalSum { get; set; }

  /// <summary>
  /// Custom identifier of the payment list.
  /// </summary>
  /// <value>Custom identifier of the payment list.</value>
  [DataMember(Name="customId", EmitDefaultValue=false)]
  [JsonPropertyName("customId")]
  public string CustomId { get; set; }

  /// <summary>
  /// List of the payment IDs
  /// </summary>
  /// <value>List of the payment IDs</value>
  [DataMember(Name="payments", EmitDefaultValue=false)]
  [JsonPropertyName("payments")]
  public List<PaymentBasicInfo> Payments { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentGroup {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  ListName: ").Append(ListName).Append('\n');
    sb.Append("  Created: ").Append(Created).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  PayerBankAccount: ").Append(PayerBankAccount).Append('\n');
    sb.Append("  TotalSum: ").Append(TotalSum).Append('\n');
    sb.Append("  CustomId: ").Append(CustomId).Append('\n');
    sb.Append("  Payments: ").Append(Payments).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}