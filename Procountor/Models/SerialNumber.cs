using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Ledger receipt serial number. This field is optional and can only appear for Swedish and Danish environments.
/// </summary>
[DataContract]
public class SerialNumber {
  /// <summary>
  /// Ledger receipt series.
  /// </summary>
  /// <value>Ledger receipt series.</value>
  [DataMember(Name="series", EmitDefaultValue=false)]
  [JsonPropertyName("series")]
  public string Series { get; set; }

  /// <summary>
  /// Ledger receipt serial number.
  /// </summary>
  /// <value>Ledger receipt serial number.</value>
  [DataMember(Name="number", EmitDefaultValue=false)]
  [JsonPropertyName("number")]
  public int? Number { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class SerialNumber {\n");
      sb.Append("  Series: ").Append(Series).Append('\n');
      sb.Append("  Number: ").Append(Number).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}