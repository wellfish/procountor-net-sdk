using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Report period rows.
/// </summary>
[DataContract]
public class LedgerAccountsReportRow {
  /// <summary>
  /// Name of the row.
  /// </summary>
  /// <value>Name of the row.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Value of the row.
  /// </summary>
  /// <value>Value of the row.</value>
  [DataMember(Name="value", EmitDefaultValue=false)]
  [JsonPropertyName("value")]
  public decimal? Value { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccountsReportRow {\n");
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Value: ").Append(Value).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}