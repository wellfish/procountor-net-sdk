using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Financing company information.
/// </summary>
[DataContract]
public class FinancingCompany {
  /// <summary>
  /// Financing company code
  /// </summary>
  /// <value>Financing company code</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public string Id { get; set; }

  /// <summary>
  /// Financing company name
  /// </summary>
  /// <value>Financing company name</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class FinancingCompany {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}