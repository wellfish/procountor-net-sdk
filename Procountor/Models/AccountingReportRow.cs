using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Accounting report period rows.
/// </summary>
[DataContract]
public class AccountingReportRow {
  /// <summary>
  /// Id of the accounting report row.
  /// </summary>
  /// <value>Id of the accounting report row.</value>
  [DataMember(Name="rowId", EmitDefaultValue=false)]
  [JsonPropertyName("rowId")]
  public int? RowId { get; set; }

  /// <summary>
  /// Type of the accounting report row.
  /// </summary>
  /// <value>Type of the accounting report row.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Name of the accounting row.
  /// </summary>
  /// <value>Name of the accounting row.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Value of the accounting row.
  /// </summary>
  /// <value>Value of the accounting row.</value>
  [DataMember(Name="value", EmitDefaultValue=false)]
  [JsonPropertyName("value")]
  public decimal? Value { get; set; }

  /// <summary>
  /// Accounting row function formula.
  /// </summary>
  /// <value>Accounting row function formula.</value>
  [DataMember(Name="formula", EmitDefaultValue=false)]
  [JsonPropertyName("formula")]
  public string Formula { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountingReportRow {\n");
      sb.Append("  RowId: ").Append(RowId).Append('\n');
      sb.Append("  Type: ").Append(Type).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  Value: ").Append(Value).Append('\n');
      sb.Append("  Formula: ").Append(Formula).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}