using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contact persons of the partner.
/// </summary>
[DataContract]
public class BusinessPartnerContactPerson {
  /// <summary>
  /// Contact person ID.
  /// </summary>
  /// <value>Contact person ID.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Name of contact person.
  /// </summary>
  /// <value>Name of contact person.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Email of contact person.
  /// </summary>
  /// <value>Email of contact person.</value>
  [DataMember(Name="email", EmitDefaultValue=false)]
  [JsonPropertyName("email")]
  public string Email { get; set; }

  /// <summary>
  /// Phone of contact person.
  /// </summary>
  /// <value>Phone of contact person.</value>
  [DataMember(Name="phone", EmitDefaultValue=false)]
  [JsonPropertyName("phone")]
  public string Phone { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerContactPerson {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  Email: ").Append(Email).Append('\n');
      sb.Append("  Phone: ").Append(Phone).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}