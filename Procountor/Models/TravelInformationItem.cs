using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Travel information items. A travel invoice may have one or more travel information items containing departure date, return date, destinations and travel purpose.
/// </summary>
[DataContract]
public class TravelInformationItem {
  /// <summary>
  /// Travel departure date. Free text.
  /// </summary>
  /// <value>Travel departure date. Free text.</value>
  [DataMember(Name="departure", EmitDefaultValue=false)]
  [JsonPropertyName("departure")]
  public string Departure { get; set; }

  /// <summary>
  /// Travel return date. Free text.
  /// </summary>
  /// <value>Travel return date. Free text.</value>
  [DataMember(Name="arrival", EmitDefaultValue=false)]
  [JsonPropertyName("arrival")]
  public string Arrival { get; set; }

  /// <summary>
  /// Travel destinations. Free text.
  /// </summary>
  /// <value>Travel destinations. Free text.</value>
  [DataMember(Name="places", EmitDefaultValue=false)]
  [JsonPropertyName("places")]
  public string Places { get; set; }

  /// <summary>
  /// Travel purpose. Free text.
  /// </summary>
  /// <value>Travel purpose. Free text.</value>
  [DataMember(Name="purpose", EmitDefaultValue=false)]
  [JsonPropertyName("purpose")]
  public string Purpose { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class TravelInformationItem {\n");
    sb.Append("  Departure: ").Append(Departure).Append('\n');
    sb.Append("  Arrival: ").Append(Arrival).Append('\n');
    sb.Append("  Places: ").Append(Places).Append('\n');
    sb.Append("  Purpose: ").Append(Purpose).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}