using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of substitutions.
/// </summary>
[DataContract]
public class Substitution {
  /// <summary>
  /// Substituted user.
  /// </summary>
  /// <value>Substituted user.</value>
  [DataMember(Name="userId", EmitDefaultValue=false)]
  [JsonPropertyName("userId")]
  public long? UserId { get; set; }

  /// <summary>
  /// Start date of substitution.
  /// </summary>
  /// <value>Start date of substitution.</value>
  [DataMember(Name="start", EmitDefaultValue=false)]
  [JsonPropertyName("start")]
  public DateTime? Start { get; set; }

  /// <summary>
  /// End date of substitution.
  /// </summary>
  /// <value>End date of substitution.</value>
  [DataMember(Name="end", EmitDefaultValue=false)]
  [JsonPropertyName("end")]
  public DateTime? End { get; set; }

  /// <summary>
  /// User substituting as verifier.
  /// </summary>
  /// <value>User substituting as verifier.</value>
  [DataMember(Name="substituteForVerifying", EmitDefaultValue=false)]
  [JsonPropertyName("substituteForVerifying")]
  public int? SubstituteForVerifying { get; set; }

  /// <summary>
  /// User substituting as approver.
  /// </summary>
  /// <value>User substituting as approver.</value>
  [DataMember(Name="substituteForApproving", EmitDefaultValue=false)]
  [JsonPropertyName("substituteForApproving")]
  public int? SubstituteForApproving { get; set; }

  /// <summary>
  /// Additional notes for substitution.
  /// </summary>
  /// <value>Additional notes for substitution.</value>
  [DataMember(Name="notes", EmitDefaultValue=false)]
  [JsonPropertyName("notes")]
  public string Notes { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class Substitution {\n");
    sb.Append("  UserId: ").Append(UserId).Append('\n');
    sb.Append("  Start: ").Append(Start).Append('\n');
    sb.Append("  End: ").Append(End).Append('\n');
    sb.Append("  SubstituteForVerifying: ").Append(SubstituteForVerifying).Append('\n');
    sb.Append("  SubstituteForApproving: ").Append(SubstituteForApproving).Append('\n');
    sb.Append("  Notes: ").Append(Notes).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}