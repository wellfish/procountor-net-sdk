using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CurrencyGroupRates {
  /// <summary>
  /// Gets or Sets BaseCurrency
  /// </summary>
  [DataMember(Name="baseCurrency", EmitDefaultValue=false)]
  [JsonPropertyName("baseCurrency")]
  public string BaseCurrency { get; set; }

  /// <summary>
  /// Gets or Sets CurrencyDate
  /// </summary>
  [DataMember(Name="currencyDate", EmitDefaultValue=false)]
  [JsonPropertyName("currencyDate")]
  public DateTime? CurrencyDate { get; set; }

  /// <summary>
  /// Gets or Sets Rates
  /// </summary>
  [DataMember(Name="rates", EmitDefaultValue=false)]
  [JsonPropertyName("rates")]
  public List<CurrencyRate> Rates { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CurrencyGroupRates {\n");
    sb.Append("  BaseCurrency: ").Append(BaseCurrency).Append('\n');
    sb.Append("  CurrencyDate: ").Append(CurrencyDate).Append('\n');
    sb.Append("  Rates: ").Append(Rates).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}