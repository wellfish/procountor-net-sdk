using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Default settings of the default product
/// </summary>
[DataContract]
public class DefaultProductSettings {
  /// <summary>
  /// Information whether special discount is in use
  /// </summary>
  /// <value>Information whether special discount is in use</value>
  [DataMember(Name="useSpecialDiscount", EmitDefaultValue=false)]
  [JsonPropertyName("useSpecialDiscount")]
  public bool? UseSpecialDiscount { get; set; }

  /// <summary>
  /// Percentage special discount
  /// </summary>
  /// <value>Percentage special discount</value>
  [DataMember(Name="specialDiscountPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("specialDiscountPercentage")]
  public decimal? SpecialDiscountPercentage { get; set; }

  /// <summary>
  /// Information whether special price is in use
  /// </summary>
  /// <value>Information whether special price is in use</value>
  [DataMember(Name="useSpecialPrice", EmitDefaultValue=false)]
  [JsonPropertyName("useSpecialPrice")]
  public bool? UseSpecialPrice { get; set; }

  /// <summary>
  /// Special price of the product
  /// </summary>
  /// <value>Special price of the product</value>
  [DataMember(Name="specialPrice", EmitDefaultValue=false)]
  [JsonPropertyName("specialPrice")]
  public decimal? SpecialPrice { get; set; }

  /// <summary>
  /// Information whether special vat is in use
  /// </summary>
  /// <value>Information whether special vat is in use</value>
  [DataMember(Name="useSpecialVat", EmitDefaultValue=false)]
  [JsonPropertyName("useSpecialVat")]
  public bool? UseSpecialVat { get; set; }

  /// <summary>
  /// Special vat percentage value
  /// </summary>
  /// <value>Special vat percentage value</value>
  [DataMember(Name="specialVatPercentage", EmitDefaultValue=false)]
  [JsonPropertyName("specialVatPercentage")]
  public decimal? SpecialVatPercentage { get; set; }

  /// <summary>
  /// Amount of the product
  /// </summary>
  /// <value>Amount of the product</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Comment of the product
  /// </summary>
  /// <value>Comment of the product</value>
  [DataMember(Name="comment", EmitDefaultValue=false)]
  [JsonPropertyName("comment")]
  public string Comment { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DefaultProductSettings {\n");
    sb.Append("  UseSpecialDiscount: ").Append(UseSpecialDiscount).Append('\n');
    sb.Append("  SpecialDiscountPercentage: ").Append(SpecialDiscountPercentage).Append('\n');
    sb.Append("  UseSpecialPrice: ").Append(UseSpecialPrice).Append('\n');
    sb.Append("  SpecialPrice: ").Append(SpecialPrice).Append('\n');
    sb.Append("  UseSpecialVat: ").Append(UseSpecialVat).Append('\n');
    sb.Append("  SpecialVatPercentage: ").Append(SpecialVatPercentage).Append('\n');
    sb.Append("  Amount: ").Append(Amount).Append('\n');
    sb.Append("  Comment: ").Append(Comment).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}