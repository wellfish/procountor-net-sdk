using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Search results.
/// </summary>
[DataContract]
public class PaymentErrorMessage {
  /// <summary>
  /// Unique identifier of the error message.
  /// </summary>
  /// <value>Unique identifier of the error message.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Date when the error message was created.
  /// </summary>
  /// <value>Date when the error message was created.</value>
  [DataMember(Name="createdDate", EmitDefaultValue=false)]
  [JsonPropertyName("createdDate")]
  public DateTime? CreatedDate { get; set; }

  /// <summary>
  /// Type of error.
  /// </summary>
  /// <value>Type of error.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Status of error handling.
  /// </summary>
  /// <value>Status of error handling.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Error code of error.
  /// </summary>
  /// <value>Error code of error.</value>
  [DataMember(Name="errorCode", EmitDefaultValue=false)]
  [JsonPropertyName("errorCode")]
  public string ErrorCode { get; set; }

  /// <summary>
  /// Identifier of payment object related to the error message. The related object can be an invoice or a direct bank transfer.
  /// </summary>
  /// <value>Identifier of payment object related to the error message. The related object can be an invoice or a direct bank transfer.</value>
  [DataMember(Name="referenceId", EmitDefaultValue=false)]
  [JsonPropertyName("referenceId")]
  public int? ReferenceId { get; set; }

  /// <summary>
  /// Type of payment object related to the error message. NOTE: BANK_TRANSFER type was replaced by DIRECT_BANK_TRANSFER.
  /// </summary>
  /// <value>Type of payment object related to the error message. NOTE: BANK_TRANSFER type was replaced by DIRECT_BANK_TRANSFER.</value>
  [DataMember(Name="referenceType", EmitDefaultValue=false)]
  [JsonPropertyName("referenceType")]
  public string ReferenceType { get; set; }

  /// <summary>
  /// Detailed type of receipt related to the error message, sent only for invoices.
  /// </summary>
  /// <value>Detailed type of receipt related to the error message, sent only for invoices.</value>
  [DataMember(Name="invoiceType", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceType")]
  public string InvoiceType { get; set; }

  /// <summary>
  /// Gets or Sets ErrorHandlingInfo
  /// </summary>
  [DataMember(Name="errorHandlingInfo", EmitDefaultValue=false)]
  [JsonPropertyName("errorHandlingInfo")]
  public ErrorHandlingInfo ErrorHandlingInfo { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentErrorMessage {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  CreatedDate: ").Append(CreatedDate).Append('\n');
    sb.Append("  Type: ").Append(Type).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  ErrorCode: ").Append(ErrorCode).Append('\n');
    sb.Append("  ReferenceId: ").Append(ReferenceId).Append('\n');
    sb.Append("  ReferenceType: ").Append(ReferenceType).Append('\n');
    sb.Append("  InvoiceType: ").Append(InvoiceType).Append('\n');
    sb.Append("  ErrorHandlingInfo: ").Append(ErrorHandlingInfo).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}