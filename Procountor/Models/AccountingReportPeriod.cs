using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of accounting report periods.
/// </summary>
[DataContract]
public class AccountingReportPeriod {
  /// <summary>
  /// Start date of the accounting report period.
  /// </summary>
  /// <value>Start date of the accounting report period.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the accounting report period.
  /// </summary>
  /// <value>End date of the accounting report period.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Accounting report period rows.
  /// </summary>
  /// <value>Accounting report period rows.</value>
  [DataMember(Name="rows", EmitDefaultValue=false)]
  [JsonPropertyName("rows")]
  public List<AccountingReportRow> Rows { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class AccountingReportPeriod {\n");
    sb.Append("  StartDate: ").Append(StartDate).Append('\n');
    sb.Append("  EndDate: ").Append(EndDate).Append('\n');
    sb.Append("  Rows: ").Append(Rows).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}