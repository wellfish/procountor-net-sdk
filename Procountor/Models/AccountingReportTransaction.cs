using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Ledger receipt transactions.
/// </summary>
[DataContract]
public class AccountingReportTransaction {
  /// <summary>
  /// Ledger transaction ID.
  /// </summary>
  /// <value>Ledger transaction ID.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Ledger receipt name.
  /// </summary>
  /// <value>Ledger receipt name.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Ledger receipt number.
  /// </summary>
  /// <value>Ledger receipt number.</value>
  [DataMember(Name="receiptNumber", EmitDefaultValue=false)]
  [JsonPropertyName("receiptNumber")]
  public int? ReceiptNumber { get; set; }

  /// <summary>
  /// Ledger receipt date.
  /// </summary>
  /// <value>Ledger receipt date.</value>
  [DataMember(Name="receiptDate", EmitDefaultValue=false)]
  [JsonPropertyName("receiptDate")]
  public DateTime? ReceiptDate { get; set; }

  /// <summary>
  /// Ledger invoice date.
  /// </summary>
  /// <value>Ledger invoice date.</value>
  [DataMember(Name="invoiceDate", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceDate")]
  public DateTime? InvoiceDate { get; set; }

  /// <summary>
  /// Ledger receipt type.
  /// </summary>
  /// <value>Ledger receipt type.</value>
  [DataMember(Name="type", EmitDefaultValue=false)]
  [JsonPropertyName("type")]
  public string Type { get; set; }

  /// <summary>
  /// Ledger transaction accounting value.
  /// </summary>
  /// <value>Ledger transaction accounting value.</value>
  [DataMember(Name="accountingValue", EmitDefaultValue=false)]
  [JsonPropertyName("accountingValue")]
  public decimal? AccountingValue { get; set; }

  /// <summary>
  /// Receipt status.
  /// </summary>
  /// <value>Receipt status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }

  /// <summary>
  /// Vat percent.
  /// </summary>
  /// <value>Vat percent.</value>
  [DataMember(Name="vatPercent", EmitDefaultValue=false)]
  [JsonPropertyName("vatPercent")]
  public decimal? VatPercent { get; set; }

  /// <summary>
  /// Vat amount.
  /// </summary>
  /// <value>Vat amount.</value>
  [DataMember(Name="vatAmount", EmitDefaultValue=false)]
  [JsonPropertyName("vatAmount")]
  public decimal? VatAmount { get; set; }

  /// <summary>
  /// Vat deduction percent.
  /// </summary>
  /// <value>Vat deduction percent.</value>
  [DataMember(Name="vatDeductionPercent", EmitDefaultValue=false)]
  [JsonPropertyName("vatDeductionPercent")]
  public decimal? VatDeductionPercent { get; set; }

  /// <summary>
  /// Vat deduction.
  /// </summary>
  /// <value>Vat deduction.</value>
  [DataMember(Name="vatDeduction", EmitDefaultValue=false)]
  [JsonPropertyName("vatDeduction")]
  public decimal? VatDeduction { get; set; }

  /// <summary>
  /// Vat status.
  /// </summary>
  /// <value>Vat status.</value>
  [DataMember(Name="vatStatus", EmitDefaultValue=false)]
  [JsonPropertyName("vatStatus")]
  public int? VatStatus { get; set; }

  /// <summary>
  /// Ledger Transaction Description.
  /// </summary>
  /// <value>Ledger Transaction Description.</value>
  [DataMember(Name="transactionDescription", EmitDefaultValue=false)]
  [JsonPropertyName("transactionDescription")]
  public string TransactionDescription { get; set; }

  /// <summary>
  /// Start date of the transaction.
  /// </summary>
  /// <value>Start date of the transaction.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the transaction.
  /// </summary>
  /// <value>End date of the transaction.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountingReportTransaction {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  ReceiptNumber: ").Append(ReceiptNumber).Append('\n');
      sb.Append("  ReceiptDate: ").Append(ReceiptDate).Append('\n');
      sb.Append("  InvoiceDate: ").Append(InvoiceDate).Append('\n');
      sb.Append("  Type: ").Append(Type).Append('\n');
      sb.Append("  AccountingValue: ").Append(AccountingValue).Append('\n');
      sb.Append("  Status: ").Append(Status).Append('\n');
      sb.Append("  VatPercent: ").Append(VatPercent).Append('\n');
      sb.Append("  VatAmount: ").Append(VatAmount).Append('\n');
      sb.Append("  VatDeductionPercent: ").Append(VatDeductionPercent).Append('\n');
      sb.Append("  VatDeduction: ").Append(VatDeduction).Append('\n');
      sb.Append("  VatStatus: ").Append(VatStatus).Append('\n');
      sb.Append("  TransactionDescription: ").Append(TransactionDescription).Append('\n');
      sb.Append("  StartDate: ").Append(StartDate).Append('\n');
      sb.Append("  EndDate: ").Append(EndDate).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}