using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Company settings. These are settings for 3rd party client usage to identify what features are usable for current company (in order to avoid requests that are bound to fail)
/// </summary>
[DataContract]
public class CompanySettings {
  /// <summary>
  /// Is invoice discussion enabled.
  /// </summary>
  /// <value>Is invoice discussion enabled.</value>
  [DataMember(Name="invoiceDiscussionEnabled", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceDiscussionEnabled")]
  public bool? InvoiceDiscussionEnabled { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CompanySettings {\n");
    sb.Append("  InvoiceDiscussionEnabled: ").Append(InvoiceDiscussionEnabled).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}