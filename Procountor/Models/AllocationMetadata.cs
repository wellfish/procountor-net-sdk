using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class AllocationMetadata {
  /// <summary>
  /// Metadata allocations for the target resource.
  /// </summary>
  /// <value>Metadata allocations for the target resource.</value>
  [DataMember(Name="allocationMetadataRows", EmitDefaultValue=false)]
  [JsonPropertyName("allocationMetadataRows")]
  public List<AllocationMetadataRow> AllocationMetadataRows { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AllocationMetadata {\n");
      sb.Append("  AllocationMetadataRows: ").Append(AllocationMetadataRows).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}