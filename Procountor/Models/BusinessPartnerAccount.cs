using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Default account of the given type.
/// </summary>
[DataContract]
public class BusinessPartnerAccount {
  /// <summary>
  /// Code of the account.
  /// </summary>
  /// <value>Code of the account.</value>
  [DataMember(Name="code", EmitDefaultValue=false)]
  [JsonPropertyName("code")]
  public string Code { get; set; }

  /// <summary>
  /// Name of the account.
  /// </summary>
  /// <value>Name of the account.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerAccount {\n");
      sb.Append("  Code: ").Append(Code).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}