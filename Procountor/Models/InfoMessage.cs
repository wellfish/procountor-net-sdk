using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Response information message
/// </summary>
[DataContract]
public class InfoMessage {
  /// <summary>
  /// Message with detail information about a result of action.
  /// </summary>
  /// <value>Message with detail information about a result of action.</value>
  [DataMember(Name="message", EmitDefaultValue=false)]
  [JsonPropertyName("message")]
  public string Message { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InfoMessage {\n");
    sb.Append("  Message: ").Append(Message).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}