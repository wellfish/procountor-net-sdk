using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contains rights for sales functionalities.
/// </summary>
[DataContract]
public class SalesRights {
  /// <summary>
  /// Access level to new sales invoice/Group invoice functionality.
  /// </summary>
  /// <value>Access level to new sales invoice/Group invoice functionality.</value>
  [DataMember(Name="newSalesInvoice", EmitDefaultValue=false)]
  [JsonPropertyName("newSalesInvoice")]
  public string NewSalesInvoice { get; set; }

  /// <summary>
  /// Access level to sales invoice search functionality.
  /// </summary>
  /// <value>Access level to sales invoice search functionality.</value>
  [DataMember(Name="salesInvoiceSearch", EmitDefaultValue=false)]
  [JsonPropertyName("salesInvoiceSearch")]
  public string SalesInvoiceSearch { get; set; }

  /// <summary>
  /// Access level to customer register functionality.
  /// </summary>
  /// <value>Access level to customer register functionality.</value>
  [DataMember(Name="customerRegister", EmitDefaultValue=false)]
  [JsonPropertyName("customerRegister")]
  public string CustomerRegister { get; set; }

  /// <summary>
  /// Access level to product register functionality.
  /// </summary>
  /// <value>Access level to product register functionality.</value>
  [DataMember(Name="productRegister", EmitDefaultValue=false)]
  [JsonPropertyName("productRegister")]
  public string ProductRegister { get; set; }

  /// <summary>
  /// Access level to group letter functionality.
  /// </summary>
  /// <value>Access level to group letter functionality.</value>
  [DataMember(Name="groupLetter", EmitDefaultValue=false)]
  [JsonPropertyName("groupLetter")]
  public string GroupLetter { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class SalesRights {\n");
      sb.Append("  NewSalesInvoice: ").Append(NewSalesInvoice).Append('\n');
      sb.Append("  SalesInvoiceSearch: ").Append(SalesInvoiceSearch).Append('\n');
      sb.Append("  CustomerRegister: ").Append(CustomerRegister).Append('\n');
      sb.Append("  ProductRegister: ").Append(ProductRegister).Append('\n');
      sb.Append("  GroupLetter: ").Append(GroupLetter).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}