using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Response for paying invoices action (POST /payments) performed successfully
/// </summary>
[DataContract]
public class InvoicePaymentSummaries {
  /// <summary>
  /// List of identifiers of added payments.
  /// </summary>
  /// <value>List of identifiers of added payments.</value>
  [DataMember(Name="transactions", EmitDefaultValue=false)]
  [JsonPropertyName("transactions")]
  public List<InvoicePaymentSummary> Transactions { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoicePaymentSummaries {\n");
    sb.Append("  Transactions: ").Append(Transactions).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}