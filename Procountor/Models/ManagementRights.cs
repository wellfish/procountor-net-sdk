using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contains rights for management functionalities.
/// </summary>
[DataContract]
public class ManagementRights {
  /// <summary>
  /// Access level to basic company info functionality.
  /// </summary>
  /// <value>Access level to basic company info functionality.</value>
  [DataMember(Name="basicCompanyInfo", EmitDefaultValue=false)]
  [JsonPropertyName("basicCompanyInfo")]
  public string BasicCompanyInfo { get; set; }

  /// <summary>
  /// Access level to basic accounting info functionality.
  /// </summary>
  /// <value>Access level to basic accounting info functionality.</value>
  [DataMember(Name="basicAccountingInfo", EmitDefaultValue=false)]
  [JsonPropertyName("basicAccountingInfo")]
  public string BasicAccountingInfo { get; set; }

  /// <summary>
  /// Access level to edit personal info functionality.
  /// </summary>
  /// <value>Access level to edit personal info functionality.</value>
  [DataMember(Name="editPersonalInfo", EmitDefaultValue=false)]
  [JsonPropertyName("editPersonalInfo")]
  public string EditPersonalInfo { get; set; }

  /// <summary>
  /// Access level to company user management functionality.
  /// </summary>
  /// <value>Access level to company user management functionality.</value>
  [DataMember(Name="companyUserManagement", EmitDefaultValue=false)]
  [JsonPropertyName("companyUserManagement")]
  public string CompanyUserManagement { get; set; }

  /// <summary>
  /// Access level to set up a company functionality.
  /// </summary>
  /// <value>Access level to set up a company functionality.</value>
  [DataMember(Name="setUpACompany", EmitDefaultValue=false)]
  [JsonPropertyName("setUpACompany")]
  public string SetUpACompany { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ManagementRights {\n");
    sb.Append("  BasicCompanyInfo: ").Append(BasicCompanyInfo).Append('\n');
    sb.Append("  BasicAccountingInfo: ").Append(BasicAccountingInfo).Append('\n');
    sb.Append("  EditPersonalInfo: ").Append(EditPersonalInfo).Append('\n');
    sb.Append("  CompanyUserManagement: ").Append(CompanyUserManagement).Append('\n');
    sb.Append("  SetUpACompany: ").Append(SetUpACompany).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}