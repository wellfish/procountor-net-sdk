using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// This object holds information about the counterparty of the invoice. With sales invoices and sales orders, it is the buyer. With purchase invoices and purchase orders it is the seller. With travel and expense invoices, it is the reporter of the expenses
/// </summary>
[DataContract]
public class CounterParty {
  /// <summary>
  /// Name of the contact person.
  /// </summary>
  /// <value>Name of the contact person.</value>
  [DataMember(Name="contactPersonName", EmitDefaultValue=false)]
  [JsonPropertyName("contactPersonName")]
  public string ContactPersonName { get; set; }

  /// <summary>
  /// SALES_INVOICE, SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER only. Business ID or national identification number.
  /// </summary>
  /// <value>SALES_INVOICE, SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER only. Business ID or national identification number.</value>
  [DataMember(Name="identifier", EmitDefaultValue=false)]
  [JsonPropertyName("identifier")]
  public string Identifier { get; set; }

  /// <summary>
  /// SALES_INVOICE and SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER only. Tax code of the customer.
  /// </summary>
  /// <value>SALES_INVOICE and SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER only. Tax code of the customer.</value>
  [DataMember(Name="taxCode", EmitDefaultValue=false)]
  [JsonPropertyName("taxCode")]
  public string TaxCode { get; set; }

  /// <summary>
  /// SALES_INVOICE, SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER only. Customer number.
  /// </summary>
  /// <value>SALES_INVOICE, SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER only. Customer number.</value>
  [DataMember(Name="customerNumber", EmitDefaultValue=false)]
  [JsonPropertyName("customerNumber")]
  public string CustomerNumber { get; set; }

  /// <summary>
  /// SALES_INVOICE and SALES_ORDER only. Email address of the buyer. Required if invoicing channel is EMAIL, otherwise not visible on the UI.
  /// </summary>
  /// <value>SALES_INVOICE and SALES_ORDER only. Email address of the buyer. Required if invoicing channel is EMAIL, otherwise not visible on the UI.</value>
  [DataMember(Name="email", EmitDefaultValue=false)]
  [JsonPropertyName("email")]
  public string Email { get; set; }

  /// <summary>
  /// Gets or Sets CounterPartyAddress
  /// </summary>
  [DataMember(Name="counterPartyAddress", EmitDefaultValue=false)]
  [JsonPropertyName("counterPartyAddress")]
  public Address CounterPartyAddress { get; set; }

  /// <summary>
  /// Gets or Sets BankAccount
  /// </summary>
  [DataMember(Name="bankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("bankAccount")]
  public BankAccount BankAccount { get; set; }

  /// <summary>
  /// Gets or Sets EinvoiceAddress
  /// </summary>
  [DataMember(Name="einvoiceAddress", EmitDefaultValue=false)]
  [JsonPropertyName("einvoiceAddress")]
  public EInvoiceAddress EinvoiceAddress { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CounterParty {\n");
    sb.Append("  ContactPersonName: ").Append(ContactPersonName).Append('\n');
    sb.Append("  Identifier: ").Append(Identifier).Append('\n');
    sb.Append("  TaxCode: ").Append(TaxCode).Append('\n');
    sb.Append("  CustomerNumber: ").Append(CustomerNumber).Append('\n');
    sb.Append("  Email: ").Append(Email).Append('\n');
    sb.Append("  CounterPartyAddress: ").Append(CounterPartyAddress).Append('\n');
    sb.Append("  BankAccount: ").Append(BankAccount).Append('\n');
    sb.Append("  EinvoiceAddress: ").Append(EinvoiceAddress).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}