using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CurrencyExchangeRate {
  /// <summary>
  /// Day specified in query.
  /// </summary>
  /// <value>Day specified in query.</value>
  [DataMember(Name="queryDay", EmitDefaultValue=false)]
  [JsonPropertyName("queryDay")]
  public DateTime? QueryDay { get; set; }

  /// <summary>
  /// Currency of exchange rate.
  /// </summary>
  /// <value>Currency of exchange rate.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Day of exchange rate.
  /// </summary>
  /// <value>Day of exchange rate.</value>
  [DataMember(Name="day", EmitDefaultValue=false)]
  [JsonPropertyName("day")]
  public DateTime? Day { get; set; }

  /// <summary>
  /// Rate of exchange rate.
  /// </summary>
  /// <value>Rate of exchange rate.</value>
  [DataMember(Name="rate", EmitDefaultValue=false)]
  [JsonPropertyName("rate")]
  public decimal? Rate { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CurrencyExchangeRate {\n");
    sb.Append("  QueryDay: ").Append(QueryDay).Append('\n');
    sb.Append("  Currency: ").Append(Currency).Append('\n');
    sb.Append("  Day: ").Append(Day).Append('\n');
    sb.Append("  Rate: ").Append(Rate).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}