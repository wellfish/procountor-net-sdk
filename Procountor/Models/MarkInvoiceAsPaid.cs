using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class MarkInvoiceAsPaid {
  /// <summary>
  /// Payment date. Cannot be in closed financial period
  /// </summary>
  /// <value>Payment date. Cannot be in closed financial period</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public string PaymentDate { get; set; }

  /// <summary>
  /// Amount in the given currency
  /// </summary>
  /// <value>Amount in the given currency</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Currency in ISO 4217 format. It should always be the same as in the invoice
  /// </summary>
  /// <value>Currency in ISO 4217 format. It should always be the same as in the invoice</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Payment description. Used only in sales invoices.
  /// </summary>
  /// <value>Payment description. Used only in sales invoices.</value>
  [DataMember(Name="description", EmitDefaultValue=false)]
  [JsonPropertyName("description")]
  public string Description { get; set; }

  /// <summary>
  /// Payment method type. Used only in sales invoices.
  /// </summary>
  /// <value>Payment method type. Used only in sales invoices.</value>
  [DataMember(Name="paymentMethodType", EmitDefaultValue=false)]
  [JsonPropertyName("paymentMethodType")]
  public string PaymentMethodType { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class MarkInvoiceAsPaid {\n");
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  Amount: ").Append(Amount).Append('\n');
    sb.Append("  Currency: ").Append(Currency).Append('\n');
    sb.Append("  Description: ").Append(Description).Append('\n');
    sb.Append("  PaymentMethodType: ").Append(PaymentMethodType).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}