using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Intermediary bank name and address.
/// </summary>
[DataContract]
public class Address {
  /// <summary>
  /// Name (\"first line\") in the address.
  /// </summary>
  /// <value>Name (\"first line\") in the address.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Specifier, such as c/o address.
  /// </summary>
  /// <value>Specifier, such as c/o address.</value>
  [DataMember(Name="specifier", EmitDefaultValue=false)]
  [JsonPropertyName("specifier")]
  public string Specifier { get; set; }

  /// <summary>
  /// Street. Required for SALES_INVOICE and SALES_ORDER if invoicing channel is MAIL. In that case, must be specified in counterPartyAddress if not specified in billingAddress.   Required for SALES_INVOICE if invoicing channel is ELECTRONIC_INVOICE. In that case, must be specified in billingAddress and counterPartyAddress.
  /// </summary>
  /// <value>Street. Required for SALES_INVOICE and SALES_ORDER if invoicing channel is MAIL. In that case, must be specified in counterPartyAddress if not specified in billingAddress.   Required for SALES_INVOICE if invoicing channel is ELECTRONIC_INVOICE. In that case, must be specified in billingAddress and counterPartyAddress.</value>
  [DataMember(Name="street", EmitDefaultValue=false)]
  [JsonPropertyName("street")]
  public string Street { get; set; }

  /// <summary>
  /// Zip code. Required for SALES_INVOICE and SALES_ORDER if invoicing channel is MAIL. In that case, must be specified in counterPartyAddress if not specified in billingAddress.   Required for SALES_INVOICE if invoicing channel is ELECTRONIC_INVOICE. In that case, must be specified in billingAddress and counterPartyAddress.
  /// </summary>
  /// <value>Zip code. Required for SALES_INVOICE and SALES_ORDER if invoicing channel is MAIL. In that case, must be specified in counterPartyAddress if not specified in billingAddress.   Required for SALES_INVOICE if invoicing channel is ELECTRONIC_INVOICE. In that case, must be specified in billingAddress and counterPartyAddress.</value>
  [DataMember(Name="zip", EmitDefaultValue=false)]
  [JsonPropertyName("zip")]
  public string Zip { get; set; }

  /// <summary>
  /// City. Required for SALES_INVOICE and SALES_ORDER if invoicing channel is MAIL. In that case, must be specified in counterPartyAddress if not specified in billingAddress.  Required for SALES_INVOICE if invoicing channel is ELECTRONIC_INVOICE. In that case, must be specified in billingAddress and counterPartyAddress.
  /// </summary>
  /// <value>City. Required for SALES_INVOICE and SALES_ORDER if invoicing channel is MAIL. In that case, must be specified in counterPartyAddress if not specified in billingAddress.  Required for SALES_INVOICE if invoicing channel is ELECTRONIC_INVOICE. In that case, must be specified in billingAddress and counterPartyAddress.</value>
  [DataMember(Name="city", EmitDefaultValue=false)]
  [JsonPropertyName("city")]
  public string City { get; set; }

  /// <summary>
  /// Country.
  /// </summary>
  /// <value>Country.</value>
  [DataMember(Name="country", EmitDefaultValue=false)]
  [JsonPropertyName("country")]
  public string Country { get; set; }

  /// <summary>
  /// Subdivision of the city
  /// </summary>
  /// <value>Subdivision of the city</value>
  [DataMember(Name="subdivision", EmitDefaultValue=false)]
  [JsonPropertyName("subdivision")]
  public string Subdivision { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Address {\n");
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  Specifier: ").Append(Specifier).Append('\n');
      sb.Append("  Street: ").Append(Street).Append('\n');
      sb.Append("  Zip: ").Append(Zip).Append('\n');
      sb.Append("  City: ").Append(City).Append('\n');
      sb.Append("  Country: ").Append(Country).Append('\n');
      sb.Append("  Subdivision: ").Append(Subdivision).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}