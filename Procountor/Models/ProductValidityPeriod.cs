using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Validity periods of the default product
/// </summary>
[DataContract]
public class ProductValidityPeriod {
  /// <summary>
  /// Start date of the validity period
  /// </summary>
  /// <value>Start date of the validity period</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the validity period
  /// </summary>
  /// <value>End date of the validity period</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Notes of the validity period
  /// </summary>
  /// <value>Notes of the validity period</value>
  [DataMember(Name="notes", EmitDefaultValue=false)]
  [JsonPropertyName("notes")]
  public string Notes { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ProductValidityPeriod {\n");
      sb.Append("  StartDate: ").Append(StartDate).Append('\n');
      sb.Append("  EndDate: ").Append(EndDate).Append('\n');
      sb.Append("  Notes: ").Append(Notes).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}