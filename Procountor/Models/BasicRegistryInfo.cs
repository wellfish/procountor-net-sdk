using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Registry info data of the partner.
/// </summary>
[DataContract]
public class BasicRegistryInfo {
  /// <summary>
  /// Status of the partner.
  /// </summary>
  /// <value>Status of the partner.</value>
  [DataMember(Name="active", EmitDefaultValue=false)]
  [JsonPropertyName("active")]
  public bool? Active { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BasicRegistryInfo {\n");
      sb.Append("  Active: ").Append(Active).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}