using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// These are possible response models
/// </summary>
[DataContract]
public class PaymentsConfirmationResponseModels {
  /// <summary>
  /// Gets or Sets PaymentSummaries
  /// </summary>
  [DataMember(Name="PaymentSummaries", EmitDefaultValue=false)]
  [JsonPropertyName("PaymentSummaries")]
  public PaymentSummaries PaymentSummaries { get; set; }

  /// <summary>
  /// Gets or Sets InfoMessage
  /// </summary>
  [DataMember(Name="InfoMessage", EmitDefaultValue=false)]
  [JsonPropertyName("InfoMessage")]
  public InfoMessage InfoMessage { get; set; }

  /// <summary>
  /// Gets or Sets InvoicePaymentSummaries
  /// </summary>
  [DataMember(Name="InvoicePaymentSummaries", EmitDefaultValue=false)]
  [JsonPropertyName("InvoicePaymentSummaries")]
  public InvoicePaymentSummaries InvoicePaymentSummaries { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentsConfirmationResponseModels {\n");
    sb.Append("  PaymentSummaries: ").Append(PaymentSummaries).Append('\n');
    sb.Append("  InfoMessage: ").Append(InfoMessage).Append('\n');
    sb.Append("  InvoicePaymentSummaries: ").Append(InvoicePaymentSummaries).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}