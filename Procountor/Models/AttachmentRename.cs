using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class AttachmentRename {
  /// <summary>
  /// Attachment name. Include a correct file extension to the value.
  /// </summary>
  /// <value>Attachment name. Include a correct file extension to the value.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AttachmentRename {\n");
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}