using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Only SALES_INVOICE, SALES_ORDER, PURCHASE_INVOICE and PURCHASE_ORDER. Cash discount set on the invoice.
/// </summary>
[DataContract]
public class CashDiscount {
  /// <summary>
  /// Up to three cash discount options can be provided. Any additional values enteredwill be ignored. Provided cash discount options will be saved in ascending order by numberOfDays.
  /// </summary>
  /// <value>Up to three cash discount options can be provided. Any additional values enteredwill be ignored. Provided cash discount options will be saved in ascending order by numberOfDays.</value>
  [DataMember(Name="optionList", EmitDefaultValue=false)]
  [JsonPropertyName("optionList")]
  public List<CashDiscountOption> OptionList { get; set; }

  /// <summary>
  /// The payment term type indicates the start point for payment due date calculation.On Danish environment, may select between two options: FROM_INV_DATE or FROM_END_OF_MONTH.For other environments, the only selectable term type is FROM_INV_DATE,which is also the default value across all environments.
  /// </summary>
  /// <value>The payment term type indicates the start point for payment due date calculation.On Danish environment, may select between two options: FROM_INV_DATE or FROM_END_OF_MONTH.For other environments, the only selectable term type is FROM_INV_DATE,which is also the default value across all environments.</value>
  [DataMember(Name="cashDiscountsTermType", EmitDefaultValue=false)]
  [JsonPropertyName("cashDiscountsTermType")]
  public string CashDiscountsTermType { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class CashDiscount {\n");
      sb.Append("  OptionList: ").Append(OptionList).Append('\n');
      sb.Append("  CashDiscountsTermType: ").Append(CashDiscountsTermType).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}