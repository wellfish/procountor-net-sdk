using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// SALES_INVOICE only. EInvoice address of the buyer. Required if invoicing channel is ELECTRONIC_INVOICE, otherwise not visible on the UI.
/// </summary>
[DataContract]
public class EInvoiceAddress {
  /// <summary>
  /// SALES_INVOICE Only. Operator code. Required if the invoiceChannel is ELECTRONIC_INVOICE and country is FINLAND.
  /// </summary>
  /// <value>SALES_INVOICE Only. Operator code. Required if the invoiceChannel is ELECTRONIC_INVOICE and country is FINLAND.</value>
  [DataMember(Name="operator", EmitDefaultValue=false)]
  [JsonPropertyName("operator")]
  public string _Operator { get; set; }

  /// <summary>
  /// SALES_INVOICE Only. EInvoice Address. Required if the invoiceChannel is ELECTRONIC_INVOICE, format must be valid for the specified country.
  /// </summary>
  /// <value>SALES_INVOICE Only. EInvoice Address. Required if the invoiceChannel is ELECTRONIC_INVOICE, format must be valid for the specified country.</value>
  [DataMember(Name="address", EmitDefaultValue=false)]
  [JsonPropertyName("address")]
  public string Address { get; set; }

  /// <summary>
  /// SALES_INVOICE Only. EDI identifier. Used if the invoiceChannel is ELECTRONIC_INVOICE and country is FINLAND.
  /// </summary>
  /// <value>SALES_INVOICE Only. EDI identifier. Used if the invoiceChannel is ELECTRONIC_INVOICE and country is FINLAND.</value>
  [DataMember(Name="ediId", EmitDefaultValue=false)]
  [JsonPropertyName("ediId")]
  public string EdiId { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class EInvoiceAddress {\n");
    sb.Append("  _Operator: ").Append(_Operator).Append('\n');
    sb.Append("  Address: ").Append(Address).Append('\n');
    sb.Append("  EdiId: ").Append(EdiId).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}