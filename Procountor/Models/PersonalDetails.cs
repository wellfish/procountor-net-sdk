using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class PersonalDetails {
  /// <summary>
  /// Partner register ID.
  /// </summary>
  /// <value>Partner register ID.</value>
  [DataMember(Name="partnerId", EmitDefaultValue=false)]
  [JsonPropertyName("partnerId")]
  public int? PartnerId { get; set; }

  /// <summary>
  /// First name of the person.
  /// </summary>
  /// <value>First name of the person.</value>
  [DataMember(Name="firstName", EmitDefaultValue=false)]
  [JsonPropertyName("firstName")]
  public string FirstName { get; set; }

  /// <summary>
  /// Last name of the person.
  /// </summary>
  /// <value>Last name of the person.</value>
  [DataMember(Name="lastName", EmitDefaultValue=false)]
  [JsonPropertyName("lastName")]
  public string LastName { get; set; }

  /// <summary>
  /// Language of the person.
  /// </summary>
  /// <value>Language of the person.</value>
  [DataMember(Name="language", EmitDefaultValue=false)]
  [JsonPropertyName("language")]
  public string Language { get; set; }

  /// <summary>
  /// Gets or Sets Address
  /// </summary>
  [DataMember(Name="address", EmitDefaultValue=false)]
  [JsonPropertyName("address")]
  public Address Address { get; set; }

  /// <summary>
  /// Gets or Sets PaymentInfo
  /// </summary>
  [DataMember(Name="paymentInfo", EmitDefaultValue=false)]
  [JsonPropertyName("paymentInfo")]
  public BusinessPartnerPaymentDetails PaymentInfo { get; set; }

  /// <summary>
  /// Gets or Sets InvoicingInfo
  /// </summary>
  [DataMember(Name="invoicingInfo", EmitDefaultValue=false)]
  [JsonPropertyName("invoicingInfo")]
  public BasicInvoicingInfo InvoicingInfo { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PersonalDetails {\n");
    sb.Append("  PartnerId: ").Append(PartnerId).Append('\n');
    sb.Append("  FirstName: ").Append(FirstName).Append('\n');
    sb.Append("  LastName: ").Append(LastName).Append('\n');
    sb.Append("  Language: ").Append(Language).Append('\n');
    sb.Append("  Address: ").Append(Address).Append('\n');
    sb.Append("  PaymentInfo: ").Append(PaymentInfo).Append('\n');
    sb.Append("  InvoicingInfo: ").Append(InvoicingInfo).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}