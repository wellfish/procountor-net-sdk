using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class Webhook {
  /// <summary>
  /// Generated webhook UUID.
  /// </summary>
  /// <value>Generated webhook UUID.</value>
  [DataMember(Name="uuid", EmitDefaultValue=false)]
  [JsonPropertyName("uuid")]
  public Guid? Uuid { get; set; }

  /// <summary>
  /// Provided URL, for receiving notifications.
  /// </summary>
  /// <value>Provided URL, for receiving notifications.</value>
  [DataMember(Name="url", EmitDefaultValue=false)]
  [JsonPropertyName("url")]
  public string Url { get; set; }

  /// <summary>
  /// Webhook's authentication type.
  /// </summary>
  /// <value>Webhook's authentication type.</value>
  [DataMember(Name="authenticationType", EmitDefaultValue=false)]
  [JsonPropertyName("authenticationType")]
  public string AuthenticationType { get; set; }

  /// <summary>
  /// Webhook's authentication meta.
  /// </summary>
  /// <value>Webhook's authentication meta.</value>
  [DataMember(Name="authenticationMeta", EmitDefaultValue=false)]
  [JsonPropertyName("authenticationMeta")]
  public Dictionary<string, Object> AuthenticationMeta { get; set; }

  /// <summary>
  /// Collection of subscribed event types.
  /// </summary>
  /// <value>Collection of subscribed event types.</value>
  [DataMember(Name="subscriptions", EmitDefaultValue=false)]
  [JsonPropertyName("subscriptions")]
  public List<string> Subscriptions { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class Webhook {\n");
    sb.Append("  Uuid: ").Append(Uuid).Append('\n');
    sb.Append("  Url: ").Append(Url).Append('\n');
    sb.Append("  AuthenticationType: ").Append(AuthenticationType).Append('\n');
    sb.Append("  AuthenticationMeta: ").Append(AuthenticationMeta).Append('\n');
    sb.Append("  Subscriptions: ").Append(Subscriptions).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}