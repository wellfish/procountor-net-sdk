using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class TaggableUserDTO {
  /// <summary>
  /// Generated display name that is used in comments to tag that given user. For the most of the time this will be firstname + lastname. Running number will be used to indicate multiple people with identical names.
  /// </summary>
  /// <value>Generated display name that is used in comments to tag that given user. For the most of the time this will be firstname + lastname. Running number will be used to indicate multiple people with identical names.</value>
  [DataMember(Name="displayName", EmitDefaultValue=false)]
  [JsonPropertyName("displayName")]
  public string DisplayName { get; set; }

  /// <summary>
  /// Full name.
  /// </summary>
  /// <value>Full name.</value>
  [DataMember(Name="fullName", EmitDefaultValue=false)]
  [JsonPropertyName("fullName")]
  public string FullName { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class TaggableUserDTO {\n");
    sb.Append("  DisplayName: ").Append(DisplayName).Append('\n');
    sb.Append("  FullName: ").Append(FullName).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}