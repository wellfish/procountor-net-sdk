using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Payment bank account. Not required if payment method is cash. Deprecated in counterParty section for invoices endpoints - used only with DIRECT_DEBIT payment method, which is deprecated as well.
/// </summary>
[DataContract]
public class BankAccount {
  /// <summary>
  /// Bank account IBAN. If using a financing agreement, the account number must match the account of the specified financing agreement. The account number must be valid for the specified country, include country code and exclude any spaces.
  /// </summary>
  /// <value>Bank account IBAN. If using a financing agreement, the account number must match the account of the specified financing agreement. The account number must be valid for the specified country, include country code and exclude any spaces.</value>
  [DataMember(Name="accountNumber", EmitDefaultValue=false)]
  [JsonPropertyName("accountNumber")]
  public string AccountNumber { get; set; }

  /// <summary>
  /// Bank account BIC/SWIFT. Not supported for SALES_INVOICE and SALES_ORDER.
  /// </summary>
  /// <value>Bank account BIC/SWIFT. Not supported for SALES_INVOICE and SALES_ORDER.</value>
  [DataMember(Name="bic", EmitDefaultValue=false)]
  [JsonPropertyName("bic")]
  public string Bic { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BankAccount {\n");
      sb.Append("  AccountNumber: ").Append(AccountNumber).Append('\n');
      sb.Append("  Bic: ").Append(Bic).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}