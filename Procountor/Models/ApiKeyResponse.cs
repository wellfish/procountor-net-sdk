using System.Text.Json.Serialization;

namespace Procountor.Models;

public sealed class ApiKeyResponse
{
    [JsonPropertyName("api_key")]
    public required string ApiKey { get; set; }
}