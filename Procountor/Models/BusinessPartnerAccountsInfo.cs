using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Default accounts for the given invoice type.
/// </summary>
[DataContract]
public class BusinessPartnerAccountsInfo {
  /// <summary>
  /// Gets or Sets DebitAccount
  /// </summary>
  [DataMember(Name="debitAccount", EmitDefaultValue=false)]
  [JsonPropertyName("debitAccount")]
  public BusinessPartnerAccount DebitAccount { get; set; }

  /// <summary>
  /// Gets or Sets CreditAccount
  /// </summary>
  [DataMember(Name="creditAccount", EmitDefaultValue=false)]
  [JsonPropertyName("creditAccount")]
  public BusinessPartnerAccount CreditAccount { get; set; }

  /// <summary>
  /// Gets or Sets ReconciliationAccount
  /// </summary>
  [DataMember(Name="reconciliationAccount", EmitDefaultValue=false)]
  [JsonPropertyName("reconciliationAccount")]
  public BusinessPartnerAccount ReconciliationAccount { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerAccountsInfo {\n");
      sb.Append("  DebitAccount: ").Append(DebitAccount).Append('\n');
      sb.Append("  CreditAccount: ").Append(CreditAccount).Append('\n');
      sb.Append("  ReconciliationAccount: ").Append(ReconciliationAccount).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}