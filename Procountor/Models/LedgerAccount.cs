using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Ledger account used for the accounting. Must be valid for the current Procountor environment. Use GET /coa to obtain the chart of accounts.
/// </summary>
[DataContract]
public class LedgerAccount {
  /// <summary>
  /// Ledger account code.
  /// </summary>
  /// <value>Ledger account code.</value>
  [DataMember(Name="ledgerAccountCode", EmitDefaultValue=false)]
  [JsonPropertyName("ledgerAccountCode")]
  public string LedgerAccountCode { get; set; }

  /// <summary>
  /// Ledger account name.
  /// </summary>
  /// <value>Ledger account name.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Indicates whether the ledger account is active or not.
  /// </summary>
  /// <value>Indicates whether the ledger account is active or not.</value>
  [DataMember(Name="active", EmitDefaultValue=false)]
  [JsonPropertyName("active")]
  public bool? Active { get; set; }

  /// <summary>
  /// Ledger account name translations. If there is no translations available for given account, api response body will not contain translations list at all.
  /// </summary>
  /// <value>Ledger account name translations. If there is no translations available for given account, api response body will not contain translations list at all.</value>
  [DataMember(Name="translations", EmitDefaultValue=false)]
  [JsonPropertyName("translations")]
  public List<LedgerAccountTranslation> Translations { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class LedgerAccount {\n");
    sb.Append("  LedgerAccountCode: ").Append(LedgerAccountCode).Append('\n');
    sb.Append("  Name: ").Append(Name).Append('\n');
    sb.Append("  Active: ").Append(Active).Append('\n');
    sb.Append("  Translations: ").Append(Translations).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}