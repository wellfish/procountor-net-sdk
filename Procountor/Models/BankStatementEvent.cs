using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of bank statement events. Events can be nested.
/// </summary>
[DataContract]
public class BankStatementEvent {
  /// <summary>
  /// Unique identifier of the bank statement event.
  /// </summary>
  /// <value>Unique identifier of the bank statement event.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Date when the payment was paid by the payer in his/her own bank.
  /// </summary>
  /// <value>Date when the payment was paid by the payer in his/her own bank.</value>
  [DataMember(Name="payDate", EmitDefaultValue=false)]
  [JsonPropertyName("payDate")]
  public DateTime? PayDate { get; set; }

  /// <summary>
  /// Date when the event was registered in the counterpart bank.
  /// </summary>
  /// <value>Date when the event was registered in the counterpart bank.</value>
  [DataMember(Name="valueDate", EmitDefaultValue=false)]
  [JsonPropertyName("valueDate")]
  public DateTime? ValueDate { get; set; }

  /// <summary>
  /// Sum of the bank statement event in the currency of the bank statement this event belongs to.
  /// </summary>
  /// <value>Sum of the bank statement event in the currency of the bank statement this event belongs to.</value>
  [DataMember(Name="sum", EmitDefaultValue=false)]
  [JsonPropertyName("sum")]
  public decimal? Sum { get; set; }

  /// <summary>
  /// Account number of the counterparty.
  /// </summary>
  /// <value>Account number of the counterparty.</value>
  [DataMember(Name="accountNumber", EmitDefaultValue=false)]
  [JsonPropertyName("accountNumber")]
  public string AccountNumber { get; set; }

  /// <summary>
  /// Name of the counterparty.
  /// </summary>
  /// <value>Name of the counterparty.</value>
  [DataMember(Name="name", EmitDefaultValue=false)]
  [JsonPropertyName("name")]
  public string Name { get; set; }

  /// <summary>
  /// Explanation code of the event. Explanation code describes the type of the event.
  /// </summary>
  /// <value>Explanation code of the event. Explanation code describes the type of the event.</value>
  [DataMember(Name="explanationCode", EmitDefaultValue=false)]
  [JsonPropertyName("explanationCode")]
  public int? ExplanationCode { get; set; }

  /// <summary>
  /// Archive code of the event. Archive codes are unique in one bank but two events from different banks can share the same archive code.
  /// </summary>
  /// <value>Archive code of the event. Archive codes are unique in one bank but two events from different banks can share the same archive code.</value>
  [DataMember(Name="archiveCode", EmitDefaultValue=false)]
  [JsonPropertyName("archiveCode")]
  public string ArchiveCode { get; set; }

  /// <summary>
  /// Message of the event.
  /// </summary>
  /// <value>Message of the event.</value>
  [DataMember(Name="message", EmitDefaultValue=false)]
  [JsonPropertyName("message")]
  public string Message { get; set; }

  /// <summary>
  /// Reference of the event.
  /// </summary>
  /// <value>Reference of the event.</value>
  [DataMember(Name="reference", EmitDefaultValue=false)]
  [JsonPropertyName("reference")]
  public string Reference { get; set; }

  /// <summary>
  /// Is the event allocated to an invoice. If it is, the event must also have an invoice ID.
  /// </summary>
  /// <value>Is the event allocated to an invoice. If it is, the event must also have an invoice ID.</value>
  [DataMember(Name="allocated", EmitDefaultValue=false)]
  [JsonPropertyName("allocated")]
  public bool? Allocated { get; set; }

  /// <summary>
  /// Unique identifier of the invoice linked to the event.
  /// </summary>
  /// <value>Unique identifier of the invoice linked to the event.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }

  /// <summary>
  /// ID of the product allocated to the bank statement event.
  /// </summary>
  /// <value>ID of the product allocated to the bank statement event.</value>
  [DataMember(Name="productId", EmitDefaultValue=false)]
  [JsonPropertyName("productId")]
  public long? ProductId { get; set; }

  /// <summary>
  /// Unique identifier of the payment.
  /// </summary>
  /// <value>Unique identifier of the payment.</value>
  [DataMember(Name="endToEndId", EmitDefaultValue=false)]
  [JsonPropertyName("endToEndId")]
  public int? EndToEndId { get; set; }

  /// <summary>
  /// List of attachments added to the event.
  /// </summary>
  /// <value>List of attachments added to the event.</value>
  [DataMember(Name="attachments", EmitDefaultValue=false)]
  [JsonPropertyName("attachments")]
  public List<Attachment> Attachments { get; set; }

  /// <summary>
  /// Metadata allocations for the bank statement event.
  /// </summary>
  /// <value>Metadata allocations for the bank statement event.</value>
  [DataMember(Name="allocationMetadataRows", EmitDefaultValue=false)]
  [JsonPropertyName("allocationMetadataRows")]
  public List<AllocationMetadataWithFullLedgerAccountDataRow> AllocationMetadataRows { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BankStatementEvent {\n");
      sb.Append("  Id: ").Append(Id).Append('\n');
      sb.Append("  PayDate: ").Append(PayDate).Append('\n');
      sb.Append("  ValueDate: ").Append(ValueDate).Append('\n');
      sb.Append("  Sum: ").Append(Sum).Append('\n');
      sb.Append("  AccountNumber: ").Append(AccountNumber).Append('\n');
      sb.Append("  Name: ").Append(Name).Append('\n');
      sb.Append("  ExplanationCode: ").Append(ExplanationCode).Append('\n');
      sb.Append("  ArchiveCode: ").Append(ArchiveCode).Append('\n');
      sb.Append("  Message: ").Append(Message).Append('\n');
      sb.Append("  Reference: ").Append(Reference).Append('\n');
      sb.Append("  Allocated: ").Append(Allocated).Append('\n');
      sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
      sb.Append("  ProductId: ").Append(ProductId).Append('\n');
      sb.Append("  EndToEndId: ").Append(EndToEndId).Append('\n');
      sb.Append("  Attachments: ").Append(Attachments).Append('\n');
      sb.Append("  AllocationMetadataRows: ").Append(AllocationMetadataRows).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}