using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class InvoiceTransactions {
  /// <summary>
  /// List of invoice transactions.
  /// </summary>
  /// <value>List of invoice transactions.</value>
  [DataMember(Name="transactions", EmitDefaultValue=false)]
  [JsonPropertyName("transactions")]
  public List<InvoiceTransaction> Transactions { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceTransactions {\n");
    sb.Append("  Transactions: ").Append(Transactions).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}