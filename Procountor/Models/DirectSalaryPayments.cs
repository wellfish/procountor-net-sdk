using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class DirectSalaryPayments {
  /// <summary>
  /// Name of the direct salary payment list. Can be used for list identification.
  /// </summary>
  /// <value>Name of the direct salary payment list. Can be used for list identification.</value>
  [DataMember(Name="listName", EmitDefaultValue=false)]
  [JsonPropertyName("listName")]
  public string ListName { get; set; }

  /// <summary>
  /// Name of the payment method. Used for all payments.
  /// </summary>
  /// <value>Name of the payment method. Used for all payments.</value>
  [DataMember(Name="paymentMethod", EmitDefaultValue=false)]
  [JsonPropertyName("paymentMethod")]
  public string PaymentMethod { get; set; }

  /// <summary>
  /// Date specifying when the payment transaction has to be performed. Payment date should be grater then current date. Used for all payments.
  /// </summary>
  /// <value>Date specifying when the payment transaction has to be performed. Payment date should be grater then current date. Used for all payments.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public string PaymentDate { get; set; }

  /// <summary>
  /// Bank account number of the person making the payment. The payer bank account has to be predefined in the environment to be able to use the payment. Used for all payments.
  /// </summary>
  /// <value>Bank account number of the person making the payment. The payer bank account has to be predefined in the environment to be able to use the payment. Used for all payments.</value>
  [DataMember(Name="payerBankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("payerBankAccount")]
  public string PayerBankAccount { get; set; }

  /// <summary>
  /// Optional custom identifier of the list. It can be used later to get list by this id.
  /// </summary>
  /// <value>Optional custom identifier of the list. It can be used later to get list by this id.</value>
  [DataMember(Name="customId", EmitDefaultValue=false)]
  [JsonPropertyName("customId")]
  public string CustomId { get; set; }

  /// <summary>
  /// List containing DirectSalaryPayment objects.
  /// </summary>
  /// <value>List containing DirectSalaryPayment objects.</value>
  [DataMember(Name="payments", EmitDefaultValue=false)]
  [JsonPropertyName("payments")]
  public List<DirectSalaryPayment> Payments { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DirectSalaryPayments {\n");
    sb.Append("  ListName: ").Append(ListName).Append('\n');
    sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  PayerBankAccount: ").Append(PayerBankAccount).Append('\n');
    sb.Append("  CustomId: ").Append(CustomId).Append('\n');
    sb.Append("  Payments: ").Append(Payments).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}