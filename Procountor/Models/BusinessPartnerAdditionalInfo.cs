using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Additional info of the partner.
/// </summary>
[DataContract]
public class BusinessPartnerAdditionalInfo {
  /// <summary>
  /// Default additional information of the partner.
  /// </summary>
  /// <value>Default additional information of the partner.</value>
  [DataMember(Name="defaultAdditionalInformation", EmitDefaultValue=false)]
  [JsonPropertyName("defaultAdditionalInformation")]
  public string DefaultAdditionalInformation { get; set; }

  /// <summary>
  /// Default reference of the partner.
  /// </summary>
  /// <value>Default reference of the partner.</value>
  [DataMember(Name="defaultReference", EmitDefaultValue=false)]
  [JsonPropertyName("defaultReference")]
  public string DefaultReference { get; set; }

  /// <summary>
  /// Group.
  /// </summary>
  /// <value>Group.</value>
  [DataMember(Name="group", EmitDefaultValue=false)]
  [JsonPropertyName("group")]
  public string Group { get; set; }

  /// <summary>
  /// Company ID of the partner. If provided partnerCompanyId will be validated according to the type.
  /// </summary>
  /// <value>Company ID of the partner. If provided partnerCompanyId will be validated according to the type.</value>
  [DataMember(Name="parentCompanyIdType", EmitDefaultValue=false)]
  [JsonPropertyName("parentCompanyIdType")]
  public string ParentCompanyIdType { get; set; }

  /// <summary>
  /// Parent company ID of the partner.
  /// </summary>
  /// <value>Parent company ID of the partner.</value>
  [DataMember(Name="parentCompanyId", EmitDefaultValue=false)]
  [JsonPropertyName("parentCompanyId")]
  public string ParentCompanyId { get; set; }

  /// <summary>
  /// Auxiliary company name.
  /// </summary>
  /// <value>Auxiliary company name.</value>
  [DataMember(Name="auxiliaryCompanyName", EmitDefaultValue=false)]
  [JsonPropertyName("auxiliaryCompanyName")]
  public string AuxiliaryCompanyName { get; set; }

  /// <summary>
  /// Former company name of the partner.
  /// </summary>
  /// <value>Former company name of the partner.</value>
  [DataMember(Name="formerCompanyName", EmitDefaultValue=false)]
  [JsonPropertyName("formerCompanyName")]
  public string FormerCompanyName { get; set; }

  /// <summary>
  /// Sector of the partner.
  /// </summary>
  /// <value>Sector of the partner.</value>
  [DataMember(Name="sector", EmitDefaultValue=false)]
  [JsonPropertyName("sector")]
  public string Sector { get; set; }

  /// <summary>
  /// Web address of the partner.
  /// </summary>
  /// <value>Web address of the partner.</value>
  [DataMember(Name="webAddress", EmitDefaultValue=false)]
  [JsonPropertyName("webAddress")]
  public string WebAddress { get; set; }

  /// <summary>
  /// First comment
  /// </summary>
  /// <value>First comment</value>
  [DataMember(Name="comments1", EmitDefaultValue=false)]
  [JsonPropertyName("comments1")]
  public string Comments1 { get; set; }

  /// <summary>
  /// Second comment
  /// </summary>
  /// <value>Second comment</value>
  [DataMember(Name="comments2", EmitDefaultValue=false)]
  [JsonPropertyName("comments2")]
  public string Comments2 { get; set; }

  /// <summary>
  /// Invoice period of the partner.
  /// </summary>
  /// <value>Invoice period of the partner.</value>
  [DataMember(Name="invoicePeriod", EmitDefaultValue=false)]
  [JsonPropertyName("invoicePeriod")]
  public string InvoicePeriod { get; set; }

  /// <summary>
  /// Withholding register marking of the partner.
  /// </summary>
  /// <value>Withholding register marking of the partner.</value>
  [DataMember(Name="withholdingRegisterMarking", EmitDefaultValue=false)]
  [JsonPropertyName("withholdingRegisterMarking")]
  public string WithholdingRegisterMarking { get; set; }

  /// <summary>
  /// Invoice ledger of the partner. If not provided, it will be automatically set to INVOICE_LEDGER.
  /// </summary>
  /// <value>Invoice ledger of the partner. If not provided, it will be automatically set to INVOICE_LEDGER.</value>
  [DataMember(Name="invoiceLedger", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceLedger")]
  public string InvoiceLedger { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerAdditionalInfo {\n");
      sb.Append("  DefaultAdditionalInformation: ").Append(DefaultAdditionalInformation).Append('\n');
      sb.Append("  DefaultReference: ").Append(DefaultReference).Append('\n');
      sb.Append("  Group: ").Append(Group).Append('\n');
      sb.Append("  ParentCompanyIdType: ").Append(ParentCompanyIdType).Append('\n');
      sb.Append("  ParentCompanyId: ").Append(ParentCompanyId).Append('\n');
      sb.Append("  AuxiliaryCompanyName: ").Append(AuxiliaryCompanyName).Append('\n');
      sb.Append("  FormerCompanyName: ").Append(FormerCompanyName).Append('\n');
      sb.Append("  Sector: ").Append(Sector).Append('\n');
      sb.Append("  WebAddress: ").Append(WebAddress).Append('\n');
      sb.Append("  Comments1: ").Append(Comments1).Append('\n');
      sb.Append("  Comments2: ").Append(Comments2).Append('\n');
      sb.Append("  InvoicePeriod: ").Append(InvoicePeriod).Append('\n');
      sb.Append("  WithholdingRegisterMarking: ").Append(WithholdingRegisterMarking).Append('\n');
      sb.Append("  InvoiceLedger: ").Append(InvoiceLedger).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}