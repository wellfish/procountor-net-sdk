using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contains rights for purchases functionalities.
/// </summary>
[DataContract]
public class PurchasesRights {
  /// <summary>
  /// Access level to new purchase invoice functionality.
  /// </summary>
  /// <value>Access level to new purchase invoice functionality.</value>
  [DataMember(Name="newPurchaseInvoice", EmitDefaultValue=false)]
  [JsonPropertyName("newPurchaseInvoice")]
  public string NewPurchaseInvoice { get; set; }

  /// <summary>
  /// Access level to new travel and expense invoice functionality.
  /// </summary>
  /// <value>Access level to new travel and expense invoice functionality.</value>
  [DataMember(Name="newTravelAndExpenseInvoice", EmitDefaultValue=false)]
  [JsonPropertyName("newTravelAndExpenseInvoice")]
  public string NewTravelAndExpenseInvoice { get; set; }

  /// <summary>
  /// Access level to new journal receipt functionality.
  /// </summary>
  /// <value>Access level to new journal receipt functionality.</value>
  [DataMember(Name="newJournalReceipt", EmitDefaultValue=false)]
  [JsonPropertyName("newJournalReceipt")]
  public string NewJournalReceipt { get; set; }

  /// <summary>
  /// Access level to search invoices functionality.
  /// </summary>
  /// <value>Access level to search invoices functionality.</value>
  [DataMember(Name="searchInvoices", EmitDefaultValue=false)]
  [JsonPropertyName("searchInvoices")]
  public string SearchInvoices { get; set; }

  /// <summary>
  /// Access level to verification functionality.
  /// </summary>
  /// <value>Access level to verification functionality.</value>
  [DataMember(Name="verification", EmitDefaultValue=false)]
  [JsonPropertyName("verification")]
  public string Verification { get; set; }

  /// <summary>
  /// Access level to supplier register functionality.
  /// </summary>
  /// <value>Access level to supplier register functionality.</value>
  [DataMember(Name="supplierRegister", EmitDefaultValue=false)]
  [JsonPropertyName("supplierRegister")]
  public string SupplierRegister { get; set; }

  /// <summary>
  /// Access level to product register functionality.
  /// </summary>
  /// <value>Access level to product register functionality.</value>
  [DataMember(Name="productRegister", EmitDefaultValue=false)]
  [JsonPropertyName("productRegister")]
  public string ProductRegister { get; set; }

  /// <summary>
  /// Access level to fixes assets register functionality. (Only Swedish environment)
  /// </summary>
  /// <value>Access level to fixes assets register functionality. (Only Swedish environment)</value>
  [DataMember(Name="fixedAssetsRegister", EmitDefaultValue=false)]
  [JsonPropertyName("fixedAssetsRegister")]
  public string FixedAssetsRegister { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class PurchasesRights {\n");
      sb.Append("  NewPurchaseInvoice: ").Append(NewPurchaseInvoice).Append('\n');
      sb.Append("  NewTravelAndExpenseInvoice: ").Append(NewTravelAndExpenseInvoice).Append('\n');
      sb.Append("  NewJournalReceipt: ").Append(NewJournalReceipt).Append('\n');
      sb.Append("  SearchInvoices: ").Append(SearchInvoices).Append('\n');
      sb.Append("  Verification: ").Append(Verification).Append('\n');
      sb.Append("  SupplierRegister: ").Append(SupplierRegister).Append('\n');
      sb.Append("  ProductRegister: ").Append(ProductRegister).Append('\n');
      sb.Append("  FixedAssetsRegister: ").Append(FixedAssetsRegister).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}