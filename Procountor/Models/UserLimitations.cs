using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// User limitations in environment
/// </summary>
[DataContract]
public class UserLimitations {
  /// <summary>
  /// Is personal sales invoices only limitations enabled.
  /// </summary>
  /// <value>Is personal sales invoices only limitations enabled.</value>
  [DataMember(Name="personalSalesInvoicesOnly", EmitDefaultValue=false)]
  [JsonPropertyName("personalSalesInvoicesOnly")]
  public bool? PersonalSalesInvoicesOnly { get; set; }

  /// <summary>
  /// Is personal purchases invoices only limitation enabled.
  /// </summary>
  /// <value>Is personal purchases invoices only limitation enabled.</value>
  [DataMember(Name="personalPurchaseInvoicesOnly", EmitDefaultValue=false)]
  [JsonPropertyName("personalPurchaseInvoicesOnly")]
  public bool? PersonalPurchaseInvoicesOnly { get; set; }

  /// <summary>
  /// Is personal travel and expense invoices limitation enabled.
  /// </summary>
  /// <value>Is personal travel and expense invoices limitation enabled.</value>
  [DataMember(Name="personalTravelAndExpenseInvoiceOnly", EmitDefaultValue=false)]
  [JsonPropertyName("personalTravelAndExpenseInvoiceOnly")]
  public bool? PersonalTravelAndExpenseInvoiceOnly { get; set; }

  /// <summary>
  /// Is personal journal receipts only limitation enabled.
  /// </summary>
  /// <value>Is personal journal receipts only limitation enabled.</value>
  [DataMember(Name="personalJournalReceiptsOnly", EmitDefaultValue=false)]
  [JsonPropertyName("personalJournalReceiptsOnly")]
  public bool? PersonalJournalReceiptsOnly { get; set; }

  /// <summary>
  /// Is personal salaries only limitation enabled.
  /// </summary>
  /// <value>Is personal salaries only limitation enabled.</value>
  [DataMember(Name="personalSalariesOnly", EmitDefaultValue=false)]
  [JsonPropertyName("personalSalariesOnly")]
  public bool? PersonalSalariesOnly { get; set; }

  /// <summary>
  /// Is only allow accounting viewing limitation enabled.
  /// </summary>
  /// <value>Is only allow accounting viewing limitation enabled.</value>
  [DataMember(Name="onlyAllowAccountingViewing", EmitDefaultValue=false)]
  [JsonPropertyName("onlyAllowAccountingViewing")]
  public bool? OnlyAllowAccountingViewing { get; set; }

  /// <summary>
  /// Is search for personal approvals only limitation enabled.
  /// </summary>
  /// <value>Is search for personal approvals only limitation enabled.</value>
  [DataMember(Name="searchForPersonalApprovalsOnly", EmitDefaultValue=false)]
  [JsonPropertyName("searchForPersonalApprovalsOnly")]
  public bool? SearchForPersonalApprovalsOnly { get; set; }

  /// <summary>
  /// Is rights to accounting only limitation enabled.
  /// </summary>
  /// <value>Is rights to accounting only limitation enabled.</value>
  [DataMember(Name="rightsToAccountingOnly", EmitDefaultValue=false)]
  [JsonPropertyName("rightsToAccountingOnly")]
  public bool? RightsToAccountingOnly { get; set; }

  /// <summary>
  /// Is limited dimensions limitation enabled.
  /// </summary>
  /// <value>Is limited dimensions limitation enabled.</value>
  [DataMember(Name="limitedDimensions", EmitDefaultValue=false)]
  [JsonPropertyName("limitedDimensions")]
  public bool? LimitedDimensions { get; set; }

  /// <summary>
  /// Is personal work hour tracking limitation enabled.
  /// </summary>
  /// <value>Is personal work hour tracking limitation enabled.</value>
  [DataMember(Name="personalWorkHourTrackingOnly", EmitDefaultValue=false)]
  [JsonPropertyName("personalWorkHourTrackingOnly")]
  public bool? PersonalWorkHourTrackingOnly { get; set; }

  /// <summary>
  /// Is login limitation enabled.
  /// </summary>
  /// <value>Is login limitation enabled.</value>
  [DataMember(Name="login", EmitDefaultValue=false)]
  [JsonPropertyName("login")]
  public bool? Login { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class UserLimitations {\n");
    sb.Append("  PersonalSalesInvoicesOnly: ").Append(PersonalSalesInvoicesOnly).Append('\n');
    sb.Append("  PersonalPurchaseInvoicesOnly: ").Append(PersonalPurchaseInvoicesOnly).Append('\n');
    sb.Append("  PersonalTravelAndExpenseInvoiceOnly: ").Append(PersonalTravelAndExpenseInvoiceOnly).Append('\n');
    sb.Append("  PersonalJournalReceiptsOnly: ").Append(PersonalJournalReceiptsOnly).Append('\n');
    sb.Append("  PersonalSalariesOnly: ").Append(PersonalSalariesOnly).Append('\n');
    sb.Append("  OnlyAllowAccountingViewing: ").Append(OnlyAllowAccountingViewing).Append('\n');
    sb.Append("  SearchForPersonalApprovalsOnly: ").Append(SearchForPersonalApprovalsOnly).Append('\n');
    sb.Append("  RightsToAccountingOnly: ").Append(RightsToAccountingOnly).Append('\n');
    sb.Append("  LimitedDimensions: ").Append(LimitedDimensions).Append('\n');
    sb.Append("  PersonalWorkHourTrackingOnly: ").Append(PersonalWorkHourTrackingOnly).Append('\n');
    sb.Append("  Login: ").Append(Login).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}