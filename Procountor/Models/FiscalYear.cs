using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List of fiscal years.
/// </summary>
[DataContract]
public class FiscalYear {
  /// <summary>
  /// Unique identifier of the fiscal year.
  /// </summary>
  /// <value>Unique identifier of the fiscal year.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Start date of the fiscal year.
  /// </summary>
  /// <value>Start date of the fiscal year.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of the fiscal year
  /// </summary>
  /// <value>End date of the fiscal year</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Time of last edition of fiscal year.
  /// </summary>
  /// <value>Time of last edition of fiscal year.</value>
  [DataMember(Name="modified", EmitDefaultValue=false)]
  [JsonPropertyName("modified")]
  public DateTime? Modified { get; set; }

  /// <summary>
  /// List of tracking periods contained in the fiscal year.
  /// </summary>
  /// <value>List of tracking periods contained in the fiscal year.</value>
  [DataMember(Name="trackingPeriods", EmitDefaultValue=false)]
  [JsonPropertyName("trackingPeriods")]
  public List<TrackingPeriod> TrackingPeriods { get; set; }

  /// <summary>
  /// Whether the fiscal year is open (true) or closed (false).
  /// </summary>
  /// <value>Whether the fiscal year is open (true) or closed (false).</value>
  [DataMember(Name="isOpen", EmitDefaultValue=false)]
  [JsonPropertyName("isOpen")]
  public bool? IsOpen { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class FiscalYear {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  StartDate: ").Append(StartDate).Append('\n');
    sb.Append("  EndDate: ").Append(EndDate).Append('\n');
    sb.Append("  Modified: ").Append(Modified).Append('\n');
    sb.Append("  TrackingPeriods: ").Append(TrackingPeriods).Append('\n');
    sb.Append("  IsOpen: ").Append(IsOpen).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}