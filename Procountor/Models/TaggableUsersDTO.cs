using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class TaggableUsersDTO {
  /// <summary>
  /// List of taggable users.
  /// </summary>
  /// <value>List of taggable users.</value>
  [DataMember(Name="taggableUsers", EmitDefaultValue=false)]
  [JsonPropertyName("taggableUsers")]
  public List<TaggableUserDTO> TaggableUsers { get; set; }

  /// <summary>
  /// Gets or Sets Comments
  /// </summary>
  [DataMember(Name="comments", EmitDefaultValue=false)]
  [JsonPropertyName("comments")]
  public List<TaggableUserDTO> Comments { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TaggableUsersDTO {\n");
      sb.Append("  TaggableUsers: ").Append(TaggableUsers).Append('\n');
      sb.Append("  Comments: ").Append(Comments).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}