using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class InvoiceNotes {
  /// <summary>
  /// Invoice notes. \\n can be used as a line break.
  /// </summary>
  /// <value>Invoice notes. \\n can be used as a line break.</value>
  [DataMember(Name="notes", EmitDefaultValue=false)]
  [JsonPropertyName("notes")]
  public string Notes { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceNotes {\n");
    sb.Append("  Notes: ").Append(Notes).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}