using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// These are possible response models
/// </summary>
[DataContract]
public class DirectSalaryPaymentsConfirmationResponseModels {
  /// <summary>
  /// Gets or Sets PaymentGroup
  /// </summary>
  [DataMember(Name="PaymentGroup", EmitDefaultValue=false)]
  [JsonPropertyName("PaymentGroup")]
  public PaymentGroup PaymentGroup { get; set; }

  /// <summary>
  /// Gets or Sets InfoMessage
  /// </summary>
  [DataMember(Name="InfoMessage", EmitDefaultValue=false)]
  [JsonPropertyName("InfoMessage")]
  public InfoMessage InfoMessage { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DirectSalaryPaymentsConfirmationResponseModels {\n");
    sb.Append("  PaymentGroup: ").Append(PaymentGroup).Append('\n');
    sb.Append("  InfoMessage: ").Append(InfoMessage).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}