using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Invoice rows. An invoice should always have at least one row. The only exception are PERIODIC_TAX_RETURN invoices which do not have any invoice rows.
/// </summary>
[DataContract]
public class InvoiceRow {
  /// <summary>
  /// Unique identifier for a invoice row.
  /// </summary>
  /// <value>Unique identifier for a invoice row.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Unique identifier for a product. Links the invoice row to a product in the product register. Note that all other fields (name, price, ...) of an invoice row can be modified independently of the information in the product register. If one wants to use accounts from product register, accountingByRow must be true.
  /// </summary>
  /// <value>Unique identifier for a product. Links the invoice row to a product in the product register. Note that all other fields (name, price, ...) of an invoice row can be modified independently of the information in the product register. If one wants to use accounts from product register, accountingByRow must be true.</value>
  [DataMember(Name="productId", EmitDefaultValue=false)]
  [JsonPropertyName("productId")]
  public int? ProductId { get; set; }

  /// <summary>
  /// Product name.
  /// </summary>
  /// <value>Product name.</value>
  [DataMember(Name="product", EmitDefaultValue=false)]
  [JsonPropertyName("product")]
  public string Product { get; set; }

  /// <summary>
  /// Product code.
  /// </summary>
  /// <value>Product code.</value>
  [DataMember(Name="productCode", EmitDefaultValue=false)]
  [JsonPropertyName("productCode")]
  public string ProductCode { get; set; }

  /// <summary>
  /// Product quantity.
  /// </summary>
  /// <value>Product quantity.</value>
  [DataMember(Name="quantity", EmitDefaultValue=false)]
  [JsonPropertyName("quantity")]
  public decimal? Quantity { get; set; }

  /// <summary>
  /// Product unit.
  /// </summary>
  /// <value>Product unit.</value>
  [DataMember(Name="unit", EmitDefaultValue=false)]
  [JsonPropertyName("unit")]
  public string Unit { get; set; }

  /// <summary>
  /// Product unit price. This value is affected by the \"unit prices include VAT\" setting on the invoice.
  /// </summary>
  /// <value>Product unit price. This value is affected by the \"unit prices include VAT\" setting on the invoice.</value>
  [DataMember(Name="unitPrice", EmitDefaultValue=false)]
  [JsonPropertyName("unitPrice")]
  public decimal? UnitPrice { get; set; }

  /// <summary>
  /// Product discount percentage.
  /// </summary>
  /// <value>Product discount percentage.</value>
  [DataMember(Name="discountPercent", EmitDefaultValue=false)]
  [JsonPropertyName("discountPercent")]
  public decimal? DiscountPercent { get; set; }

  /// <summary>
  /// Product VAT percentage. Must be a percentage currently in use for the company.
  /// </summary>
  /// <value>Product VAT percentage. Must be a percentage currently in use for the company.</value>
  [DataMember(Name="vatPercent", EmitDefaultValue=false)]
  [JsonPropertyName("vatPercent")]
  public decimal? VatPercent { get; set; }

  /// <summary>
  /// Product VAT status.
  /// </summary>
  /// <value>Product VAT status.</value>
  [DataMember(Name="vatStatus", EmitDefaultValue=false)]
  [JsonPropertyName("vatStatus")]
  public int? VatStatus { get; set; }

  /// <summary>
  /// Invoice row comment. Visible on the invoice. Use \\ as line break.
  /// </summary>
  /// <value>Invoice row comment. Visible on the invoice. Use \\ as line break.</value>
  [DataMember(Name="comment", EmitDefaultValue=false)]
  [JsonPropertyName("comment")]
  public string Comment { get; set; }

  /// <summary>
  /// Start date of accrual/delivery periods of invoice row.
  /// </summary>
  /// <value>Start date of accrual/delivery periods of invoice row.</value>
  [DataMember(Name="startDate", EmitDefaultValue=false)]
  [JsonPropertyName("startDate")]
  public DateTime? StartDate { get; set; }

  /// <summary>
  /// End date of accrual/delivery periods of invoice row.
  /// </summary>
  /// <value>End date of accrual/delivery periods of invoice row.</value>
  [DataMember(Name="endDate", EmitDefaultValue=false)]
  [JsonPropertyName("endDate")]
  public DateTime? EndDate { get; set; }

  /// <summary>
  /// Header text of the content row associated with invoice row
  /// </summary>
  /// <value>Header text of the content row associated with invoice row</value>
  [DataMember(Name="headerText", EmitDefaultValue=false)]
  [JsonPropertyName("headerText")]
  public string HeaderText { get; set; }

  /// <summary>
  /// Explanation text of the content row associated with invoice row
  /// </summary>
  /// <value>Explanation text of the content row associated with invoice row</value>
  [DataMember(Name="explanationText", EmitDefaultValue=false)]
  [JsonPropertyName("explanationText")]
  public string ExplanationText { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class InvoiceRow {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  ProductId: ").Append(ProductId).Append('\n');
    sb.Append("  Product: ").Append(Product).Append('\n');
    sb.Append("  ProductCode: ").Append(ProductCode).Append('\n');
    sb.Append("  Quantity: ").Append(Quantity).Append('\n');
    sb.Append("  Unit: ").Append(Unit).Append('\n');
    sb.Append("  UnitPrice: ").Append(UnitPrice).Append('\n');
    sb.Append("  DiscountPercent: ").Append(DiscountPercent).Append('\n');
    sb.Append("  VatPercent: ").Append(VatPercent).Append('\n');
    sb.Append("  VatStatus: ").Append(VatStatus).Append('\n');
    sb.Append("  Comment: ").Append(Comment).Append('\n');
    sb.Append("  StartDate: ").Append(StartDate).Append('\n');
    sb.Append("  EndDate: ").Append(EndDate).Append('\n');
    sb.Append("  HeaderText: ").Append(HeaderText).Append('\n');
    sb.Append("  ExplanationText: ").Append(ExplanationText).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}