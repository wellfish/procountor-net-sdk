using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Values of dimension items associated with this transaction. The number of provided dimension items must be within the dimension max count defined by the purchased Procountor license. Provided dimension pairs (dimension id - item id) must be unique within the list provided.
/// </summary>
[DataContract]
public class DimensionItemValue {
  /// <summary>
  /// Dimension ID. Must exist in the current environment. For a list of available dimensions, see the GET /dimensions endpoint.
  /// </summary>
  /// <value>Dimension ID. Must exist in the current environment. For a list of available dimensions, see the GET /dimensions endpoint.</value>
  [DataMember(Name="dimensionId", EmitDefaultValue=false)]
  [JsonPropertyName("dimensionId")]
  public int? DimensionId { get; set; }

  /// <summary>
  /// Dimension item ID. Must exist in the current environment. For a list of available dimensions, see the GET /dimensions endpoint.
  /// </summary>
  /// <value>Dimension item ID. Must exist in the current environment. For a list of available dimensions, see the GET /dimensions endpoint.</value>
  [DataMember(Name="itemId", EmitDefaultValue=false)]
  [JsonPropertyName("itemId")]
  public int? ItemId { get; set; }

  /// <summary>
  /// Dimension item value with maximum two decimal places. Use absolute values instead of percentages. The sum of dimension item values on a dimension must equal the accounting value of the parent transaction.
  /// </summary>
  /// <value>Dimension item value with maximum two decimal places. Use absolute values instead of percentages. The sum of dimension item values on a dimension must equal the accounting value of the parent transaction.</value>
  [DataMember(Name="value", EmitDefaultValue=false)]
  [JsonPropertyName("value")]
  public decimal? Value { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DimensionItemValue {\n");
    sb.Append("  DimensionId: ").Append(DimensionId).Append('\n');
    sb.Append("  ItemId: ").Append(ItemId).Append('\n');
    sb.Append("  Value: ").Append(Value).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}