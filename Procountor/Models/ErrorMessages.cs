using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class ErrorMessages {
  /// <summary>
  /// List of error messages.
  /// </summary>
  /// <value>List of error messages.</value>
  [DataMember(Name="errors", EmitDefaultValue=false)]
  [JsonPropertyName("errors")]
  public List<ErrorMessage> Errors { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class ErrorMessages {\n");
    sb.Append("  Errors: ").Append(Errors).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}