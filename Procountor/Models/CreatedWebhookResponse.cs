using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class CreatedWebhookResponse {
  /// <summary>
  /// HTTP response status.
  /// </summary>
  /// <value>HTTP response status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public int? Status { get; set; }

  /// <summary>
  /// Generated UUID
  /// </summary>
  /// <value>Generated UUID</value>
  [DataMember(Name="uuid", EmitDefaultValue=false)]
  [JsonPropertyName("uuid")]
  public Guid? Uuid { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class CreatedWebhookResponse {\n");
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("  Uuid: ").Append(Uuid).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}