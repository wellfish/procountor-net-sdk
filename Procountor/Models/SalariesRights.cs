using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// Contains rights for salaries functionalities.
/// </summary>
[DataContract]
public class SalariesRights {
  /// <summary>
  /// Access level to payroll accounting (all salary functions) functionality.
  /// </summary>
  /// <value>Access level to payroll accounting (all salary functions) functionality.</value>
  [DataMember(Name="payrollAccounting", EmitDefaultValue=false)]
  [JsonPropertyName("payrollAccounting")]
  public string PayrollAccounting { get; set; }

  /// <summary>
  /// Access level to person register functionality.
  /// </summary>
  /// <value>Access level to person register functionality.</value>
  [DataMember(Name="personRegister", EmitDefaultValue=false)]
  [JsonPropertyName("personRegister")]
  public string PersonRegister { get; set; }

  /// <summary>
  /// Access level to working time tracking functionality.
  /// </summary>
  /// <value>Access level to working time tracking functionality.</value>
  [DataMember(Name="workingTimeTracking", EmitDefaultValue=false)]
  [JsonPropertyName("workingTimeTracking")]
  public string WorkingTimeTracking { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class SalariesRights {\n");
      sb.Append("  PayrollAccounting: ").Append(PayrollAccounting).Append('\n');
      sb.Append("  PersonRegister: ").Append(PersonRegister).Append('\n');
      sb.Append("  WorkingTimeTracking: ").Append(WorkingTimeTracking).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}