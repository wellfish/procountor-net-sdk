using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class BusinessPartnerDefaultAccounts {
  /// <summary>
  /// Default VAT %.
  /// </summary>
  /// <value>Default VAT %.</value>
  [DataMember(Name="vatPercent", EmitDefaultValue=false)]
  [JsonPropertyName("vatPercent")]
  public decimal? VatPercent { get; set; }

  /// <summary>
  /// Default VAT deduction %.
  /// </summary>
  /// <value>Default VAT deduction %.</value>
  [DataMember(Name="vatDeduction", EmitDefaultValue=false)]
  [JsonPropertyName("vatDeduction")]
  public decimal? VatDeduction { get; set; }

  /// <summary>
  /// VAT status.
  /// </summary>
  /// <value>VAT status.</value>
  [DataMember(Name="vatStatus", EmitDefaultValue=false)]
  [JsonPropertyName("vatStatus")]
  public int? VatStatus { get; set; }

  /// <summary>
  /// VAT processing.
  /// </summary>
  /// <value>VAT processing.</value>
  [DataMember(Name="vatProcessing", EmitDefaultValue=false)]
  [JsonPropertyName("vatProcessing")]
  public string VatProcessing { get; set; }

  /// <summary>
  /// Incoming invoice parsing. If true always combine the invoice rows of an incoming invoice into one summary row using product 'Invoicing according to attachment'.
  /// </summary>
  /// <value>Incoming invoice parsing. If true always combine the invoice rows of an incoming invoice into one summary row using product 'Invoicing according to attachment'.</value>
  [DataMember(Name="combineInvoiceRows", EmitDefaultValue=false)]
  [JsonPropertyName("combineInvoiceRows")]
  public bool? CombineInvoiceRows { get; set; }

  /// <summary>
  /// Gets or Sets SalesInvoiceAccounts
  /// </summary>
  [DataMember(Name="salesInvoiceAccounts", EmitDefaultValue=false)]
  [JsonPropertyName("salesInvoiceAccounts")]
  public BusinessPartnerAccountsInfo SalesInvoiceAccounts { get; set; }

  /// <summary>
  /// Gets or Sets PurchaseInvoiceAccounts
  /// </summary>
  [DataMember(Name="purchaseInvoiceAccounts", EmitDefaultValue=false)]
  [JsonPropertyName("purchaseInvoiceAccounts")]
  public BusinessPartnerAccountsInfo PurchaseInvoiceAccounts { get; set; }

  /// <summary>
  /// Gets or Sets TravelInvoiceAccounts
  /// </summary>
  [DataMember(Name="travelInvoiceAccounts", EmitDefaultValue=false)]
  [JsonPropertyName("travelInvoiceAccounts")]
  public BusinessPartnerAccountsInfo TravelInvoiceAccounts { get; set; }

  /// <summary>
  /// Gets or Sets ExpenseClaimAccounts
  /// </summary>
  [DataMember(Name="expenseClaimAccounts", EmitDefaultValue=false)]
  [JsonPropertyName("expenseClaimAccounts")]
  public BusinessPartnerAccountsInfo ExpenseClaimAccounts { get; set; }

  /// <summary>
  /// Gets or Sets JournalAccounts
  /// </summary>
  [DataMember(Name="journalAccounts", EmitDefaultValue=false)]
  [JsonPropertyName("journalAccounts")]
  public BusinessPartnerAccountsInfo JournalAccounts { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BusinessPartnerDefaultAccounts {\n");
      sb.Append("  VatPercent: ").Append(VatPercent).Append('\n');
      sb.Append("  VatDeduction: ").Append(VatDeduction).Append('\n');
      sb.Append("  VatStatus: ").Append(VatStatus).Append('\n');
      sb.Append("  VatProcessing: ").Append(VatProcessing).Append('\n');
      sb.Append("  CombineInvoiceRows: ").Append(CombineInvoiceRows).Append('\n');
      sb.Append("  SalesInvoiceAccounts: ").Append(SalesInvoiceAccounts).Append('\n');
      sb.Append("  PurchaseInvoiceAccounts: ").Append(PurchaseInvoiceAccounts).Append('\n');
      sb.Append("  TravelInvoiceAccounts: ").Append(TravelInvoiceAccounts).Append('\n');
      sb.Append("  ExpenseClaimAccounts: ").Append(ExpenseClaimAccounts).Append('\n');
      sb.Append("  JournalAccounts: ").Append(JournalAccounts).Append('\n');
      sb.Append("}\n");
      return sb.ToString();
    }
}