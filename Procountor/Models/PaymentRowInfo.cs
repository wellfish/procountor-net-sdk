using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// 
/// </summary>
[DataContract]
public class PaymentRowInfo {
  /// <summary>
  /// Unique payment identifier.
  /// </summary>
  /// <value>Unique payment identifier.</value>
  [DataMember(Name="id", EmitDefaultValue=false)]
  [JsonPropertyName("id")]
  public int? Id { get; set; }

  /// <summary>
  /// Unique invoice identifier.
  /// </summary>
  /// <value>Unique invoice identifier.</value>
  [DataMember(Name="invoiceId", EmitDefaultValue=false)]
  [JsonPropertyName("invoiceId")]
  public int? InvoiceId { get; set; }

  /// <summary>
  /// Date specifying when the payment transaction has to be performed. Payment date should be grater than current date.
  /// </summary>
  /// <value>Date specifying when the payment transaction has to be performed. Payment date should be grater than current date.</value>
  [DataMember(Name="paymentDate", EmitDefaultValue=false)]
  [JsonPropertyName("paymentDate")]
  public DateTime? PaymentDate { get; set; }

  /// <summary>
  /// The amount of the payment in the given currency. Currency is defined within the invoice.
  /// </summary>
  /// <value>The amount of the payment in the given currency. Currency is defined within the invoice.</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Recipient name.
  /// </summary>
  /// <value>Recipient name.</value>
  [DataMember(Name="receiverName", EmitDefaultValue=false)]
  [JsonPropertyName("receiverName")]
  public string ReceiverName { get; set; }

  /// <summary>
  /// Currency of payment.
  /// </summary>
  /// <value>Currency of payment.</value>
  [DataMember(Name="currency", EmitDefaultValue=false)]
  [JsonPropertyName("currency")]
  public string Currency { get; set; }

  /// <summary>
  /// Payer bank account.
  /// </summary>
  /// <value>Payer bank account.</value>
  [DataMember(Name="payerAccount", EmitDefaultValue=false)]
  [JsonPropertyName("payerAccount")]
  public string PayerAccount { get; set; }

  /// <summary>
  /// Payer BIC (Bank Identifier Code) code.
  /// </summary>
  /// <value>Payer BIC (Bank Identifier Code) code.</value>
  [DataMember(Name="payerBic", EmitDefaultValue=false)]
  [JsonPropertyName("payerBic")]
  public string PayerBic { get; set; }

  /// <summary>
  /// Message of the payment.
  /// </summary>
  /// <value>Message of the payment.</value>
  [DataMember(Name="message", EmitDefaultValue=false)]
  [JsonPropertyName("message")]
  public string Message { get; set; }

  /// <summary>
  /// Receiver bank account.
  /// </summary>
  /// <value>Receiver bank account.</value>
  [DataMember(Name="receiverAccount", EmitDefaultValue=false)]
  [JsonPropertyName("receiverAccount")]
  public string ReceiverAccount { get; set; }

  /// <summary>
  /// Received BIC (Bank Identifier Code) code.
  /// </summary>
  /// <value>Received BIC (Bank Identifier Code) code.</value>
  [DataMember(Name="receiverBic", EmitDefaultValue=false)]
  [JsonPropertyName("receiverBic")]
  public string ReceiverBic { get; set; }

  /// <summary>
  /// Name of the user who created payment.
  /// </summary>
  /// <value>Name of the user who created payment.</value>
  [DataMember(Name="createdBy", EmitDefaultValue=false)]
  [JsonPropertyName("createdBy")]
  public string CreatedBy { get; set; }

  /// <summary>
  /// Payment status.
  /// </summary>
  /// <value>Payment status.</value>
  [DataMember(Name="status", EmitDefaultValue=false)]
  [JsonPropertyName("status")]
  public string Status { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class PaymentRowInfo {\n");
    sb.Append("  Id: ").Append(Id).Append('\n');
    sb.Append("  InvoiceId: ").Append(InvoiceId).Append('\n');
    sb.Append("  PaymentDate: ").Append(PaymentDate).Append('\n');
    sb.Append("  Amount: ").Append(Amount).Append('\n');
    sb.Append("  ReceiverName: ").Append(ReceiverName).Append('\n');
    sb.Append("  Currency: ").Append(Currency).Append('\n');
    sb.Append("  PayerAccount: ").Append(PayerAccount).Append('\n');
    sb.Append("  PayerBic: ").Append(PayerBic).Append('\n');
    sb.Append("  Message: ").Append(Message).Append('\n');
    sb.Append("  ReceiverAccount: ").Append(ReceiverAccount).Append('\n');
    sb.Append("  ReceiverBic: ").Append(ReceiverBic).Append('\n');
    sb.Append("  CreatedBy: ").Append(CreatedBy).Append('\n');
    sb.Append("  Status: ").Append(Status).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}