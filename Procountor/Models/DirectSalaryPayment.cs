using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace Procountor.Models;

/// <summary>
/// List containing DirectSalaryPayment objects.
/// </summary>
[DataContract]
public class DirectSalaryPayment {
  /// <summary>
  /// The amount of the payment in the given currency. Currency is defined within the invoice, which identifier is set in invoiceId field.
  /// </summary>
  /// <value>The amount of the payment in the given currency. Currency is defined within the invoice, which identifier is set in invoiceId field.</value>
  [DataMember(Name="amount", EmitDefaultValue=false)]
  [JsonPropertyName("amount")]
  public decimal? Amount { get; set; }

  /// <summary>
  /// Bank reference code.
  /// </summary>
  /// <value>Bank reference code.</value>
  [DataMember(Name="bankReferenceCode", EmitDefaultValue=false)]
  [JsonPropertyName("bankReferenceCode")]
  public string BankReferenceCode { get; set; }

  /// <summary>
  /// Reference code generation type. If type not allowed for a user country will be given, then 400 is returned
  /// </summary>
  /// <value>Reference code generation type. If type not allowed for a user country will be given, then 400 is returned</value>
  [DataMember(Name="bankReferenceCodeType", EmitDefaultValue=false)]
  [JsonPropertyName("bankReferenceCodeType")]
  public string BankReferenceCodeType { get; set; }

  /// <summary>
  /// Message for the payment, if reference payment is not used.
  /// </summary>
  /// <value>Message for the payment, if reference payment is not used.</value>
  [DataMember(Name="message", EmitDefaultValue=false)]
  [JsonPropertyName("message")]
  public string Message { get; set; }

  /// <summary>
  /// Bank account number of the recipient.
  /// </summary>
  /// <value>Bank account number of the recipient.</value>
  [DataMember(Name="recipientBankAccount", EmitDefaultValue=false)]
  [JsonPropertyName("recipientBankAccount")]
  public string RecipientBankAccount { get; set; }

  /// <summary>
  /// BIC code of recipient bank account number.
  /// </summary>
  /// <value>BIC code of recipient bank account number.</value>
  [DataMember(Name="recipientBicCode", EmitDefaultValue=false)]
  [JsonPropertyName("recipientBicCode")]
  public string RecipientBicCode { get; set; }

  /// <summary>
  /// The recipient name.
  /// </summary>
  /// <value>The recipient name.</value>
  [DataMember(Name="recipientName", EmitDefaultValue=false)]
  [JsonPropertyName("recipientName")]
  public string RecipientName { get; set; }

  /// <summary>
  /// Gets or Sets RecipientCode
  /// </summary>
  [DataMember(Name="recipientCode", EmitDefaultValue=false)]
  [JsonPropertyName("recipientCode")]
  public BusinessIdentifier RecipientCode { get; set; }

  /// <summary>
  /// The code of the currency.
  /// </summary>
  /// <value>The code of the currency.</value>
  [DataMember(Name="currencyCode", EmitDefaultValue=false)]
  [JsonPropertyName("currencyCode")]
  public string CurrencyCode { get; set; }

  /// <summary>
  /// Custom id for the payment.
  /// </summary>
  /// <value>Custom id for the payment.</value>
  [DataMember(Name="customId", EmitDefaultValue=false)]
  [JsonPropertyName("customId")]
  public string CustomId { get; set; }


  /// <summary>
  /// Get the string presentation of the object
  /// </summary>
  /// <returns>String presentation of the object</returns>
  public override string ToString()  {
    var sb = new StringBuilder();
    sb.Append("class DirectSalaryPayment {\n");
    sb.Append("  Amount: ").Append(Amount).Append('\n');
    sb.Append("  BankReferenceCode: ").Append(BankReferenceCode).Append('\n');
    sb.Append("  BankReferenceCodeType: ").Append(BankReferenceCodeType).Append('\n');
    sb.Append("  Message: ").Append(Message).Append('\n');
    sb.Append("  RecipientBankAccount: ").Append(RecipientBankAccount).Append('\n');
    sb.Append("  RecipientBicCode: ").Append(RecipientBicCode).Append('\n');
    sb.Append("  RecipientName: ").Append(RecipientName).Append('\n');
    sb.Append("  RecipientCode: ").Append(RecipientCode).Append('\n');
    sb.Append("  CurrencyCode: ").Append(CurrencyCode).Append('\n');
    sb.Append("  CustomId: ").Append(CustomId).Append('\n');
    sb.Append("}\n");
    return sb.ToString();
  }
}