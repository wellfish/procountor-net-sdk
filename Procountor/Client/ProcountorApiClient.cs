using System.Text;
using System.Text.Json;
using Procountor.Models;
using RestSharp;

namespace Procountor.Client;

// TODO: implement rate limiting
public sealed class ProcountorApiClient
{
    private readonly string _apiUrl;
    public ProcountorApiClient(string apiUrl = "https://api.procountor.com/api/")
    {
        _apiUrl = apiUrl;
    }
    public async Task<T> ExecuteRequest<T>(Method method, string path, string accessToken)
    {
        RestClient restClient = new(_apiUrl);
        var request = new RestRequest(path, method);
        request.AddHeader("Authorization", $"Bearer {accessToken}");
        request.AddHeader("Content-Type", "application/json");
        request.AddHeader("Accept", "application/json");

        var response = await restClient.ExecuteAsync<T>(request).ConfigureAwait(false);

        if (response.ResponseStatus != ResponseStatus.Completed)
        {
            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }
            throw new HttpRequestException(response.Data?.ToString(), response.ErrorException);
        }

        if (!response.IsSuccessful || response.Data == null)
        {
            var error = response.Content ?? response.ErrorMessage ??  "Could not get data from Procountor";
            throw new HttpRequestException(error);
        }
        if (response.Data != null)
        {
            return response.Data;
        }
        throw new HttpRequestException("Could not get data from Procountor");
    }

    public async Task<(AttachmentMetadata metadata, byte[] fileData)> DownloadDocumentWithMetadata(int id, string accessToken)
    {
        RestClient restClient = new(_apiUrl);
        var request = new RestRequest($"attachments/{id}");
        request.AddHeader("Authorization", $"Bearer {accessToken}");
        request.AddHeader("Accept", "multipart/mixed");

        var response = await restClient.ExecuteAsync(request).ConfigureAwait(false);

        if (response.ResponseStatus != ResponseStatus.Completed || !response.IsSuccessful)
        {
            throw new HttpRequestException(response.ErrorMessage, response.ErrorException);
        }

        if (response.RawBytes == null)
        {
            throw new HttpRequestException("Could not get data from Procountor");
        }

        var boundary = response.Content?.Split("\r\n")[0];
        var parts = Encoding.UTF8.GetString(response.RawBytes).Split(boundary, StringSplitOptions.RemoveEmptyEntries);

        AttachmentMetadata? metadata = null;
        byte[]? fileData = null;

        foreach (var part in parts)
        {
            // If the part is the end boundary, skip processing
            if (part.Trim() == "--" || string.IsNullOrWhiteSpace(part.Trim()))
            {
                continue;
            }
            var headers = part[..part.IndexOf("\r\n\r\n", StringComparison.Ordinal)].Trim();
            var content = part[part.IndexOf("\r\n\r\n", StringComparison.Ordinal)..].Trim();

            if (headers.Contains("application/json"))
            {
                metadata = JsonSerializer.Deserialize<AttachmentMetadata>(content);
            }
            else if (headers.Contains("application/octet-stream"))
            {
                // Get the position of the "Content-Type: application/octet-stream"
                var position = headers.IndexOf("Content-Type: application/octet-stream", StringComparison.Ordinal);

                // Get the raw bytes starting from the position after the "Content-Type: application/octet-stream"
                fileData = response.RawBytes.Skip(position + "Content-Type: application/octet-stream".Length).ToArray();
            }
        }

        if (metadata == null || fileData == null)
        {
            throw new HttpRequestException("Could not get data from Procountor");
        }

        return (metadata, fileData);
    }
}